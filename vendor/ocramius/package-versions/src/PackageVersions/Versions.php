<?php

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    const VERSIONS = array (
  'composer/installers' => 'v1.6.0@cfcca6b1b60bc4974324efb5783c13dca6932b5b',
  'johnpbloch/wordpress-core-installer' => '0.2.1@a04c2c383ef13aae077f36799ed2eafdebd618d2',
  'pantheon-systems/quicksilver-pushback' => '1.0.1@32c65effd6802bdf829f1c68fb75ade2bd5894a0',
  'pantheon-systems/wordpress-composer' => '4.9.8@2b13d79e331571530f30f522dc457bbfbdb4cfff',
  'roots/wp-password-bcrypt' => '1.0.0@5cecd2e98ccc3193443cc5c5db9b3bc7abed5ffa',
  'rvtraveller/qs-composer-installer' => '1.1@20d6f4397e4b77599646767ad030092a60f5f92a',
  'vlucas/phpdotenv' => 'v2.5.1@8abb4f9aa89ddea9d52112c65bbe8d0125e2fa8e',
  'wpackagist-plugin/pantheon-advanced-page-cache' => '0.1.5@tags/0.1.5',
  'wpackagist-plugin/wp-native-php-sessions' => '0.6.9@tags/0.6.9',
  'wpackagist-theme/twentyseventeen' => '1.7@1.7',
  'antecedent/patchwork' => '2.0.9@cab3be4865e47f1dc447715e76c7b616e48b005d',
  'behat/behat' => 'v3.5.0@e4bce688be0c2029dc1700e46058d86428c63cab',
  'behat/gherkin' => 'v4.5.1@74ac03d52c5e23ad8abd5c5cce4ab0e8dc1b530a',
  'behat/mink' => 'v1.7.1@e6930b9c74693dff7f4e58577e1b1743399f3ff9',
  'behat/mink-browserkit-driver' => '1.3.3@1b9a7ce903cfdaaec5fb32bfdbb26118343662eb',
  'behat/mink-extension' => '2.3.1@80f7849ba53867181b7e412df9210e12fba50177',
  'behat/mink-goutte-driver' => 'v1.2.1@8b9ad6d2d95bc70b840d15323365f52fcdaea6ca',
  'behat/mink-selenium2-driver' => 'v1.3.1@473a9f3ebe0c134ee1e623ce8a9c852832020288',
  'behat/transliterator' => 'v1.2.0@826ce7e9c2a6664c0d1f381cbb38b1fb80a7ee2c',
  'brain/monkey' => '1.5.0@44b2ea87147803227154c990fa01fd5e82a6bb61',
  'container-interop/container-interop' => '1.2.0@79cbf1341c22ec75643d841642dd5d6acd83bdb8',
  'doctrine/instantiator' => '1.0.5@8e884e78f9f0eb1329e445619e04456e64d8051d',
  'fabpot/goutte' => 'v3.2.3@3f0eaf0a40181359470651f1565b3e07e3dd31b8',
  'guzzlehttp/guzzle' => '6.3.3@407b0cb880ace85c9b63c5f9551db498cb2d50ba',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.4.2@f5b8a8512e2b58b0071a7280e39f14f72e05d87c',
  'hamcrest/hamcrest-php' => 'v1.2.2@b37020aa976fa52d3de9aa904aa2522dc518f79c',
  'instaclick/php-webdriver' => '1.4.5@6fa959452e774dcaed543faad3a9d1a37d803327',
  'mockery/mockery' => '0.9.9@6fdb61243844dc924071d3404bb23994ea0b6856',
  'myclabs/deep-copy' => '1.7.0@3b8a3a99ba1f6a3952ac2747d989303cbd6b7a3e',
  'ocramius/package-versions' => '1.2.0@ad8a245decad4897cc6b432743913dad0d69753c',
  'ocramius/proxy-manager' => '2.0.4@a55d08229f4f614bf335759ed0cf63378feeb2e6',
  'paulgibbs/behat-wordpress-extension' => 'v0.8.0@04aaa4e2262f3678c4cf76829331b99051bf0ff9',
  'phar-io/manifest' => '1.0.1@2df402786ab5368a0169091f61a7c1e0eb6852d0',
  'phar-io/version' => '1.0.1@a70c0ced4be299a63d32fa96d9281d03e94041df',
  'phpdocumentor/reflection-common' => '1.0.1@21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
  'phpdocumentor/reflection-docblock' => '4.3.0@94fd0001232e47129dd3504189fa1c7225010d08',
  'phpdocumentor/type-resolver' => '0.4.0@9c977708995954784726e25d0cd1dddf4e65b0f7',
  'phpspec/prophecy' => '1.8.0@4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
  'phpunit/php-code-coverage' => '5.3.2@c89677919c5dd6d3b3852f230a663118762218ac',
  'phpunit/php-file-iterator' => '1.4.5@730b01bc3e867237eaac355e06a36b85dd93a8b4',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '1.0.9@3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
  'phpunit/php-token-stream' => '2.0.2@791198a2c6254db10131eecfe8c06670700904db',
  'phpunit/phpunit' => '6.5.13@0973426fb012359b2f18d3bd1e90ef1172839693',
  'phpunit/phpunit-mock-objects' => '5.0.10@cd1cf05c553ecfec36b170070573e540b67d3f1f',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/log' => '1.0.2@4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
  'roave/security-advisories' => 'dev-master@74a42b8d8d9f9cd672be58e7d1c65094da4ae971',
  'sebastian/code-unit-reverse-lookup' => '1.0.1@4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
  'sebastian/comparator' => '2.1.3@34369daee48eafb2651bea869b4b15d75ccc35f9',
  'sebastian/diff' => '2.0.1@347c1d8b49c5c3ee30c7040ea6fc446790e6bddd',
  'sebastian/environment' => '3.1.0@cd0871b3975fb7fc44d11314fd1ee20925fce4f5',
  'sebastian/exporter' => '3.1.0@234199f4528de6d12aaa58b612e98f7d36adb937',
  'sebastian/global-state' => '2.0.0@e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
  'sebastian/object-enumerator' => '3.0.3@7cfd9e65d11ffb5af41198476395774d4c8a84c5',
  'sebastian/object-reflector' => '1.1.1@773f97c67f28de00d397be301821b06708fca0be',
  'sebastian/recursion-context' => '3.0.0@5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
  'sebastian/resource-operations' => '1.0.0@ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
  'sebastian/version' => '2.0.1@99732be0ddb3361e16ad77b68ba41efc8e979019',
  'sensiolabs/behat-page-object-extension' => 'v2.1.0@bd2a34221ba65ea8c86d8e693992d718de03dbae',
  'squizlabs/php_codesniffer' => '2.9.2@2acf168de78487db620ab4bc524135a13cfe6745',
  'symfony/browser-kit' => 'v3.3.6@8079a6b3668ef15cdbf73a4c7d31081abb8bb5f0',
  'symfony/class-loader' => 'v3.3.6@386a294d621576302e7cc36965d6ed53b8c73c4f',
  'symfony/config' => 'v3.3.6@54ee12b0dd60f294132cabae6f5da9573d2e5297',
  'symfony/console' => 'v2.8.47@48ed63767c4add573fb3e9e127d3426db27f78e8',
  'symfony/css-selector' => 'v3.3.6@4d882dced7b995d5274293039370148e291808f2',
  'symfony/debug' => 'v3.0.9@697c527acd9ea1b2d3efac34d9806bf255278b0a',
  'symfony/dependency-injection' => 'v3.3.6@8d70987f991481e809c63681ffe8ce3f3fde68a0',
  'symfony/dom-crawler' => 'v3.3.6@fc2c588ce376e9fe04a7b8c79e3ec62fe32d95b1',
  'symfony/event-dispatcher' => 'v3.3.6@67535f1e3fd662bdc68d7ba317c93eecd973617e',
  'symfony/filesystem' => 'v3.3.6@427987eb4eed764c3b6e38d52a0f87989e010676',
  'symfony/polyfill-mbstring' => 'v1.10.0@c79c051f5b3a46be09205c73b80b346e4153e494',
  'symfony/translation' => 'v3.3.6@35dd5fb003c90e8bd4d8cabdf94bf9c96d06fdc3',
  'symfony/yaml' => 'v3.3.6@ddc23324e6cfe066f3dd34a37ff494fa80b617ed',
  'theseer/tokenizer' => '1.1.0@cb2f008f3f05af2893a87208fe6a6c4985483f8b',
  'webmozart/assert' => '1.3.0@0df1908962e7a3071564e857d86874dad1ef204a',
  'wp-coding-standards/wpcs' => 'dev-master@7aa217ab38156c5cb4eae0f04ae376027c407a9b',
  'zendframework/zend-code' => '3.1.0@2899c17f83a7207f2d7f53ec2f421204d3beea27',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'pantheon-systems/example-wordpress-composer' => 'dev-master@dbe2c9602f243a340dec6a747b6581e89ad247d1',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException if a version cannot be located
     */
    public static function getVersion(string $packageName) : string
    {
        if (! isset(self::VERSIONS[$packageName])) {
            throw new \OutOfBoundsException(
                'Required package "' . $packageName . '" is not installed: cannot detect its version'
            );
        }

        return self::VERSIONS[$packageName];
    }
}
