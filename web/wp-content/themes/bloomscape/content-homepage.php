<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package bloomscape
 */

use Bloomscape\Customizer;

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" class="main-hero"
	 style="<?php Customizer::init()->bloomscape_homepage_content_styles(); ?>"
	 data-featured-image="<?php echo $featured_image; // WPCS: XSS OK. ?>">
	<?php
	echo Customizer::init()->bloomscape_homepage_hero(); // WPCS: XSS OK.
	?>
</div><!-- #post-## -->
