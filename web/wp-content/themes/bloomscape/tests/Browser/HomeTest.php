<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class HomeTest
 *
 * @package Tests\Browser
 */
class HomeTest extends DuskTestCase {

	/**
	 * Feature test
	 *
	 * @throws \Exception Exception.
	 * @throws \Throwable Manage \Throwable Exceptions.
	 */
	public function test_if_home_renders_ok() {
		$this->browse(
			function ( Browser $browser ) {
				$browser->visit( '/' )
					->assertSee( 'bloomscape' );
			}
		);
	}
}
