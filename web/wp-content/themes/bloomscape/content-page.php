<?php
	/**
	 * Template Name: Content Page
	 */
	get_header();
	the_post();
?>

	<div class="inner-wrapper">
		<?php
			the_content();
		?>
		<div class="inner-content">
			<?php get_template_part( '/partials/content-inner-module' ); ?>
			<?php get_template_part( '/partials/side-image-module' ); ?>
		</div>
	</div>

<?php
	get_footer();
