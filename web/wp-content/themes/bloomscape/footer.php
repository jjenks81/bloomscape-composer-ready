<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

</div><!-- #content -->

<?php do_action( 'storefront_before_footer' ); ?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="row footer-nav">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-4">
					<h4><?php echo esc_html( wp_get_nav_menu_object( 'company' )->name ); ?></h4>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'company',
						)
					);
					?>
				</div>
				<div class="col-md-4">
					<?php
					$menu_name = 'plants';
					$locations = get_nav_menu_locations();
					$menu_id   = $locations[ $menu_name ];
					?>
					<h4><?php echo esc_html( wp_get_nav_menu_object( $menu_id )->name ); ?></h4>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'plants',
						)
					);
					?>
				</div>
				<div class="col-md-4">
					<h4><?php echo esc_html( wp_get_nav_menu_object( 'support' )->name ); ?></h4>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'support',
						)
					);
					?>
				</div>
			</div>
		</div>
		<div class="col-md-6 newsletter">
			<div class="row">
				<div class="col-md-12">
					<h4><?php the_field( 'bloom_newsletter_label','option' ); ?></h4>
					<?php gravity_form( get_field( 'bloom_newsletter_frm_id','option' ), false, false, false, '', true, 12 ); ?>
				</div>
			</div>
			<div class="row">
				<?php get_template_part( 'partials/footer-social-networks-module' ); ?>
			</div>
		</div>
	</div>
	<div class="copyright">
		<nav class="col-md-4 col-md-offset-4 legals">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'legals',
				)
			);
			?>
		</nav>
	</div>
</footer><!-- #colophon -->

<?php get_template_part( 'partials/signup-in-modal' ); ?>
<?php get_template_part( 'partials/search-form' ); ?>

<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php get_template_part( 'partials/chat-module' ); ?>
<?php wp_footer(); ?>

</body>
</html>
