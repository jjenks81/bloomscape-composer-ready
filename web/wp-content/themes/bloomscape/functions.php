<?php

namespace Bloomscape;

include 'vendor/autoload.php';
include 'inc/acf-fields.php';

$bloomscape        = Customizer::init();

$product_page      = WooProductPage::init();
BloomscapePDPCustomizer::init();

$product_grid_page  = BloomscapeProductGridCustomizer::init();
$bloom_api          = BloomscapeAPI::init();
$bloom_user         = NewUser::init();
$bloom_blog         = PlantCare::init();
$hubspot            = Hubspot_Api::init();

/**
 * Sanitize
 *
 * @param mixed $array Array to sanitize.
 *
 * @return mixed
 */
function sanitize_array( $array ) {
	return array_map(
		function( $item ) {
			return sanitize_text_field( $item );
		},$array
	);
}

/**
 * Woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'Bloomscape\hide_shipping_when_free_is_available', 10,2 );

/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package.
 * @param array $package The package array/object being shipped.
 * @return array of modified rates.
 */
function hide_shipping_when_free_is_available( $rates, $package ) {

	$free = [];
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id || 'flat_rate' !== $rate->method_id ) {
			$free[ $rate_id ] = $rate;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}

/**
 * Exclude hidden products from product queries
 *
 * @param WP_Query $q A WP_Query object.
 */
function action_woocommerce_product_query( $q ) {
	if ( ! is_admin() ) {
		$q->set(
			'tax_query', array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'exclude-from-catalog',
					'operator' => 'NOT IN',
				),
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'exclude-from-catalog',
					'operator' => '!=',
				),
			)
			);
	}
};

add_action( 'pre_get_posts', 'Bloomscape\action_woocommerce_product_query', 10, 1 );

/**
 * Add ZenDesk Chat widget scripts to footer.
 */
function add_zendeskchat_script() {
	$bloom_zend_desk_code = get_field( 'bloom_zend_desk_code','option' );
	if ( ! empty( $bloom_zend_desk_code ) ) {
		echo '<!-- Start of bloomscape Zendesk Widget script -->';
		echo '<script>/*<![CDATA[*/window.zE||(function(e,t,s){var n=window.zE=window.zEmbed=function(){n._.push(arguments)}, a=n.s=e.createElement(t),r=e.getElementsByTagName(t)[0];n.set=function(e){ n.set._.push(e)},n._=[],n.set._=[],a.async=true,a.setAttribute("charset","utf-8"), a.src="https://static.zdassets.com/ekr/asset_composer.js?key="+s, n.t=+new Date,a.type="text/javascript",r.parentNode.insertBefore(a,r)})(document,"script","298384c9-e592-4162-aed7-4d37d3475efe");/*]]>*/</script>';
		echo '<!-- End of bloomscape Zendesk Widget script -->';
	}
}
add_action( 'wp_footer', 'Bloomscape\add_zendeskchat_script' );

/**
 * Add Fontawesome v5.0.13
 */
function add_fontawesome() {
	wp_register_style( 'FontAwesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css' );
	wp_enqueue_style( 'FontAwesome' );
}
add_action( 'wp_footer', 'Bloomscape\add_fontawesome' );

/**
 * WooCommerce - Change Checkout Notes Label and Place Holder Text.
 *
 * @param array $fields array with all checkout fields.
 * @return array checkout fields filtered with the changes applied.
 */
function override_checkout_notes_fields( $fields ) {
	$fields['order']['order_comments']['label'] = 'Notes';
	$fields['order']['order_comments']['placeholder'] = 'Sending a gift? Add a note here.';
	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'Bloomscape\override_checkout_notes_fields' );

/**
 * Gavity Forms - function to change the content on the submit button on the newsletter sign-up form
 *
 * @param string $button The string containing the <input> tag to be filtered.
 * @param object $form The string containing the <input> tag to be filtered.
 * @return string The filtered button.
 */
function form_submit_button( $button, $form ) {
	return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><i class='fas fa-chevron-right' aria-hidden='true'></i></button>";
}
add_filter( 'gform_submit_button_1', 'Bloomscape\form_submit_button', 10, 2 );
