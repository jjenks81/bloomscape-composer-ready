<?php

namespace Bloomscape;

/**
 * Class BloomscapeAPI
 *
 * @package Bloomscape
 */
class BloomscapeAPI {

	/**
	 * Singleon instance
	 *
	 * @var BloomscapeAPI
	 */
	use Singleton;

	/**
	 * BloomscapeAPI constructor.
	 */
	public function __construct() {
		add_action(
			'rest_api_init', function() {
				register_rest_route(
					'bloomscape/v1', '/filters', [
						'methods'  => 'POST',
						'callback' => [ $this, 'get_filters' ],
					]
				);
			}
		);
	}

	/**
	 * Get filters
	 *
	 * @param \WP_REST_Request $request Request object.
	 *
	 * @return \WP_REST_Response
	 */
	public function get_filters( \WP_REST_Request $request ) {
		$data = $request->get_params();

		$return = $this->bloom_pre_shop_grid_load( $data );

		return new \WP_REST_Response( $return, 200 );
	}


	/**
	 * Parse data
	 *
	 * @param mixed $data database data.
	 *
	 * @return mixed
	 */
	private function bloom_pre_shop_grid_load( $data ) {
		$args                        = [
			'post_type'      => 'product',
			'posts_per_page' => - 1,
		];
		$args                        = $this->build_meta_query( $args, $data );
		$return['args'] = $args;
		$return['filtered_results']  = new \WP_Query( $args );
		$return['unmatched_results'] = $this->set_unmatched( $return['filtered_results'] );

		$return['filtered_results'] = $return['filtered_results']->posts;
		if ( ! empty( $return['filtered_results'] ) ) {
			foreach ( $return['filtered_results'] as $key => $post ) {
				$product                                          = wc_get_product( $post );
				$return['filtered_results'][ $key ]->product_data = [
					'price'      => $product->get_price(),
					'attributes' => $product->get_attributes(),
					'size'       => get_post_meta( $post->ID, 'plant_size', true ),
					'image'      => $product->get_image( 'shop_catalog' ),
					'link'       => $product->get_permalink(),
				];
			}
		}

		$return['unmatched_results'] = $return['unmatched_results']->posts;
		if ( ! empty( $return['unmatched_results'] ) ) {
			foreach ( $return['unmatched_results'] as $key => $post ) {
				$product                                           = wc_get_product( $post );
				$return['unmatched_results'][ $key ]->product_data = [
					'price'      => $product->get_price(),
					'attributes' => $product->get_attributes(),
					'size'       => get_post_meta( $post->ID, 'plant_size', true ),
					'image'      => $product->get_image( 'shop_catalog' ),
					'link'       => $product->get_permalink(),
				];
			}
		}

		return $return;
	}

	/**
	 * Build query
	 *
	 * @param array $args Arguments query.
	 * @param array $data Data array.
	 *
	 * @return mixed
	 */
	private function build_meta_query( $args, $data ) {
		if ( ! is_array( $data ) ) {
			return $args;
		}

		foreach ( $data as $meta_key => $key_values ) {
			$values = [];

			$args['meta_query']['relation'] = 'AND';

			if ( 'plant_light_level' === $meta_key && is_array( $key_values ) ) {

				foreach ( $key_values as $value ) {
					$args['meta_query'][] = [
						'relation' => 'OR',
						'key'     => $meta_key,
						'value'   => $value,
						'compare' => 'LIKE',
					];
				}
			} else {
				$args['meta_query'][] = [
					'key'     => $meta_key,
					'value'   => $key_values,
					'compare' => 'IN',
				];
			}
		}

		return $args;
	}

	/**
	 * Set unmatched elements
	 *
	 * @param \WP_Query $filtered_results Filtered results.
	 *
	 * @return \WP_Query
	 */
	private function set_unmatched( \WP_Query $filtered_results ) {
		$unmatched_ids = [];
		if ( $filtered_results->have_posts() ) {
			foreach ( $filtered_results->posts as $key => $post ) {
				$unmatched_ids[] = $post->ID;
			}

			return new \WP_Query(
				[
					'post_type'      => 'product',
					'post__not_in'   => $unmatched_ids,
					'posts_per_page' => - 1,
				]
			);
		}

		return new \WP_Query(
			[
				'post_type'      => 'product',
				'post__not_in'   => $unmatched_ids,
				'posts_per_page' => - 1,
			]
		);
	}

}
