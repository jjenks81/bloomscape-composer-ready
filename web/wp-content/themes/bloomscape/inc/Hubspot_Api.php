<?php

namespace Bloomscape;

/**
 * Class Hubspot_Api
 *
 * HubSpot API Integration
 *
 * @package Bloomscape
 */
class Hubspot_Api {
	const HUBAPI_URL = 'https://api.hubapi.com/';
	const HSFORMS = 'https://forms.hubspot.com/uploads/form/v2/';
	const DEALS_ENDPOINT = 'deals/v1/deal';
	const PRODUCTS_ENDPOINT = 'crm-objects/v1/objects/products';
	const CONTACTS_EMAIL_ENDPOINT = 'contacts/v1/contact/email';
	const CONTACTS_CREATEUPDATE_ENDPOINT = 'contacts/v1/contact/createOrUpdate/email';
	const HUBSPOT_PRODUCT_FIELD_ID = 'field_5a64ea4927877_1';

	/**
	 * Use Singleton for object
	 */
	use Singleton;

	/**
	 * HubSpot Integration constructor.
	 */
	public function __construct() {
		add_action( 'wp_footer', [ $this, 'hubspot_tracking_code' ] );
		add_action( 'woocommerce_thankyou', [ $this, 'contact_new_deal' ] );
		add_action( 'profile_update', [ $this, 'contact_update' ] );
		add_action( 'save_post_product', [ $this, 'create_update_product_hubspot' ], 20, 3 );

		isset( $_SESSION['customer_email'] ) || is_user_logged_in() ? add_action( 'wp_footer', [ $this, 'set_contact_identify_hubspot_cookie' ] ) : false;
		add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'update_hubspot_newsletter_option' ] );

		! empty( $this->get_newsletter_checkout_option() ) ? add_action( 'woocommerce_after_checkout_billing_form', [ $this, 'show_newsletter_option' ] ) : false;
	}

	/**
	 * Show the "Subscribe to our newsletter option" on the checkout page.
	 */
	public function show_newsletter_option() {
		?>
			<p class="form-row form-row-wide hubspot-newsletter">
			<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="hubspot_woocommerce_newsletter" type="checkbox" name="hubspot_woocommerce_newsletter" value="1">
			<label for="hubspot_woocommerce_newsletter" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline"><span>Subscribe to our newsletter</span></label>
			</p>
			<div class="clear"></div>
		<?php
	}

	/**
	 * Show the "Subscribe to our newsletter option" on the checkout page.
	 *
	 * @param int $order_id the ID of the new order.
	 */
	public function update_hubspot_newsletter_option( $order_id ) {
		isset( $_POST['hubspot_woocommerce_newsletter'] ) ? update_post_meta( $order_id, 'hubspot_woocommerce_newsletter', esc_attr( wp_unslash( $_POST['hubspot_woocommerce_newsletter'] ) ) ) : false;  // phpcs:ignore

		$contact_data = $this->get_customer_data_by_order( $order_id );

		$this->add_contact_newsletter( $contact_data );
	}

	/**
	 * Add HubSpot tracking code
	 */
	public function hubspot_tracking_code() {
		$hubspot_tracking_code = get_field( 'hubspot_tracking_code','option' );

		if ( ! empty( $hubspot_tracking_code ) ) {
			echo $hubspot_tracking_code; // phpcs:ignore
		}
	}

	/**
	 * Get the HubSpot API key
	 *
	 * @return string with the API key.
	 */
	public function get_hapikey() {
		$hapikey = get_field( 'hubspot_api_key', 'option' );
		return $hapikey;
	}

	/**
	 * Get the HubSpot Portal ID
	 *
	 * @return string Portal ID.
	 */
	public function get_portal_id() {
		$portal_id = get_field( 'hubspot_portal_id', 'option' );
		return $portal_id;
	}

	/**
	 * Get the HubSpot Newsletter sign-up checkout option
	 *
	 * @return string sign-up checkout option
	 */
	public function get_newsletter_checkout_option() {
		$newsletter_checkout_option = get_field( 'hubspot_newsletter_option', 'option' );
		return $newsletter_checkout_option;
	}

	/**
	 * Get the HubSpot Newsletter sign-up form ID
	 *
	 * @return string sign-up form ID.
	 */
	public function get_newsletter_form_id() {
		$newsletter_form_id = get_field( 'hubspot_newsletter_form_id', 'option' );
		return $newsletter_form_id;
	}

	/**
	 * Install ecommerce settings HubSpot
	 */
	public function install_ecommerce() {
		$arr = [];
		$post_json = wp_json_encode( $arr );
		$endpoint = 'https://api.hubapi.com/extensions/ecomm/v1/installs?hapikey=' . $this->get_hapikey();
		$this->http( $endpoint,$post_json );
	}

	/**
	 * Enabled ecommerce settings HubSpot
	 */
	public function settings_enabled() {
		$arr = [
			'enabled' => true,
		];
		$post_json = wp_json_encode( $arr );
		$endpoint = 'https://api.hubapi.com/extensions/ecomm/v1/settings?hapikey=' . $this->get_hapikey();
		$this->http( $endpoint,$post_json );
	}

	/**
	 * Create new contact deal on HubSpot
	 *
	 * @param int $order_id the ID of the new order.
	 */
	public function contact_new_deal( $order_id ) {
		$contact_data = $this->get_customer_data_by_order( $order_id );

		$this->hubspot_create_or_update_contact( $contact_data );

		$hubspot_contact_data = $this->hubspot_get_contact( $contact_data['email'] );

		$deal_data = $this->get_deal_data_by_order( $order_id );

		$_SESSION['customer_email'] = $contact_data['email'];

		$arr = [
			'associations' => [
				'associatedVids' => [
					$hubspot_contact_data->vid,
				],
			],
			'properties' => [
				[
					'value' => $deal_data['dealname'],
					'name' => 'dealname',
				],
				[
					'value' => $deal_data['dealstage'],
					'name' => 'dealstage',
				],
				[
					'value' => $deal_data['amount'],
					'name' => 'amount',
				],
				[
					'value' => '75e28846-ad0d-4be2-a027-5e1da6590b98',
					'name' => 'pipeline',
				],
				[
					'value' => $deal_data['order_number'],
					'name' => 'order_number',
				],
				[
					'value' => $deal_data['sent_gift'],
					'name' => 'sent_gift',
				],
				[
					'value' => $deal_data['products_purchased'],
					'name' => 'products_purchased',
				],
				[
					'value' => $contact_data['total_orders'],
					'name' => 'total_orders',
				],
			],
		];

		if ( ! empty( $deal_data['coupons_used'] ) ) {
			$coupons_used = [
				'value' => $deal_data['coupons_used'],
				'name' => 'coupons_used',
			];

			array_push( $arr['properties'],$coupons_used );
		}

		if ( ! empty( $contact_data['total_revenue'] ) ) {
			$total_revenue = [
				'value' => $contact_data['total_revenue'],
				'name' => 'total_revenue',
			];

			array_push( $arr['properties'],$total_revenue );
		}

		if ( ! empty( $contact_data['first_purchase_date'] ) ) {
			$first_purchase_date = [
				'value' => $contact_data['first_purchase_date'],
				'name' => 'first_purchase_date',
			];

			array_push( $arr['properties'],$first_purchase_date );
		}

		if ( ! empty( $contact_data['last_purchase_date'] ) ) {
			$last_purchase_date = [
				'value' => $contact_data['last_purchase_date'],
				'name' => 'last_purchase_date',
			];

			array_push( $arr['properties'],$last_purchase_date );
		}

		if ( ! empty( $contact_data['average_order_value'] ) ) {
			$average_order_value = [
				'value' => $contact_data['average_order_value'],
				'name' => 'average_order_value',
			];

			array_push( $arr['properties'],$average_order_value );
		}

		$endpoint = sprintf( '%s%s?hapikey=%s', self::HUBAPI_URL, self::DEALS_ENDPOINT, $this->get_hapikey() );
		$deal_data_json = json_encode( $arr );
		$ch = curl_init( $endpoint );

		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $deal_data_json );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt(
			$ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json',
				'Content-Length: ' . strlen( $deal_data_json ),
			]
		);

		$result = curl_exec( $ch );
	}

	/**
	 * Get customer data by the order ID
	 *
	 * @param int $order_id the ID of the order.
	 *
	 * @return array
	 */
	public function get_customer_data_by_order( $order_id ) {
		$order = new \WC_Order( $order_id );

		$contact_total_orders = count( $this->get_all_customer_orders_by_email( $order->get_billing_email() ) );

		$contact_average = $this->get_contact_average( $order->get_billing_email() );

		$contact_revenue = $this->get_contact_revenue( $order->get_billing_email() );

		$customer_first_purchase_date = $this->customer_first_purchase_date( $order->get_billing_email() );

		$customer_last_purchase_date = $this->customer_last_purchase_date( $order->get_billing_email() );

		$contact_data = [
			'email'               => $order->get_billing_email(),
			'firstname'           => $order->get_billing_first_name(),
			'lastname'            => $order->get_billing_last_name(),
			'phone'               => $order->get_billing_phone(),
			'company'             => $order->get_billing_company(),
			'address'             => $order->get_billing_address_1(),
			'zip'                 => $order->get_billing_postcode(),
			'state'               => $order->get_billing_state(),
			'country'             => $order->get_billing_country(),
			'total_orders'        => $contact_total_orders,
			'total_revenue'       => $contact_revenue,
			'average_order_value' => $contact_average,
			'first_purchase_date' => $customer_first_purchase_date,
			'last_purchase_date'  => $customer_last_purchase_date,
		];

		return $contact_data;
	}

	/**
	 * Set order data from order
	 *
	 * @param int $order_id the ID of the order.
	 *
	 * @return array
	 */
	public function get_deal_data_by_order( $order_id ) {
		$order = new \WC_Order( $order_id );

		$products_purchased = $this->get_order_items( $order_id );

		$deal_data = [
			'order_number'       => $order_id,
			'sent_gift'          => $this->is_gift_order( $order_id ),
			'products_purchased' => $products_purchased,
			'coupons_used'       => $this->get_coupons_order( $order_id ),
			'dealname'           => 'Order #' . $order_id,
			'dealstage'          => 'checkout_completed',
			'amount'             => $order->get_total(),
		];

		return $deal_data;
	}

	/**
	 * Update contact data on HubSpot on user profile update
	 */
	public function contact_update() {
		$customer = new \WC_Customer( wp_get_current_user()->ID );

		$contact_total_orders = count( $this->get_all_customer_orders_by_email( wp_get_current_user()->ID ) );

		$contact_data = [
			'email'     => $customer->billing_email,
			'firstname' => $customer->billing_first_name,
			'lastname'  => $customer->billing_last_name,
		];

		$customer->billing_phone ? $contact_data['phone']       = $customer->billing_phone : false;
		$customer->billing_address_1 ? $contact_data['address'] = $customer->billing_address_1 : false;
		$customer->billing_postcode ? $contact_data['zip']      = $customer->billing_postcode : false;
		$customer->billing_state ? $contact_data['state']       = $customer->billing_state : false;
		$customer->billing_company ? $contact_data['company']   = $customer->billing_company : false;
		$customer->billing_country ? $contact_data['country']   = $customer->billing_country : false;

		$this->hubspot_create_or_update_contact( $contact_data );
	}

	/**
	 * Get all the items names inside an order
	 *
	 * @param int $order_id the ID of the order.
	 *
	 * @return array
	 */
	public function get_order_items( $order_id ) {
		$order = new \WC_Order( $order_id );

		$order_items = $order->get_items();
		$products_purchased = [];

		foreach ( $order_items as $item ) {
			array_push( $products_purchased, $item->get_name() );
		}

		$products_purchased = implode( '; ', $products_purchased );

		return $products_purchased;
	}

	/**
	 * Validate if order is a gift
	 *
	 * @param int $order_id the ID of the order.
	 * @return string
	 */
	public function is_gift_order( $order_id ) {
		return get_post_meta( $order_id, 'giftpack', true ) ? 'Yes' : 'No';
	}

	/**
	 * Get the coupon used on the order
	 *
	 * @param int $order_id the ID of the order.
	 * @return string
	 */
	public function get_coupons_order( $order_id ) {
		$order        = new \WC_Order( $order_id );
		$used_coupons = $order->get_used_coupons();
		$used_coupons = implode( ',', $used_coupons );

		return $used_coupons;
	}

	/**
	 * Create or update a contact on HubSpot
	 *
	 * @param array $contact_data contact data.
	 */
	public function hubspot_create_or_update_contact( $contact_data ) {
		$arr = [
			'properties' => [
				[
					'property' => 'firstname',
					'value'    => $contact_data['firstname'],
				],
				[
					'property' => 'lastname',
					'value'    => $contact_data['lastname'],
				],
				[
					'property' => 'phone',
					'value'    => $contact_data['phone'],
				],
				[
					'property' => 'company',
					'value'    => $contact_data['company'],
				],
				[
					'property' => 'address',
					'value'    => $contact_data['address'],
				],
				[
					'property' => 'zip',
					'value'    => $contact_data['zip'],
				],
				[
					'property' => 'state',
					'value'    => $contact_data['state'],
				],
				[
					'property' => 'country',
					'value'    => $contact_data['country'],
				],
				[
					'property' => 'total_orders',
					'value'    => $contact_data['total_orders'],
				],
				[
					'property' => 'products_purchased',
					'value'    => $contact_data['order_total_items'],
				],
			],
		];

		$endpoint = sprintf( '%s%s/%s/?hapikey=%s', self::HUBAPI_URL, self::CONTACTS_CREATEUPDATE_ENDPOINT, $contact_data['email'], $this->get_hapikey() );
		$post_json = wp_json_encode( $arr );
		$this->http( $endpoint, $post_json );
	}

	/**
	 * Get the contact data from hubspot
	 *
	 * @param string $email contact email.
	 * @return array
	 */
	public function hubspot_get_contact( $email ) {
		$endpoint = sprintf( '%s%s/%s/profile?hapikey=%s', self::HUBAPI_URL, self::CONTACTS_EMAIL_ENDPOINT, $email, $this->get_hapikey() );
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_URL, $endpoint );
		$result = curl_exec( $ch );
		curl_close( $ch );
		$contact_data = json_decode( $result );

		return $contact_data;
	}

	/**
	 * Get the first purchase date from contact
	 *
	 * @param string $customer_email contact email.
	 * @return mixed
	 */
	public function customer_first_purchase_date( $customer_email ) {
		$customer_orders = $this->get_all_customer_orders_by_email( $customer_email );
		if ( ! empty( $customer_orders ) ) {
			$first_order = end( $customer_orders );

			return ! empty( $first_order ) ? $this->convert_date_to_utc_midnight( $first_order->post_date ) : false;
		} else {
			return false;
		}
	}

	/**
	 * Get the last purchase date from contact
	 *
	 * @param string $customer_email contact email.
	 * @return mixed
	 */
	public function customer_last_purchase_date( $customer_email ) {
		$customer_orders = $this->get_all_customer_orders_by_email( $customer_email );
		if ( ! empty( $customer_orders ) ) {
			$last_order = current( $customer_orders );

			return ! empty( $last_order ) ? $this->convert_date_to_utc_midnight( $last_order->post_date ) : false;
		} else {
			return false;
		}
	}

	/**
	 * Convert date to UTC midnight timestamp
	 *
	 * @param string $date date to convert.
	 * @return mixed
	 */
	public function convert_date_to_utc_midnight( $date ) {
		$date = date( 'd-m-Y', strtotime( $date ) );

		$date = new \DateTime( $date );
		$date->setTimezone( timezone_open( 'UTC' ) );
		$date->modify( 'midnight' );
		$date->setTime( 0,0,0 );

		return $date->format( 'UHi' ) . PHP_EOL;
	}

	/**
	 * Get all the orders from customer
	 *
	 * @param string $customer_email customer email.
	 * @return mixed
	 */
	public function get_all_customer_orders_by_email( $customer_email ) {
		$customer_data = get_user_by( 'email', $customer_email );

		if ( ! empty( $customer_data ) ) {
			$customer_orders = get_posts(
				[
					'meta_key'    => '_customer_user',
					'meta_value'  => $customer_data->ID,
					'post_type'   => wc_get_order_types(),
					'post_status' => array_keys( wc_get_order_statuses() ),
				]
			);

			return $customer_orders;
		} else {
			return false;
		}
	}

	/**
	 * Get contact average based on the total orders amount / total orders
	 *
	 * @param string $customer_email customer email.
	 * @return mixed
	 */
	public function get_contact_average( $customer_email ) {
		$customer_orders = $this->get_all_customer_orders_by_email( $customer_email );

		if ( is_array( $customer_orders ) || is_object( $customer_orders ) ) {
			foreach ( $customer_orders as $customer_order ) {
				$order = new \WC_Order( $customer_order->ID );
				$total_orders_amount = $total_orders_amount + $order->get_total();
			}

			$contact_average = $total_orders_amount / count( $customer_orders );

			return $contact_average;
		} else {
			return false;
		}
	}

	/**
	 * Get contact total revenue
	 *
	 * @param string $customer_email customer email.
	 * @return mixed
	 */
	public function get_contact_revenue( $customer_email ) {
		$customer_orders = $this->get_all_customer_orders_by_email( $customer_email );

		if ( is_array( $customer_orders ) || is_object( $customer_orders ) ) {
			foreach ( $customer_orders as $customer_order ) {
				$order = new \WC_Order( $customer_order->ID );
				$contact_revenue = $contact_revenue + $order->get_total();
			}

			return $contact_revenue;
		} else {
			return false;
		}
	}

	/**
	 * Request for HubSpot
	 *
	 * @param string $endpoint HubSpot endpoint.
	 * @param array  $post_json contact data.
	 */
	public function http( $endpoint, $post_json ) {
		$result = wp_remote_post(
			$endpoint,
			[
				'sslverify' => false,
				'timeout' => 15,
				'body' => $post_json,
			]
		);
	}

	/**
	 * Set identity of the customer for HubsPot tracking
	 */
	public function set_contact_identify_hubspot_cookie() {
		?>
		<script>
			var _hsq = window._hsq = window._hsq || [];
			_hsq.push(["identify",{
				email: '<?php echo esc_html( ( ( isset( $_SESSION['customer_email'] ) ) ? $_SESSION['customer_email'] : wp_get_current_user()->user_email ) ); ?>'
			}]);
		</script>
		<?php
	}

	/**
	 * Adding the customer email to the Newsletter sign-up form
	 *
	 * @param array $contact_data contact data.
	 */
	public function add_contact_newsletter( $contact_data ) {
		$hubspotutk      = array_key_exists( 'hubspotutk', $_COOKIE ) ? isset( $_COOKIE['hubspotutk'] ) ? sanitize_text_field( wp_unslash( $_COOKIE['hubspotutk'] ) ) : false : false;
		$ip_addr         = array_key_exists( 'REMOTE_ADDR', $_SERVER ) ? isset( $_SERVER['REMOTE_ADDR'] ) ? sanitize_text_field( wp_unslash( $_SERVER['REMOTE_ADDR'] ) ) : false : false;
		$hs_context      = [
			'hutk' => $hubspotutk,
			'ipAddress' => $ip_addr,
			'pageUrl' => ( 'on' === isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", // phpcs:ignore
			'pageName' => 'Checkout page',
		];
		$hs_context_json = json_encode( $hs_context );

		$str_post = 'firstname=' . $contact_data['firstname']
			. '&lastname=' . $contact_data['lastname']
			. '&email=' . $contact_data['email']
			. '&phone=' . $contact_data['phone']
			. '&hs_context=' . urlencode( $hs_context_json );

		$endpoint = self::HSFORMS . $this->get_portal_id() . '/' . $this->get_newsletter_form_id();

		$ch = @curl_init();
		@curl_setopt( $ch, CURLOPT_POST, true );
		@curl_setopt( $ch, CURLOPT_POSTFIELDS, $str_post );
		@curl_setopt( $ch, CURLOPT_URL, $endpoint );
		@curl_setopt(
			$ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/x-www-form-urlencoded',
			]
		);
		@curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$response    = @curl_exec( $ch );
		$status_code = @curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		@curl_close( $ch );
	}

	/**
	 * Create or update a product on HubSpot.
	 *
	 * @param int $product_id the product ID.
	 */
	public function create_update_product_hubspot( $product_id ) {
		if ( 0 === get_field( 'add_product_hubspot', $product_id ) ) {
			return false;
		}

		$wc_product = wc_get_product( $product_id );

		$product = [
			[
				'name' => 'name',
				'value' => $wc_product->get_title(),
			],
			[
				'name' => 'description',
				'value' => sanitize_text_field( $wc_product->get_short_description() ),
			],
			[
				'name' => 'price',
				'value' => wp_unslash( $_POST['_regular_price'] ), // phpcs:ignore
			],
		];

		$product_data_json = json_encode( $product );

		if ( ( ! empty( get_field( 'hubspot_product_id', $product_id ) ) || ! empty( get_post_meta( $product_id, 'hubspot_product_id_new', true ) )) && empty( $_POST[ 'acf[ ' . self::HUBSPOT_PRODUCT_FIELD_ID . ' ]' ] ) ) {
			$hubspot_product_id = ! empty( get_field( 'hubspot_product_id', $product_id ) ) ? get_field( 'hubspot_product_id', $product_id ) : get_post_meta( $product_id, 'hubspot_product_id_new', true );
			$endpoint = sprintf( '%s%s/%s?hapikey=%s', self::HUBAPI_URL, self::PRODUCTS_ENDPOINT , $hubspot_product_id, $this->get_hapikey() );
			$request_metod = 'PUT';
		} else {
			$endpoint = sprintf( '%s%s?hapikey=%s', self::HUBAPI_URL, self::PRODUCTS_ENDPOINT , $this->get_hapikey() );
			$request_metod = 'POST';
		}

		$args = [
			'method'  => $request_metod,
			'headers' => [
				'Content-type' => 'application/json',
			],
			'body'    => $product_data_json,
		];

		$response           = wp_remote_request( $endpoint, $args );
		$decode             = json_decode( $response['body'] );
		$hubspot_product_id = $decode->objectId; // phpcs:ignore

		update_post_meta( $product_id, 'hubspot_product_id_new', $hubspot_product_id );
	}
}

acf_add_local_field_group(
	array(
		'key' => 'group_5a64ea2c9f18_1',
		'title' => 'HubSpot Product mapping',
		'fields' => array(
			array(
				'key' => 'field_5a64ea4927877_0',
				'label' => 'Add this product to HubSpot?',
				'name' => 'add_product_hubspot',
				'type' => 'radio',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'1' => 'Yes',
					'0' => 'No',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '1',
				'layout' => 'vertical',
				'return_format' => 'value',
			),
			array(
				'key' => 'field_5a64ea4927877_1',
				'label' => 'HubSpot product ID',
				'name' => 'hubspot_product_id',
				'type' => 'text',
				'instructions' => 'If this product is already on HubSpot, put their ID here',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5a64ea4927877_0',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 2,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	)
);
