<?php

if ( function_exists( 'acf_add_local_field_group' ) ) :

	// Page Specific ACF.
	require_once 'acf-fields/acf-product.php';
	require_once 'acf-fields/acf-home.php';
	require_once 'acf-fields/acf-landing.php';

	require_once 'acf-fields/modules/acf-plant-care-hero.php';
	require_once 'acf-fields/modules/acf-plant-care-image.php';

	// Module Specific ACF (can be applied to multiple pages).
	require_once 'acf-fields/modules/acf-gift-card-design.php';
	require_once 'acf-fields/modules/acf-image-text-module.php';
	require_once 'acf-fields/modules/acf-hero-image-module.php';
	require_once 'acf-fields/modules/acf-title-box-module.php';
	require_once 'acf-fields/modules/acf-footer-hero-image-module.php';
	require_once 'acf-fields/modules/acf-side-image-module.php';
	require_once 'acf-fields/modules/acf-faq-module.php';

	// Global configuration.
	require_once 'acf-fields/global/acf-people-reviews.php';
	require_once 'acf-fields/global/acf-customer-reviews.php';
	require_once 'acf-fields/global/acf-footer-config.php';
	require_once 'acf-fields/global/acf-plants-made-simple.php';
	require_once 'acf-fields/global/acf-shop-all-size.php';
	require_once 'acf-fields/global/acf-zenddesk-chat.php';
	require_once 'acf-fields/global/acf-hubspot.php';

endif;
