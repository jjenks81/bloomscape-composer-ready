<?php
/**
 * Created by PhpStorm.
 * User: roysivan
 * Date: 1/21/18
 * Time: 2:36 PM
 */

namespace Bloomscape;


/**
 * Class WooProductCustomAttrs
 *
 * @package Bloomscape
 */
class WooProductCustomAttrs {

	/**
	 * Custom Attrs
	 *
	 * @var array
	 */
	private $custom_attrs;

	/**
	 * Singleton Instance
	 *
	 * @var WooProductCustomAttrs
	 */
	use Singleton;

	/**
	 * WooProductCustomAttrs constructor.
	 *
	 * @param int $prod_id Post ID.
	 */
	public function __construct( $prod_id ) {
		$this->custom_attrs = [
			'plant_size'               => false,
			'plant_size_label'         => false,
			'plant_difficulty'         => false,
			'plant_difficulty_label'   => false,
			'plant_light_level'        => false,
			'plant_light_label'        => false,
			'plant_pet_friendly'       => false,
			'plant_pet_friendly_label' => false,
			'plant_air_cleaner'        => false,
			'plant_air_cleaner_label'  => false,
		];
	}

	/**
	 * Initialize attrs
	 *
	 * @return string
	 */
	public function init_attrs_detail() {
		global $post;

		foreach ( $this->custom_attrs as $attr => $value ) {
			$value = get_field( $attr, $post->ID );
			$this->custom_attrs[ $attr ] = ( $value ) ? $value : false;
		}

		return $this->build_custom_attr_template();

	}

	/**
	 * Product short description
	 *
	 * @return string
	 */
	public function product_short_description() {
		global $post;

		$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
		$short_description .= '<div class="bottom-helper"></div>';

		return $short_description;
	}

	/**
	 * Build custom attribute template
	 *
	 * @return string
	 */
	private function build_custom_attr_template() {
		$template = '<div id="plant-custom-attrs-wrapper" class="plant-custom-attrs-wrapper">';
		foreach ( $this->custom_attrs as $key => $value ) {
			$template .= '<div class="plant-custom-attr">';
			$template .= $this->build_custom_attr_row( $key, $value );
			$template .= '</div>';
		}
		$template .= '</div>';

		return $template;
	}

	/**
	 * Build Custom attr template
	 *
	 * @param string $attr Attributes.
	 * @param string $value Value.
	 *
	 * @return string
	 */
	private function build_custom_attr_row( $attr, $value ) {
		$row = '<div class="plant-custom-attr-title">';
		$row .= $this->get_attr_title( $attr );
		$row .= '</div>';
		$row .= '<div class="plant-custom-attr-value">';
		$row .= $this->get_attr_value( $attr, $value );
		$row .= '</div>';

		return $row;
	}

	/**
	 * Get Title
	 *
	 * @param string $attr Attributes.
	 *
	 * @return string
	 */
	private function get_attr_title( $attr ) {

		// If label, return.
		if ( false !== strpos( $attr, '_label' ) ) {
			return '';
		}

		// Get title for attribute.
		switch ( $attr ) {
			case 'plant_size':
				return 'Size';
			case 'plant_difficulty':
				return 'Difficulty';
			case 'plant_light_level':
				return 'Light';
			case 'plant_pet_friendly':
				return 'Pet Friendly';
			case 'plant_air_cleaner':
				return 'Air Cleaner';
			default:
				return '';
		}
	}

	/**
	 * Get Value from Array
	 *
	 * @param string $attr Attribute.
	 * @param string $value Value.
	 *
	 * @return string
	 */
	private function get_attr_value( $attr, $value ) {
		// If label, return value.
		if ( false !== strpos( $attr, '_label' ) ) {
			return $value;
		}

		switch ( $attr ) {
			case 'plant_size':
				return $this->parse_size_values( $value );
			case 'plant_difficulty':
				return $this->parse_difficulty_values( $value );
			case 'plant_light_level':
				return $this->parse_light_values( $value );
			case 'plant_pet_friendly':
				return $this->parse_boolean_values( 'plant-pet-value', $value );
			case 'plant_air_cleaner':
				return $this->parse_boolean_values( 'plant-air-cleaner-value', $value );
			default:
				return '';
		}
	}

	/**
	 * Parse Size Values
	 *
	 * @param strint $value Value.
	 *
	 * @return string
	 */
	private function parse_size_values( $value ) {
		$choices = [
			'mini'   => 'MINI',
			'xsmall' => 'XS',
			'small'  => 'S',
			'medium' => 'M',
			'large'  => 'L',
			'xlarge' => 'XL',
		];

		$options = '<div class="plant-size-value value-' . $value . '">';
		foreach ( $choices as $key => $choice ) {
			$active  = ( $key === $value ) ? ' active' : '';
			$options .= '<span class="' . $key . '-size' . $active . '">' . $choice . '</span>';
		}
		$options .= '</div>';

		return $options;
	}

	/**
	 * Parse difficulty values
	 *
	 * @param string $value Value.
	 *
	 * @return string
	 */
	private function parse_difficulty_values( $value ) {
		$choices = [
			'1',
			'2',
			'3',
		];

		$options = '<div class="plant-difficulty-value value-' . $value . '">';
		foreach ( $choices as $choice ) {
			$active  = ( $choice === $value ) ? ' active' : '';
			$options .= '<span class="level-' . $choice . $active . '"></span>';
		}
		$options .= '</div>';

		return $options;
	}

	/**
	 * Parse Light Value
	 *
	 * @param array $values Value.
	 *
	 * @return string
	 */
	private function parse_light_values( $values ) {
		$choices = [
			'none',
			'partial',
			'full',
		];

		$classes = '';
		foreach ( $values as $value ) {
			$classes .= ' value-' . $value;
		}

		$options = '<div class="plant-light-value' . $classes . '">';
		foreach ( $choices as $choice ) {
			$active  = ( in_array( $choice, $values, true ) ) ? ' active' : '';
			$options .= '<span class="level-' . $choice . $active . '"></span>';
		}
		$options .= '</div>';

		return $options;
	}

	/**
	 * Parse boolean values
	 *
	 * @param string $div_wrapper css class wrapper.
	 * @param string $value Value.
	 *
	 * @return string
	 */
	private function parse_boolean_values( $div_wrapper, $value ) {
		$choices = [
			0 => 'N',
			1 => 'Y',
		];

		$options = '<div class="boolean-selector ' . $div_wrapper . ' value-' . $value . '">';
		foreach ( $choices as $key => $choice ) {
			$active  = ( $key === (int) $value ) ? ' active' : '';
			$options .= '<span class="level-' . $choice . $active . '">' . $choice . '</span>';
		}
		$options .= '</div>';

		return $options;
	}


}
