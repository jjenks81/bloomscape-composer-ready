<?php

namespace Bloomscape;

global $filtered_results, $unmatched_query;

/**
 * Class BloomscapeSidebarFilter
 *
 * @package Bloomscape
 */
class BloomscapeSidebarFilter {

	/**
	 * Singleton instance
	 *
	 * @var BloomscapeSidebarFilter
	 */
	use Singleton;

	/**
	 * Plant Attributes
	 *
	 * @var $attributes
	 */
	private $attributes;
	/**
	 * Flag to activate filters
	 *
	 * @var bool
	 */
	private $init_filters;

	/**
	 * BloomscapeSidebarFilter constructor.
	 */
	public function __construct() {
		$this->set_attrs();
		$this->init_filters = false;
		if (
			isset( $_POST ) ||
			get_query_var( 'plant_size' ) ||
			get_query_var( 'plant_difficulty' ) ||
			get_query_var( 'plant_light_level' ) ||
			get_query_var( 'plant_pet_friendly' ) ||
			get_query_var( 'plant_air_cleaner' )
		) {
			add_action( 'wp', [ $this, 'set_current_attrs' ] );
		}

		add_action( 'wp', [ $this, 'bloom_pre_shop_grid_load' ] );
		add_filter( 'post_class', [ $this, 'bloom_post_class_product' ] );

		// Rewrite tags & Rules!
		add_action( 'init', [ $this, 'bloom_filters_write_tags' ] );
		add_action( 'init', [ $this, 'bloom_filters_rewrite_rules' ] );
	}

	/**
	 * Set Attributes, should match ACF Fields for WOO Products (in acf-fields.php)
	 */
	private function set_attrs() {
		$this->attributes = [
			[
				'label'         => 'Plant Size',
				'field_name'    => 'plant_size',
				'field_options' => [
					[
						'label' => 'Mini',
						'value' => 'mini',
					],
					[
						'label' => 'Extra Small',
						'value' => 'xsmall',
					],
					[
						'label' => 'Small',
						'value' => 'small',
					],
					[
						'label' => 'Medium',
						'value' => 'medium',
					],
					[
						'label' => 'Large',
						'value' => 'large',
					],
					[
						'label' => 'Extra Large',
						'value' => 'xlarge',
					],
				],
			],
			[
				'label'         => 'Difficulty',
				'field_name'    => 'plant_difficulty',
				'field_options' => [
					[
						'label' => 'Care Free',
						'value' => 1,
					],
					[
						'label' => 'Easy',
						'value' => 2,
					],
					[
						'label' => 'Moderate',
						'value' => 3,
					],
				],
			],
			[
				'label'         => 'Light Level',
				'field_name'    => 'plant_light_level',
				'field_options' => [
					[
						'label' => 'Low light',
						'value' => 'none',
					],
					[
						'label' => 'Medium light',
						'value' => 'partial',
					],
					[
						'label' => 'Bright light',
						'value' => 'full',
					],
				],
			],
			[
				'label'         => 'Pet Friendly',
				'field_name'    => 'plant_pet_friendly',
				'field_options' => [
					[
						'label' => 'Yes',
						'value' => '1',
					],
				],
			],
			[
				'label'         => 'Air Cleaner',
				'field_name'    => 'plant_air_cleaner',
				'field_options' => [
					[
						'label' => 'Yes',
						'value' => '1',
					],
				],
			],

		];
	}

	/**
	 * Set the attributes current value from POST or query_var data
	 */
	public function set_current_attrs() {
		foreach ( $this->attributes as $key => $attr ) {
			if ( isset( $_POST[ $attr['field_name'] ] ) ) {
				// Set Attribute from POST data.
				if ( ! is_array( $_POST[ $attr['field_name'] ] ) ) {
					$this->init_filters                        = true;
					$this->attributes[ $key ]['current_value'] = sanitize_text_field( wp_unslash( $_POST[ $attr['field_name'] ] ) );
				} else {
					foreach ( sanitize_array( wp_unslash( $_POST[ $attr['field_name'] ] ) ) as $value ) {
						$this->init_filters                                  = true;
						$this->attributes[ $key ]['current_value'][ $value ] = true;
					}
				}
			} else {
				// Set Attribute from query_var.
				$query_var = get_query_var( $attr['field_name'] );
				if ( $query_var ) {
					$values = explode( '-', $query_var );
					foreach ( $values as $value ) {
						$this->init_filters                                  = true;
						$this->attributes[ $key ]['current_value'][ $value ] = true;
					}
				}
			}
		}
	}

	/**
	 * Filter Form
	 *
	 * @return string
	 */
	public function sidebar_filters() {
		$filter_form = '<form class="bloom_woo_filters" id="bloom_woo_filters" method="post" action="' . get_permalink( wc_get_page_id( 'shop' ) ) . '"';
		$filter_form .= ( $this->init_filters ) ? 'data-init="true"' : 'data-init="false"';
		$filter_form .= ' >';
		$filter_form .= $this->build_form_fields();
		$filter_form .= $this->size_guide_button();
		$filter_form .= '</form>';

		return $filter_form;
	}

	/**
	 * Size Guide
	 *
	 * @return string
	 */
	public function size_guide_button() {
		$info       = get_field( 'bloom_size_guide', 'options' );
		$size_guide = '<div class="size-guide-helper">';
		$size_guide .= '<a href="#openSizeModal" data-toggle="modal" data-target="#myModal">' . $info['bloom_size_guide_label'] . '</a>';
		$size_guide .= '</div>';
		$size_guide .= $this->build_modal( $info );

		return $size_guide;
	}

	/**
	 * Input block for each attribute
	 *
	 * @return string
	 */
	private function build_form_fields() {
		$fields = '';
		foreach ( $this->attributes as $key => $field ) {
			$collapsed = ( ! isset( $field['current_value']['any'] ) ) ? ( ! isset( $field['current_value'] )) : true;
			$fields .= '<div class="form-group bloom-filter">';
			$fields .= '<h4><a class="collapsed filter-' . strtolower( str_replace( ' ', '-', $field['label'] ) ) . '" data-toggle="collapse" data-parent="#bloom_woo_filters" href="#collapse_' . $key . '">' . $field['label'] . '</a></h4>';
			$fields .= '<div class="single-filter-wrapper collapse ';
			$fields .= ( ! $collapsed ) ? 'in' : '';
			$fields .= '" id="collapse_' . $key . '">';
			foreach ( $field['field_options'] as $option ) {
				$fields .= '<label for="' . $field['label'] . '_' . $option['label'] . '" class="custom-bloom-checkbox">' . $option['label'];
				$fields .= '<input name="' . $field['field_name'] . '" type="checkbox" id="' . $field['label'] . '_' . $option['label'] . '" value="' . $option['value'] . '"';
				$fields .= ( isset( $field['current_value'] ) && array_key_exists(
					$option['value'],
					$field['current_value']
				) ) ? 'checked' : '';
				$fields .= '><span class="checkmark"></span></label>';
			}
			$fields .= '</div>';
			$fields .= '</div>';
		}

		return $fields;
	}

	/**
	 * Load Filtered and Unmatched before load - Only needed for non AJAX
	 */
	public function bloom_pre_shop_grid_load() {
		global $filtered_results, $unmatched_query;
		if ( isset( $_POST, $_POST['filters_applied'] ) ) {
			$args             = [
				'post_type' => 'product',
			];
			$args             = $this->build_meta_query( $args );
			$filtered_results = new \WP_Query( $args );
			$unmatched_query  = $this->set_unmatched( $filtered_results );
		}
	}

	/**
	 * Build Meta Query for Filtered Results - DUPLICATED IN API
	 *
	 * @param mixed $args query arguments.
	 *
	 * @return mixed
	 */
	private function build_meta_query( $args ) {
		$meta_query_start = false;
		foreach ( $this->attributes as $key => $attr ) {
			if ( isset( $attr['current_value'] ) && 'any' !== $attr['current_value'] ) {

				$values = [];
				if ( is_array( $attr['current_value'] ) ) {
					foreach ( $attr['current_value'] as $attr_value => $true ) {
						$values[] = $attr_value;
					}
				}

				if ( ! $meta_query_start ) {
					$args['meta_query']['relation'] = 'OR';
					$meta_query_start               = true;
				}
				$args['meta_query'][] = [
					'key'     => $attr['field_name'],
					'value'   => $values,
					'compare' => 'IN',
				];
			}
		}

		return $args;
	}

	/**
	 * Create unmatched query - DUPLICATED IN API
	 *
	 * @param \WP_Query $filtered_results Results filtered.
	 *
	 * @return \WP_Query
	 */
	private function set_unmatched( \WP_Query $filtered_results ) {
		$unmatched_ids = [];
		if ( $filtered_results->have_posts() ) {
			foreach ( $filtered_results->posts as $key => $post ) {
				$unmatched_ids[] = $post->ID;
			}

			return new \WP_Query(
				[
					'post_type'    => 'product',
					'post__not_in' => $unmatched_ids,
				]
			);
		} else {
			return new \WP_Query(
				[
					'post_type'    => 'product',
					'post__not_in' => $unmatched_ids,
				]
			);
		}
	}

	/**
	 * Add Class for unmatched
	 *
	 * @param mixed $classes Apply unmatched classes if apply.
	 *
	 * @return array
	 */
	public function bloom_post_class_product( $classes ) {
		global $post, $unmatched_query;

		if ( is_a( $unmatched_query, 'WP_Query' ) ) {
			foreach ( $unmatched_query->posts as $unmpost ) {
				$classes[] = ( $unmpost->ID === $post->ID ) ? 'unmatched' : '';
			}
		}

		return $classes;
	}

	/**
	 * Filter rewrite tags
	 */
	public function bloom_filters_write_tags() {
		add_rewrite_tag( '%plant_size%', '([^&]+)' );
		add_rewrite_tag( '%plant_difficulty%', '([^&]+)' );
		add_rewrite_tag( '%plant_light_level%', '([^&]+)' );
		add_rewrite_tag( '%plant_pet_friendly%', '([^&]+)' );
		add_rewrite_tag( '%plant_air_cleaner%', '([^&]+)' );
	}

	/**
	 * Filter rewrite rules
	 */
	public function bloom_filters_rewrite_rules() {
		add_rewrite_rule(
			'^shop/([^/]*)/([^/]*)/([^/]*)/([^/]*)/([^/]*)/?',
			'index.php?post_type=product&plant_size=$matches[1]&plant_difficulty=$matches[2]&plant_light_level=$matches[3]&plant_pet_friendly=$matches[4]&plant_air_cleaner=$matches[5]',
			'top'
		);
	}

	/**
	 * Builds the modal to the Plants made simple
	 *
	 * @param mixed $info Plants made simple data.
	 *
	 * @return string
	 */
	private function build_modal( $info ) {
		$modal = <<<MODAL
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<div class="row btn-close">
      			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
        <div class="row">
        	<div class="col-md-12 banner-container">
        		<img class="bloom-banner hidden-xs" src="{$info['bloom_size_guide_poster']['url']}" alt="{$info['bloom_size_guide_poster']['label']}">
        		<img class="bloom-banner visible-xs" src="{$info['bloom_size_guide_poster_mobile']['url']}" alt="{$info['bloom_size_guide_poster']['label']}">
			</div>
        </div>
      </div>
    </div>
  </div>
</div>
MODAL;

		return $modal;
	}

}
