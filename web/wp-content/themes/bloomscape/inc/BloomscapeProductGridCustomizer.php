<?php

namespace Bloomscape;

/**
 * Class BloomscapeProductGridCustomizer
 *
 * @package Bloomscape
 */
class BloomscapeProductGridCustomizer {

	/**
	 * Singleton instance
	 *
	 * @var BloomscapeProductGridCustomizer
	 */
	use Singleton;

	/*
	 * Useful functions to remove stuff
	 * @trait Remove Storefront/woocomerce stuff
	 */
	use CustomGrid;

	/**
	 * Sidebar container
	 *
	 * @var $sidebar Holds the sidebar element
	 */
	private $sidebar;

	/**
	 * BloomscapeProductGridCustomizer constructor.
	 */
	public function __construct() {
		$this->sidebar = BloomscapeSidebarFilter::init();
		add_action( 'woocommerce_before_shop_loop', [ $this, 'bloom_before_grid' ], 99 );
		add_action( 'woocommerce_after_shop_loop', [ $this, 'bloom_after_grid' ], 99 );
		add_action( 'woocommerce_no_products_found', [ $this, 'bloom_before_grid' ], 1 );
		add_action( 'woocommerce_no_products_found', [ $this, 'bloom_after_grid' ], 99 );
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 30;' ), 20 );
		$this->remove_default_woo_stuff();
	}

	/**
	 * Wrap the product grid for sidebar
	 */
	public function bloom_before_grid() {
		echo '<div class="row">';
		echo '<div class="col-md-3 col-sm-3 bloom-sidebar">';
		echo '<div class="side-wrapper">';
		echo '<div class="sort-title"><h3>Filter</h3></div>';
		echo $this->sidebar->sidebar_filters(); // WPCS: XSS OK.
		echo '</div>';
		echo '</div>';
		echo '<div class="col-md-9 col-sm-9 bloom-grid">';
	}

	/**
	 * End Wrapper for product wrapper
	 */
	public function bloom_after_grid() {
		echo '</div>';
		echo '</div>';
	}
}
