<?php

		acf_add_local_field_group(
			array(
				'key' => 'group_5a9067c04f7e2',
				'title' => 'Plant Care Image',
				'fields' => array(
					array(
						'key' => 'field_5a9067cbc0abf',
						'label' => 'Post Image',
						'name' => 'bloom_post_image',
						'type' => 'image',
						'instructions' => 'Add an image to your post',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'image',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => 1760,
						'max_height' => '',
						'max_size' => '',
						'mime_types' => 'jpg,png',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'post',
						),
					),
					array(
						array(
							'param' => 'page_template',
							'operator' => '==',
							'value' => 'plant-care-archive.php',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			)
		);
