<?php

	// Pages by slug or ID that want to be added.
	$pages = [ 'about' ];

foreach ( $pages as $page ) {
	if ( is_int( $page ) ) {
		$page_id = (string) $page;
	} else {
		$page_id = get_page_by_path( $page );
		if ( $page_id ) {
			$page_id = $page_id->ID;
		}
	}
	if ( $page_id ) {
		$location[] = array(
			array(
				'param'    => 'page',
				'operator' => '==',
				'value'    => (string) $page_id,
			),
		);
	}
}

		acf_add_local_field_group(
			array(
				'key' => 'group_5a8211f1b8da0',
				'title' => 'Title Description Group',
				'fields' => array(
					array(
						'key' => 'field_5a8212b7e6718',
						'label' => 'Title and description',
						'name' => 'bloom_title_box',
						'type' => 'group',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'layout' => 'block',
						'sub_fields' => array(
							array(
								'key' => 'field_5a8212e8e6719',
								'label' => 'Title',
								'name' => 'bloom_title_box',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => 200,
							),
							array(
								'key' => 'field_5a821326e671a',
								'label' => 'Content',
								'name' => 'bloom_content_box',
								'type' => 'textarea',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'rows' => '',
								'new_lines' => '',
							),
						),
					),
				),
				'location' => $location,
				'menu_order' => 2,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			)
		);
