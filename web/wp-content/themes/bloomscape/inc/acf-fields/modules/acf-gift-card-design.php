<?php

// Location array for ACF module, default to Post Type Product.
$location = array(
	array(
		array(
			'param' => 'post_type',
			'operator' => '==',
			'value' => 'product',
		),
	),
);

// Pages by slug or ID that want to be added.
$pages = [ 'about', 'cart' ];

foreach ( $pages as $page ) {
	if ( is_int( $page ) ) {
		$page_id = (string) $page;
	} else {
		$page_id = get_page_by_path( $page );
		if ( $page_id ) {
			$page_id = $page_id->ID;
		}
	}
	if ( $page_id ) {
		$location[] = array(
			array(
				'param' => 'page',
				'operator' => '==',
				'value' => (string) $page_id,
			),
		);
	}
}

acf_add_local_field_group(
	array(
		'key' => 'group_cufx8esilf9m1',
		'title' => 'Page template',
		'fields' => array(
			array(
				'key' => 'field_cufx8esilf9m1',
				'label' => 'Page template',
				'name' => 'product_page_template',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'page',
				),
				'taxonomy' => array(),
				'allow_null' => 0,
				'multiple' => 0,
				'return_format' => 'id',
				'ui' => 1,
				'choices' => array(
					'default' => 'Default',
					'giftCard' => 'Gift card',
				),
				'default_value' => 'dafault',
			),
		),
		'location' => $location,
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	)
);
