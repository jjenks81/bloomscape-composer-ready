<?php

		acf_add_local_field_group(
			array(
				'key' => 'group_5a821c0828f07',
				'title' => 'People Reviews',
				'fields' => array(
					array(
						'key' => 'field_5a8245e0c34b8',
						'label' => 'Label People Reviews',
						'name' => 'bloom_label_people_reviews',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 'what people are saying',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5a821d43cb6e6',
						'label' => 'People Saying Reviews',
						'name' => 'people_saying_reviews',
						'type' => 'repeater',
						'instructions' => 'Add many comments you want',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add new review',
						'sub_fields' => array(
							array(
								'key' => 'field_5a821dcacb6e7',
								'label' => 'Review Text',
								'name' => 'bloom_review_people_title',
								'type' => 'text',
								'instructions' => 'Add the text of the review',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'field_5a821e49cb6e8',
								'label' => 'CTA Review',
								'name' => 'bloom_review_people_cta',
								'type' => 'link',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'acf-options-bloomscape-settings',
						),
					),
				),
				'menu_order' => 5,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			)
		);
