<?php

	acf_add_local_field_group(
		array(
			'key' => 'group_5aafd7b997a53',
			'title' => 'Chat',
			'fields' => array(
				array(
					'key' => 'field_5aafd7d01fce2',
					'label' => 'Zend Desk Code',
					'name' => 'bloom_zend_desk_code',
					'type' => 'text',
					'instructions' => 'Paste bloomscape zend desk url',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-bloomscape-settings',
					),
				),
			),
			'menu_order' => 10,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		)
	);
