<?php

namespace Bloomscape;

/**
 *
 * Class BloomscapeBlog
 *
 * @package Bloomscape
 */
class PlantCare {

	/**
	 * Singleon instance
	 *
	 * @var BloomscapeAPI
	 */
	use Singleton;

	/**
	 * BloomscapeBlog constructor.
	 */
	public function __construct() {
		add_action( 'storefront_loop_before', [ $this, 'before_plantcare' ] );
		add_action( 'storefront_single_post_top', [ $this, 'before_plantcare' ] );
		add_action( 'storefront_single_post', [ $this, 'append_image_to_post' ] , 10 );
		add_action( 'storefront_single_post', [ $this, 'bloomscape_post_header' ] , 10 );
	}

	/**
	 * Before Plant Care add submenu and hero
	 */
	public function before_plantcare() {
		get_template_part( 'partials/plant-care-submenu' );
		get_template_part( 'partials/plant-care-hero' );
		remove_action( 'storefront_single_post',           'storefront_post_header',10 );
		remove_action( 'storefront_single_post','storefront_post_meta',20 );
		remove_action( 'storefront_single_post_bottom',  'storefront_post_nav',             10 );
		remove_action( 'storefront_single_post_bottom',  'storefront_display_comments',     20 );
	}

	/**
	 * Display the image post
	 *
	 * @since 1.0.0
	 */
	public function append_image_to_post() {
		remove_action( 'storefront_post_content_before', 'storefront_post_thumbnail',10 );
		get_template_part( 'partials/plant-care-image' );
		add_action( 'storefront_single_post_bottom', [ $this, 'bloomscape_post_date_author' ] , 10 );
	}

	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	public function bloomscape_post_header() {
		get_template_part( 'partials/plant-care/post-title' );
	}

	/**
	 * Display the author and date
	 *
	 * @since 1.0.0
	 */
	public function bloomscape_post_date_author() {
		get_template_part( 'partials/plant-care/date-author' );
		get_template_part( 'partials/plant-care/related-posts' );
	}

}
