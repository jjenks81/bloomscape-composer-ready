<?php

/**
 * Created by PhpStorm.
 * User: roysivan
 * Date: 1/21/18
 * Time: 1:40 PM
 */

namespace Bloomscape;

/**
 * Class WooProductPage
 *
 * @package Bloomscape
 */
class WooProductPage {

	/**
	 * Singleton instance
	 *
	 * @var WooProductPage
	 */
	use Singleton;

	/**
	 * BloomWooProductPage constructor.
	 * Adds hooks for WooCommerce Product Page
	 * Init the PDP Customizer Class
	 * Supporting Template: woocommerce/single-product/add-to-cart/variable.php
	 */
	public function __construct() {
		add_action( 'woocommerce_before_add_to_cart_form', [ $this, 'init_pdp_detail' ] );
	}

	/**
	 * Init PDP details
	 */
	public function init_pdp_detail() {
		global $post;
		$custom_attrs = WooProductCustomAttrs::init( $post->ID );

		$product_page_template = get_field( 'product_page_template' );

		if ( 'giftCard' === $product_page_template ) :
			echo $custom_attrs->product_short_description(); // WPCS: XSS OK.
		else :
			echo $custom_attrs->init_attrs_detail(); // WPCS: XSS OK.
		endif;
	}

}
