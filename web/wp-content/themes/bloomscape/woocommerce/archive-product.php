<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
global $filtered_results, $wp_query, $unmatched_query;

if ( ! is_a( $filtered_results, 'WP_Query' ) ) {
	$filtered_results = $wp_query;
}

defined( 'ABSPATH' ) || exit; // Exit if accessed directly.

define( 'GIFT_CARD_ID', 2367 );

get_header( 'shop' ); ?>

<?php
/**
 * Woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>

	<header class="woocommerce-products-header">

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
		/**
		 * Woocommerce_archive_description hook.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action( 'woocommerce_archive_description' );
		?>

	</header>

<?php
/**
 * Woocommerce_before_shop_loop hook.
 *
 * @hooked wc_print_notices - 10
 * @hooked woocommerce_result_count - 20
 * @hooked woocommerce_catalog_ordering - 30
 */
do_action( 'woocommerce_before_shop_loop' );
?>

<?php woocommerce_product_loop_start(); ?>


	<ul id="preload-shop-items">
		<?php
		while ( have_posts() ) :
			the_post();
?>

			<?php
			/**
			 * Woocommerce_shop_loop hook.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );
			?>

			<?php wc_get_template_part( 'content', 'product' ); ?>

		<?php endwhile; // end of the loop. ?>

		<?php
		$post   = get_post( GIFT_CARD_ID );

		if ( $post ) :
			$_product = wc_get_product( GIFT_CARD_ID );
			?>
				<li class="post-<?php echo esc_attr( GIFT_CARD_ID ); ?> product type-product status-publish has-post-thumbnail first instock taxable shipping-taxable purchasable product-type-simple">
					<a href="<?php echo esc_url( get_permalink( GIFT_CARD_ID ) ); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
						<img width="324" height="393" src="<?php echo esc_url( get_the_post_thumbnail_url( GIFT_CARD_ID ) ); ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="<?php echo esc_attr( $post->post_title ); ?>" sizes="(max-width: 324px) 100vw, 324px">
						<div class="bloomscape-size"></div>
						<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $post->post_title ); ?></h2>
						<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?php echo esc_html( $_product->get_regular_price() ); ?></span></span>
					</a>
				</li>
			<?php
		endif;
		?>
	</ul>

	<div id="filter-results" style="clear: both;">
		<script id="filter-results-template" type="text/x-handlebars-template">
			{{#each filtered_results}}
			<li class="post-{{ID}} product">
				<a href="{{product_data.link}}"
				   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
					{{{product_data.image}}}
					<div class="bloomscape-size">{{product_data.size}}</div>
					<h2 class="woocommerce-loop-product__title">{{post_title}}</h2>
					<span class="price"><span class="woocommerce-Price-amount amount"><span
									class="woocommerce-Price-currencySymbol">$</span>{{product_data.price}}</span></span>
				</a>
			</li>
			{{/each}}

			{{#each unmatched_results}}
			<li class="post-{{ID}} product unmatched">
				<a href="{{product_data.link}}"
				   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
					{{{product_data.image}}}
					<div class="bloomscape-size">{{product_data.size}}</div>
					<h2 class="woocommerce-loop-product__title">{{post_title}}</h2>
					<span class="price"><span class="woocommerce-Price-amount amount"><span
									class="woocommerce-Price-currencySymbol">$</span>{{product_data.price}}</span></span>
				</a>
			</li>
			{{/each}}

			<?php
			$post   = get_post( GIFT_CARD_ID );

			if ( $post ) :
				$_product = wc_get_product( GIFT_CARD_ID );
				?>
					<li class="post-<?php echo esc_attr( GIFT_CARD_ID ); ?> product type-product status-publish has-post-thumbnail first instock taxable shipping-taxable purchasable product-type-simple">
						<a href="<?php echo esc_url( get_permalink( GIFT_CARD_ID ) ); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
							<img width="324" height="393" src="<?php echo esc_url( get_the_post_thumbnail_url( GIFT_CARD_ID ) ); ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="<?php echo esc_attr( $post->post_title ); ?>" sizes="(max-width: 324px) 100vw, 324px">
							<div class="bloomscape-size"></div>
							<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $post->post_title ); ?></h2>
							<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?php echo esc_html( $_product->get_regular_price() ); ?></span></span>
						</a>
					</li>
				<?php
			endif;
			?>
		</script>
	</div>


<?php woocommerce_product_loop_end(); ?>

<?php
/**
 * Woocommerce_after_shop_loop hook.
 *
 * @hooked woocommerce_pagination - 10
 */
do_action( 'woocommerce_after_shop_loop' );
?>

<?php
/**
 * Woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
?>

<?php get_footer( 'shop' ); ?>
