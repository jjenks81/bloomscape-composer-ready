<?php
/**
 * Product quantity inputs
 *
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit; // Exit if accessed directly.

global $product;

$defaults = array(
	'max_value' => apply_filters( 'woocommerce_quantity_input_max', '', $product ),
	'min_value' => apply_filters( 'woocommerce_quantity_input_min', '', $product ),
	'step'      => apply_filters( 'woocommerce_quantity_input_step', '1', $product ),
);

if ( ! empty( $defaults['min_value'] ) ) {
	$min = $defaults['min_value'];
} else {
	$min = 1;
}

if ( ! empty( $defaults['max_value'] ) ) {
	$max = $defaults['max_value'];
} else {
	$max = 10;
}

if ( ! empty( $defaults['step'] ) ) {
	$step = $defaults['step'];
} else {
	$step = 1;
}

?>
<div class="quantity_select custom-dropdown">
	<span class="quantity-label-dropdown">Quantity</span>
	<select name="<?php echo esc_attr( $input_name ); ?>"
			title="<?php _ex( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); // WPCS: XSS OK. ?>" class="qty">
		<?php
		for ( $count = $min; $count <= $max; $count = $count + $step ) {
			if ( $count === $input_value ) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
			echo '<option value="' . $count . '"' . $selected . '>' . $count . '</option>'; // WPCS: XSS OK.
		}
		?>
	</select>
</div>
