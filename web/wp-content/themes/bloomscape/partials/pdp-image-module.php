<?php
$pdp_image_module = get_field( 'image_module' );

if ( $pdp_image_module ) :
?>
	<div id="pdp-image-module-wrapper" class="pdp-image-module-wrapper">
		<div class="image-container">
				<?php echo wp_get_attachment_image( $pdp_image_module , [ 1200, 762 ] ); ?>
		</div>
	</div>
<?php
endif;
?>
