<div class="people-reviews-module review-slider">
	<div class="label-module">
		<h3><?php echo esc_html( get_sub_field( 'testimonial_title' ) ); ?></h3>
	</div>
	<div class="module-carousel">
	<?php
	if ( have_rows( 'testimonial_box_repeater' ) ) :
		while ( have_rows( 'testimonial_box_repeater' ) ) :
			the_row();
			?>
			<div class="review-box">
				<div class="box-wrapper">
					<h4 class="review-title">
						<?php echo esc_html( get_sub_field( 'text' ) ); ?>
					</h4>
					<div class="review-cta">
						<?php
						if ( get_sub_field( 'link_url' ) ) :
						?>

							<a class="btn bloom-btn" href="<?php echo esc_url( get_sub_field( 'link_url' ) ); ?>"><?php echo esc_html( get_sub_field( 'link_text' ) ); ?></a>

						<?php endif; ?>
						</div>
					</div>
				</div>
			<?php
			endwhile;
		endif;
		?>
</div>
</div>
