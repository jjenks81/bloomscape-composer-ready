<?php
	$image_field = get_field( 'bloom_post_image' );
?>
<div id="plant-care-post-image" class="plant-care-post-image">
	<?php echo ( $image_field ) ? wp_get_attachment_image( $image_field, 'full' ) : ''; ?>
</div>
