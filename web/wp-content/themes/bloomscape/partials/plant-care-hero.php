<?php

if ( ! is_home() && ! is_category() ) {
	return;
}

	$queried_object = get_queried_object();
	$blog_page_id = get_option( 'page_for_posts' );

	$fields = [
		'image' => [],
		'title' => [],
		'description' => [],
	];
	foreach ( $fields as $name => $value ) {
		$fields[ $name ] = ( $queried_object->term_id )
		? get_field( "plant_care_hero_$name", $queried_object )
		: get_field( "plant_care_hero_$name", $blog_page_id );
	}

	list($image,$title,$desc) = array_values( $fields );

	if ( ! $image ) {
		return;
	}

	$image = wp_get_attachment_image_src( $image, 'full' );
?>

<div id="plant-care-hero" style="background-image:url(<?php echo esc_html( $image[0] ); ?>);" class="plant-care-hero">
	<div class="plant-care-hero-info">
		<div class="plant-care-container">
			<?php echo ( $title ) ? '<p class="service-text">' . esc_html( $title ) . '</p>' : ''; ?>
			<?php if ( $desc ) : ?>
				<h2 class="plant-care-hero-desc">
					<?php echo esc_html( $desc ); ?>
				</h2>
			<?php endif; ?>
		</div>
	</div>
</div>
