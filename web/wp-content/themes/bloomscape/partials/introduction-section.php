<?php
$introduction = [
	'title'   => get_sub_field( 'introduction_title' ),
	'content' => get_sub_field( 'introduction_content' ),
];
?>

<div id="introduction-section" class="introduction-section">
	<h1><?php echo esc_html( $introduction['title'] ); ?></h1>

	<h2><?php echo esc_html( $introduction['content'] ); ?></h2>
</div>
