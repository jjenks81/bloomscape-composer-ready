<div class="plants-made">
	<div class="row">
		<div class="col-md-12">
			<h3><?php echo esc_html( get_sub_field( 'value_prop_title' ) ); ?></h3>
		</div>
	</div>
	<?php if ( have_rows( 'value_prop_box_repeater' ) ) : ?>
		<div class="row">
			<?php
			while ( have_rows( 'value_prop_box_repeater' ) ) :
				the_row();
				$image = get_sub_field( 'image' );
				?>
				<div class="col-md-4 content-box">
					<?php if ( $image ) : ?>
					<div class="box-image">
						<?php echo wp_get_attachment_image( $image , 'full' ); ?>
					</div>
					<?php endif; ?>
					<div class="box-label">
						<h4><?php echo esc_html( get_sub_field( 'label' ) ); ?></h4>
					</div>
					<div class="box-text">
						<?php echo get_sub_field( 'description' ); // phpcs:ignore ?>
					</div>
					<?php if ( get_sub_field( 'link_url' ) && get_sub_field( 'link_text' ) ) : ?>
						<div class="box-link">
							<a href="<?php echo esc_url( get_sub_field( 'link_url' ) ); ?>"><?php echo esc_html( get_sub_field( 'link_text' ) ); ?></a>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		</div>
	<?php else : ?>
	no rows
	<?php endif; ?>
</div>
