<div class="people-reviews-module review-slider">
	<?php if ( have_rows( 'people_saying_reviews', 'option' ) ) : ?>
		<div class="label-module">
			<h3><?php echo esc_html( get_field( 'bloom_label_people_reviews','options' ) ); ?></h3>
		</div>
		<div class="module-carousel">
			<?php
			while ( have_rows( 'people_saying_reviews', 'option' ) ) :
			the_row();
			?>
			<div class="review-box">
				<div class="box-wrapper">
					<h4 class="review-title">
						<?php echo esc_html( get_sub_field( 'bloom_review_people_title' ) ); ?>
					</h4>
					<div class="review-cta">
						<?php

							$link = get_sub_field( 'bloom_review_people_cta' );

						if ( $link ) :
							?>

								<a class="btn bloom-btn" href="<?php echo esc_url( $link['url'] ); ?>"
								target="<?php echo esc_attr( $link['target'] ); ?>"><h3><?php echo esc_html( $link['title'] ); ?></h3></a>

								<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</div>
