<?php
	$bloom_inner_content = get_field( 'bloom_inner_text_content' );
?>
<?php if ( $bloom_inner_content ) : ?>
		<div class="side-content-module-wrapper">
			<?php echo wp_kses_post( $bloom_inner_content ); ?>
		</div>
<?php
endif;
