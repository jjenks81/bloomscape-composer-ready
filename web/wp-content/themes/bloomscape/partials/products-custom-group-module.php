<?php
$shop_products = get_sub_field( 'products_custom_group' );

if ( ! empty( $shop_products ) ) :
?>

	<div class="products-custom-group archive">
		<ul class="products">
			<?php
			foreach ( $shop_products as &$shop_product ) {
				$product = wc_get_product( $shop_product->ID );
				$product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $shop_product->ID ), 'single-post-thumbnail' );
				$product_link = get_permalink( $shop_product->ID );
				$product_size = get_field( 'plant_size', $shop_product->ID );

				echo '<li class="product has-post-thumbnail">';
					echo '<a href="' . esc_url( $product_link ) . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">';
						echo '<img src="' . esc_url( $product_image[0] ) . '" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" title="' . esc_attr( $shop_product->post_title ) . '" >';
						echo '<div class="bloomscape-size">' . esc_html( $product_size ) . '</div>';
						echo '<h2 class="woocommerce-loop-product__title">' . esc_html( $shop_product->post_title ) . '</h2>';
						echo '<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>' . esc_html( $product->get_price() ) . '</span></span>';
					echo '</a>';
				echo '</li>';
			}
			?>
		</ul>
	</div>
<?php
endif;
?>
