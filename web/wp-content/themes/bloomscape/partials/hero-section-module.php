<?php
$hero_image = get_sub_field( 'hero_image' );
$hero_image_featured = $hero_image['sizes']['thumbnail'];
$hero_title = get_sub_field( 'hero_title' );
$hero_subtitle = get_sub_field( 'hero_subtitle' );
$hero_button_text = get_sub_field( 'hero_button_text' );
$hero_button_url = get_sub_field( 'hero_button_url' );
?>

<div id="main-hero" class="main-hero" style="background-image: url(<?php echo esc_url( $hero_image['url'] ); ?>); width: 100%; " data-featured-image="<?php echo esc_url( $hero_image_featured ); ?>">
		<div class="entry-hero">
			<div class="hero-text">
				<?php if ( $hero_title ) : ?>
					<h1><?php echo esc_html( $hero_title ); ?></h1>
				<?php endif; ?>
				<?php if ( $hero_subtitle ) : ?>
					<h3><?php echo esc_html( $hero_subtitle ); ?></h3>
				<?php endif; ?>
			</div>
			<?php if ( $hero_button_text && $hero_button_url ) : ?>
				<a class="btn btn-bloom" href="<?php echo esc_url( $hero_button_url ); ?>" target=""><?php echo esc_html( $hero_button_text ); ?></a>
			<?php endif; ?>
		</div>
</div>
