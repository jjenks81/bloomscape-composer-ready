<?php if ( have_rows( 'three_card' ) ) : ?>
<div id="three-card-module-wrapper" class="three-card-module-wrapper">
<div class="left-parallax-image-leafs-cards rellax" data-rellax-speed="-4"></div>
	<?php
	$i = 0;
	$sizes = [ [ 300, 300 ], [ 200, 200 ], [ 560, 662 ] ];
	while ( have_rows( 'three_card' ) ) :
		the_row();
	?>
		<div class="three-card-single card-<?php echo esc_html( $i ); ?>">
			 <div class="three-card-image">
					<?php $img = get_sub_field( 'image' ); ?>
					<?php if ( $img ) : ?>
						<?php echo wp_get_attachment_image( $img , $sizes[ $i ] ); ?>
					<?php endif; ?>
				 <a class="btn btn-bloom" href="<?php echo esc_url( get_sub_field( 'link_url' ) ); ?>"><?php echo esc_html( get_sub_field( 'link_text' ) ); ?></a>
			 </div>
		 </div>
	<?php
	$i++;
endwhile;
?>

</div>
<?php endif; ?>
