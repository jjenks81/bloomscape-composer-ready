<?php
/**
 * Image & Text Module - powered by acf-fields/modules/acf-image-text-module.php
 */
if ( have_rows( 'image_text_modules' ) ) :
?>
	<div id="image-text-modules-wrapper" class="image-text-modules-wrapper">
		<?php
		while ( have_rows( 'image_text_modules' ) ) :
			the_row();
			?>
			<div class="image-text-module <?php echo esc_html( get_sub_field( 'image_placement' ) ); ?>">
				<div class="images">
					<?php
					if ( have_rows( 'images' ) ) :
						while ( have_rows( 'images' ) ) :
							the_row();
							?>
							<?php
							$img = get_sub_field( 'image' );
							if ( $img ) :
								echo wp_get_attachment_image( $img , 'full' );
							endif;
						endwhile;
					endif;
					?>
				</div>
				<div class="text">
					<?php if ( get_sub_field( 'title' ) ) : ?>
						<h3><?php echo wp_kses_post( get_sub_field( 'title' ) ); ?></h3>
					<?php endif; ?>
					<?php
						echo wp_kses_post( get_sub_field( 'text' ) );
						if ( ! empty( get_sub_field( 'link_url' ) ) && ! empty( get_sub_field( 'link_text' ) ) ) {
							echo '<a class="btn btn-primary" href="' . esc_url( get_sub_field( 'link_url' ) ) . '" tabindex="0">' . esc_html( get_sub_field( 'link_text' ) ) . '</a>';
						}
					?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php
endif;
?>
