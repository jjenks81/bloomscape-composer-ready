<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package bloomscape
 */

use Bloomscape\Customizer;

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>
<div class="left-parallax-image-leaf-home-back rellax" data-rellax-zindex="-1"></div>
<div class="left-parallax-image-leaf-home-top rellax" data-rellax-speed="1"></div>
<div id="main-hero" class="main-hero"
	 style="<?php Customizer::init()->bloomscape_homepage_content_styles(); ?>"
	 data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<?php
	echo Customizer::init()->bloomscape_homepage_hero( get_the_content() ); // WPCS: XSS OK.
	?>
</div><!-- #post-## -->
<div class="left-parallax-image-leaf-home-back-r rellax" data-rellax-speed="-4"></div>
<div class="left-parallax-image-leaf-home-top-r rellax" data-rellax-speed="4"></div>
