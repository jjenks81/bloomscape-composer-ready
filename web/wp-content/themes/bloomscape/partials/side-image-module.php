<?php
	$inner_image_module = get_field( 'bloom_inner_side_image' );
if ( $inner_image_module ) :
	?>
	<div class="side-image-module-wrapper">
		<div class="image-container">
					<?php echo wp_get_attachment_image( $inner_image_module, 'full' ); ?>
		</div>
		</div>
	<?php
	endif;
?>
