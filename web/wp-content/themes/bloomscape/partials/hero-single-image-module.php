<?php
$hero_single_image = get_sub_field( 'image_hero_section_url' );
$hero_single_image_featured = $hero_single_image['sizes']['thumbnail'];
?>

<div class="hero-single-image">
	<img src="<?php echo esc_url( $hero_single_image['url'] ); ?>" alt="">
</div>
