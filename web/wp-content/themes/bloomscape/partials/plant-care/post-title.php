<header class="entry-header">
	<?php
		$category = get_the_category();
	?>
	<?php if ( is_array( $category ) ) : ?>
	<h4><?php echo esc_html( $category[0]->cat_name ); ?></h4>
	<?php endif; ?>
	<?php
		the_title( sprintf( '<h1><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
	?>
</header><!-- .entry-header -->
