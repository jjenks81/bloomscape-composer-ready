<div class="plant-care-related">
	<h4>also worth checking out</h4>

	<div class="plan-care-wrapper">
	<?php
		$max_articles = 6;


		$article_tags = get_the_tags();
		$tags_string = '';
	if ( $article_tags ) {
		foreach ( $article_tags as $article_tag ) {
			$tags_string .= $article_tag->slug . ',';
		}

		$tag_related_posts = get_posts( 'exclude=' . get_post()->ID . '&numberposts=' . $max_articles . '&tag=' . $tags_string );
		if ( $tag_related_posts ) {
			foreach ( $tag_related_posts as $related_post ) {
	?>
				<div class="plant-care-box">
					<div class="plant-care-image">
						<?php
							$featured_image = get_the_post_thumbnail( $related_post->ID ,'large' );
						?>
						<a href="<?php echo esc_url( get_the_permalink( $related_post->ID ) ); ?>">
						<?php echo ( $featured_image ) ? wp_kses_post( $featured_image ) : ''; ?>
						</a>
						</div>
						<?php
						$category = get_the_category( $related_post->ID );
						?>
						<?php if ( is_array( $category ) ) : ?>
							<p><?php echo esc_html( $category[0]->cat_name ); ?></p>
						<?php endif; ?>
						<h3><a href="<?php echo esc_url( get_the_permalink( $related_post->ID ) ); ?>"><?php echo esc_html( get_the_title( $related_post->ID ) ); ?></a></h3>
					</div>

				<?php
			}
		}
	}
	?>
	</div>
</div>
