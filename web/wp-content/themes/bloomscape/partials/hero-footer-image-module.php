<?php
	$post = ( in_the_loop() ) ? false : get_page_by_path( 'shop' );
	$bloomscape_hero_image_footer_field = get_field( 'bloom_footer_hero_image' , $post );
?>
<?php if ( is_array( $bloomscape_hero_image_footer_field ) ) : ?>
	<div class="hero-image-module-wrapper">
		<?php $img = wp_get_attachment_image_src( $bloomscape_hero_image_footer_field['ID'], 'full' ); ?>
		<?php if ( is_array( $img ) ) : ?>
			<img alt="<?php echo esc_html( $bloomscape_hero_image_footer_field['alt'] ); ?>" src="<?php echo esc_url( $img[0] ); ?>">
		<?php endif; ?>
	</div>
<?php endif; ?>
