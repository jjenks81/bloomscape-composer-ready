<!-- SIGN IN MODAL -->
<div class="modal fade sign-in-modal" id="sign-in-modal" tabindex="-1" role="dialog" aria-labelledby="SignIn">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<!-- login form -->
						<h3>sign in</h3>
						<p>
							Welcome back!
						</p>
						<?php
						wp_login_form(
							array(
								'redirect' => get_bloginfo( 'url' ) . '/my-account',
							)
						);
						?>
						<a class="lost-password" href="<?php echo esc_url( wp_lostpassword_url( get_bloginfo( 'url' ) . '/my-account' ) ); ?>">lost password?</a>
					</div>
					<div class="col-sm-6 register-form">
						<!-- register form -->
						<h3>new customer</h3>
						<p>
							Creating an account gives you easy access to order history, care instructions for purchased products and support history
						</p>
						<form method="post" action="<?php echo esc_html( get_bloginfo( 'url' ) ); ?>">
							<div class="field-group">
								<label for="_new_user_email">email Address</label>
								<input type="email" name="_new_user_email" required />
							</div>
							<div class="field-group">
								<label for="_new_user_password">password</label>
								<input type="password" minlength="6" name="_new_user_password" required />
								<em>Password must be at least 6 characters</em>
							</div>
							<div class="field-group submit">
								<input type="hidden" name="_new_user_init" value="1" />
								<input type="hidden" name="_new_user_nonce" value="<?php echo esc_html( wp_create_nonce( 'new_user_reg' ) ); ?>" />
								<button class="btn-primary" type="submit">create account</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- / SIGN IN MODAL -->
