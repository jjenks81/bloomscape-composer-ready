<?php $newsletter_module_title = get_field( 'newsletter_module_title' ); ?>
<div id="newsletter-module-wrapper" class="newsletter-module-wrapper">
	<div class="newsletter-inner">
		<?php if ( $newsletter_module_title ) : ?>
			<h2 class="newsletter-module-title">
				<?php echo esc_html( $newsletter_module_title ); ?>
			</h2>
		<?php endif; ?>

		<?php gravity_form( get_field( 'bloom_newsletter_frm_id','option' ), false, false, false, '', true, 12 ); ?>
	</div>
</div>
