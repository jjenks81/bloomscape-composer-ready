<?php
	$iframe_content = get_field( 'youtube_module_url' );
	if ( ! $iframe_content ) :
	$iframe_content = get_sub_field( 'youtube_module_url' );
	endif;
if ( $iframe_content ) : ?>
	<?php
	$iframe_regex = preg_match( '/embed\/(.*)\?/',$iframe_content,$youtube_id );
	?>
	<?php if ( is_array( $youtube_id ) && ! wp_is_mobile() ) : ?>
<div id="youtube-module-wrapper" class="youtube-module-wrapper">
	<div class="video-foreground">
		<iframe src="https://www.youtube.com/embed/<?php echo esc_html( $youtube_id[1] ); ?>?controls=0&showinfo=0&rel=0&autoplay=1&mute=1&loop=1&playlist=<?php echo esc_html( $youtube_id[1] ); ?>" frameborder="0" allowfullscreen></iframe>
	</div>
</div>
		<?php endif; ?>
<?php endif; ?>
