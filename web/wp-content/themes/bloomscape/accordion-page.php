<?php
	/**
	 * Template Name: Accordion Page
	 */
	get_header();
	the_post();
?>

	<div class="inner-wrapper">
		<?php
			the_content();
		?>
		<div class="inner-content">
			<?php get_template_part( '/partials/faq-module' ); ?>
		</div>
	</div>

<?php
	get_footer();
