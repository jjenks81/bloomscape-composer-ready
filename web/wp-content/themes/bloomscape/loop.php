<?php


while ( have_posts() ) :
	the_post(); ?>
		<?php
		$categories[ get_post()->post_type ][] = get_post( get_the_ID() );
		?>
	<?php
	endwhile;
if ( array_key_exists( 'product',$categories ) ) :
	?>
		<h2 class="result-title">Plants</h2>
		<div class="row-search-result product">
	<?php foreach ( $categories['product'] as $product ) : ?>
			<div class="product-result">
				<div class="thumb">
					<a href="<?php echo esc_url( get_the_permalink( $product ) ); ?>">
					<?php echo get_the_post_thumbnail( $product ); ?>
					</a>
				</div>
				<h3><a href="<?php echo esc_url( get_the_permalink( $product ) ); ?>"><?php echo esc_html( get_the_title( $product ) ); ?></a></h3>
				<p class="excerpt">
					<?php echo ( has_excerpt( $product ) ) ? wp_kses_post( get_the_excerpt( $product ) ) : ''; ?>
				</p>
			</div>
		<?php endforeach; ?>
	</div>
		<?php endif; ?>
	<?php if ( array_key_exists( 'post',$categories ) ) : ?>
	<h2 class="result-title">Blog posts</h2>
	<div class="row-search-result post">
		<?php foreach ( $categories['post'] as $product ) : ?>
			<div class="product-result">
				<h3><a href="<?php echo esc_url( get_the_permalink( $product ) ); ?>"><?php echo esc_html( get_the_title( $product ) ); ?></a></h3>
				<p class="excerpt">
					<?php echo ( has_excerpt( $product ) ) ? wp_kses_post( get_the_excerpt( $product ) ) : ''; ?>
				</p>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
<?php if ( array_key_exists( 'page',$categories ) ) : ?>
	<h2 class="result-title">Pages</h2>
	<div class="row-search-result post">
		<?php foreach ( $categories['page'] as $product ) : ?>
			<div class="product-result">
				<h3><a href="<?php echo esc_url( get_the_permalink( $product ) ); ?>"><?php echo esc_html( get_the_title( $product ) ); ?></a></h3>
				<p class="excerpt">
					<?php echo ( has_excerpt( $product ) ) ? wp_kses_post( get_the_excerpt( $product ) ) : ''; ?>
				</p>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
	<?php
	/**
	 * Functions hooked in to storefront_paging_nav action
	 *
	 * @hooked storefront_paging_nav - 10
	 */
	do_action( 'storefront_loop_after' );
