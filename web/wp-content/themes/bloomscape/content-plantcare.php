<?php
	define( 'MAX_PLANTS',100 );
	define( 'PLANT_CARE_PAGE', 'plant-care' );
	$plant_life_page = get_page_by_path( 'plant-life' );
if ( $plant_life_page ) {
	$plant_life_children = get_children(
		[
			'post_parent' => $plant_life_page->ID,
			'numberposts' => MAX_PLANTS,
		]
	);
}
	$current_category = get_category( get_query_var( 'cat' ) );
?>

<div class="content-plant-care">
	<div class="content-grid">
<?php

	do_action( 'storefront_loop_before' );
	?>
		<div class="plant-care-content-wrapper">
			<div class="plant-care-element"></div>
	<div class="plant-care-wrapper">
	<?php
	while ( have_posts() ) :
		the_post();
?>

		<div class="plant-care-box">
			<div class="plant-care-image">
				<?php
					$featured_image = get_the_post_thumbnail( null,'large' );
				?>
				<a href="<?php echo esc_url( get_the_permalink() ); ?>">
				<?php echo ( $featured_image ) ? wp_kses_post( $featured_image ) : ''; ?>
				</a>
			</div>
			<?php
				$category = get_the_category();
			?>
			<?php if ( is_array( $category ) ) : ?>
			<p><?php echo esc_html( $category[0]->cat_name ); ?></p>
			<?php endif; ?>
			<h3><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php echo esc_html( get_the_title() ); ?></a></h3>
		</div>

	<?php
	endwhile;
	if ( PLANT_CARE_PAGE === $current_category->slug && count( $plant_life_children ) > 0 ) :
		foreach ( $plant_life_children as $plantcare_page ) :
			if ( 'page' !== $plantcare_page->post_type ) :
				continue;
			endif;
		?>
			<div class="plant-care-box">
				<div class="plant-care-image">
					<?php
						$featured_image = get_the_post_thumbnail( $plantcare_page->ID,'large' );
					?>
					<a href="<?php echo esc_url( get_the_permalink( $plantcare_page->ID ) ); ?>">
						<?php echo ( $featured_image ) ? wp_kses_post( $featured_image ) : ''; ?>
					</a>
				</div>
				<?php if ( isset( $current_category->name ) ) : ?>
					<p><?php echo esc_html( $current_category->name ); ?></p>
				<?php endif; ?>
				<h3><a href="<?php echo esc_url( get_the_permalink( $plantcare_page->ID ) ); ?>"><?php echo esc_html( get_the_title( $plantcare_page->ID ) ); ?></a></h3>
			</div>
		<?php
		endforeach;
		?>
		<?php endif; ?>
		<?php
		/**
		 * Functions hooked in to storefront_paging_nav action
		 *
		 * @hooked storefront_paging_nav - 10
		 */
		do_action( 'storefront_loop_after' );
	?>
	</div>
		<div class="plant-care-archive-wrapper">
			<div class="plant-care-archive">
				<div class="plant-care-archive-container">
					<?php if ( ! empty( $plant_life_children ) ) : ?>
					<h3>Plant care archive</h3>
					<ul>
						<?php
						foreach ( $plant_life_children as $page ) {
							if ( 'page' !== $page->post_type ) {
								continue; }
							echo '<li><a href="' . esc_url( get_permalink( $page->ID ) ) . '">' . esc_html( $page->post_title ) . '</a></li>';
						}
						?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
