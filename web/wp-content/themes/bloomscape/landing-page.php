<?php
	/**
	 * Template Name: Landing page
	 */
	get_header();
	the_post();
?>

<?php

if ( have_rows( 'content' ) ) :

	while ( have_rows( 'content' ) ) :
		the_row();

		if ( get_row_layout() === 'hero_section' ) :

			get_template_part( '/partials/hero-section-module' );

		elseif ( get_row_layout() === 'products_custom_group' ) :

			get_template_part( '/partials/products-custom-group-module' );

		elseif ( get_row_layout() === 'value_prop' ) :

			get_template_part( '/partials/value-prop-module' );

		elseif ( get_row_layout() === 'testimonials' ) :

			get_template_part( '/partials/testimonials-module' );

		elseif ( get_row_layout() === 'youtube_module' ) :

			get_template_part( '/partials/youtube-module' );

		elseif ( get_row_layout() === 'image_hero_section' ) :

			get_template_part( '/partials/hero-single-image-module' );

		elseif ( get_row_layout() === 'introduction_section' ) :

			get_template_part( '/partials/introduction-section' );

		elseif ( get_row_layout() === 'image_text_modules' ) :

			get_template_part( '/partials/image-text-module' );

		endif;

	endwhile;

endif;

?>

<?php
	get_footer();
