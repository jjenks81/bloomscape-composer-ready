const mix = require('laravel-mix');
mix.setResourceRoot('/wp-content/themes/bloomscape/');
mix.setPublicPath('./');
mix.sass('assets/sass/style.scss', './').options({
    processCssUrls: false
});
mix.copyDirectory('assets/fonts','./fonts');