<?php
	/**
	 * Template Name: About Us
	 */
	get_header();
	the_post();
?>

<?php get_template_part( '/partials/hero-image-module' ); ?>
<?php get_template_part( '/partials/title-box-module' ); ?>
<?php get_template_part( '/partials/image-text-module' ); ?>
<?php get_template_part( '/partials/hero-footer-image-module' ); ?>
<?php get_template_part( '/partials/people-reviews-module' ); ?>


<?php
	get_footer();
