<?php

namespace Bloomscape;

/**
 * Class BloomscapePDPCustomizer
 *
 * @package Bloomscape
 */
class BloomscapePDPCustomizer {

	/**
	 * Singleton instance
	 *
	 * @var BloomscapePDPCustomizer
	 */
	use Singleton;

	/**
	 * BloomscapePDPCustomizer constructor.
	 */
	public function __construct() {
		$this->remove_woo_extra()->customize_pdp();
	}

	/**
	 * Remove storefront extras
	 *
	 * @return $this
	 */
	public function remove_woo_extra() {

		remove_theme_support( 'wc-product-gallery-zoom' );
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
		add_filter( 'woocommerce_sale_flash', [ $this, 'woo_custom_hide_sales_flash' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_child_theme_styles' ], PHP_INT_MAX );
		add_action( 'wp_print_styles', [ $this, 'sf_child_theme_dequeue_style' ], 1 );

		add_action( 'woocommerce_after_single_product', [ $this, 'pdp_page_modules' ] );

		return $this;
	}

	/**
	 * Customize_pdp
	 * Append some wrappers to manage the custom markup
	 *
	 * @return $this
	 */
	public function customize_pdp() {

		add_action( 'woocommerce_before_single_product_summary', [ $this, 'bloom_wrap_summary_pdp_open' ] );
		add_action( 'woocommerce_after_single_product_summary', [ $this, 'bloom_wrap_summary_pdp_close' ] );
		add_action( 'woocommerce_before_add_to_cart_button', [ $this, 'open_wrapper_before_cart' ] );
		add_action( 'woocommerce_after_add_to_cart_button', [ $this, 'close_wrapper_before_cart' ] );
		add_action( 'init', [ $this, 'remove_shop_breadcrumbs' ] );
		add_action( 'init', [ $this, 'move_related_products' ] );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		add_action( 'woocommerce_single_product_summary', [ $this, 'move_price_down_next_to_excerpt' ] );
		add_action( 'woocommerce_single_product_summary', [ $this, 'append_mobile_button' ] );
		add_filter( 'woocommerce_product_tabs', [ $this, 'woo_remove_product_tabs' ], 98 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
		add_filter(
			'woocommerce_product_single_add_to_cart_text',
			[ $this, 'custom_woocommerce_product_add_to_cart_price' ]
		);
		add_filter( 'wc_add_to_cart_message', [ $this, 'empty_wc_add_to_cart_message' ], 10, 2 );
		// Order Detail Page.
		add_action( 'woocommerce_order_item_meta_end', [ $this, 'bloom_add_plant_care_order_detail' ], 99, 3 );
		// Remove stock.
		add_filter( 'woocommerce_stock_html', [ $this, 'my_wc_hide_in_stock_message' ], 10, 3 );

		return $this;
	}

	/**
	 * Hide the On sale label
	 *
	 * @return bool
	 */
	public function woo_custom_hide_sales_flash() {
		return false;
	}

	/**
	 * Wrap the product attributes
	 */
	public function bloom_wrap_summary_pdp_open() {
		echo '<div class="col-md-12 bloom-summary-wrapper">';
	}

	/**
	 * Close the row and the new column appended
	 */
	public function bloom_wrap_summary_pdp_close() {
		echo '</div>';
	}

	/**
	 * Add a helper wrapper for bloomscape customized layout
	 */
	public function open_wrapper_before_cart() {
		echo '<div class="row"><div class="bloom-checkout">';
	}

	/**
	 * Append close wrapper
	 */
	public function close_wrapper_before_cart() {
		echo '</div></div>';
	}

	/**
	 * Remove default message
	 *
	 * @param string  $message Message.
	 * @param integer $product_id Post ID.
	 *
	 * @return string
	 */
	public function empty_wc_add_to_cart_message( $message, $product_id ) {
		return '';
	}

	/**
	 * Move related products
	 */
	public function move_related_products() {
		remove_action( 'woocommerce_after_single_product_summary',    'storefront_upsell_display',                15 );
		add_action( 'bloomscape_related_products', 'storefront_upsell_display', 100 );
	}


	/**
	 * Remove default styles
	 */
	public function sf_child_theme_dequeue_style() {
		wp_dequeue_style( 'storefront-style' );
		wp_dequeue_style( 'storefront-woocommerce-style' );
	}

	/**
	 *  Add custom styles
	 */
	public function enqueue_child_theme_styles() {
		wp_enqueue_style( 'child-style', get_stylesheet_uri(), [ 'theme' ] );
		wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto+Mono', [], '1.0.0' );
	}

	/**
	 * Remove breadcrumbs
	 */
	public function remove_shop_breadcrumbs() {
		remove_action( 'storefront_content_top', 'woocommerce_breadcrumb', 10 );
	}

	/**
	 *  Relocate price just before the title
	 */
	public function move_price_down_next_to_excerpt() {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}

	/**
	 *  Append mobile button
	 */
	public function append_mobile_button() {
		add_action( 'woocommerce_single_product_summary', [ $this, 'link_see_details' ], 26 );
	}

	/**
	 *  Append mobile toggle button
	 */
	public function link_see_details() {
		echo '<div class="bottom-helper"><a href="#toggle" class="product-mobile-toogler">See product details</a></div>';
	}

	/**
	 * Remove PDP unuseful things
	 *
	 * @param mixed $tabs Elements to be hooked.
	 *
	 * @return mixed
	 */
	public function woo_remove_product_tabs( $tabs ) {

		unset( $tabs['description'] );        // Remove the description tab!
		unset( $tabs['reviews'] );            // Remove the reviews tab!
		unset( $tabs['additional_information'] );    // Remove the additional information tab!

		return $tabs;

	}

	/**
	 * Add the price to the CTA PDP add to cart button
	 *
	 * @return string
	 */
	public function custom_woocommerce_product_add_to_cart_price() {
		global $product;

		$product_type = $product->product_type;

		switch ( $product_type ) {
			case 'external':
				return __( 'Buy product', 'woocommerce' );
			case 'grouped':
				return __( 'View products', 'woocommerce' );
			case 'simple':
				return ( 'Add to cart - $' . $product->price );
			case 'variable':
				return ( 'Add to cart - $' . $product->price );
			default:
				return __( 'Read more', 'woocommerce' );
		}

	}

	/**
	 * PDP Modules
	 */
	public function pdp_page_modules() {
		get_template_part( '/partials/pdp-desc-module' );
		do_action( 'bloomscape_related_products' );
		get_template_part( '/partials/image-text-module' );
		get_template_part( '/partials/instagram-module' );
		get_template_part( '/partials/customer-reviews-module' );
		get_template_part( '/partials/pdp-image-module' );
	}

	/**
	 * Add Related Plant Care Page to Order Details
	 *
	 * @param integer $item_id ID of the product.
	 * @param mixed   $item product data.
	 * @param integer $order product order.
	 */
	public function bloom_add_plant_care_order_detail( $item_id, $item, $order ) {
		$plant_care_page = get_field( 'related_plant_care_page', $item['product_id'] );
		if ( $plant_care_page ) {
			echo '<div class="plant-care-order"><strong>Plant care page:</strong> <a href="' . esc_url( get_permalink( $plant_care_page ) ) . '">' . esc_html( get_the_title( $plant_care_page ) ) . '</a></div>';
		}
	}

	/**
	 * Hide the "In stock" message on product page.
	 *
	 * @param string     $html html.
	 * @param string     $text text.
	 * @param WC_Product $product product.
	 * @return string
	 */
	function my_wc_hide_in_stock_message( $html, $text, $product ) {
		$availability = $product->get_availability();
		if ( isset( $availability['class'] ) && 'in-stock' === $availability['class'] ) {
			return '';
		}
		return $html;
	}

}
