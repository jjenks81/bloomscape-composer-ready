<?php

namespace Bloomscape;

/**
 * Class NewUser
 *
 * @package Bloomscape
 */
class NewUser {

	/**
	 * Use Singleton for object
	 */
	use Singleton;

	/**
	 * Store error message if one exists
	 *
	 * @var string
	 */
	private $error_msg;

	/**
	 * NewUser constructor.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'new_user_check' ] );
	}

	/**
	 * Check if new user form submitted
	 */
	public function new_user_check() {
		if (
			isset( $_POST, $_POST['_new_user_init'], $_POST['_new_user_email'], $_POST['_new_user_password'], $_POST['_new_user_nonce'] ) &&
			wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_new_user_nonce'] ) ), 'new_user_reg' )
		) {
			$email = sanitize_email( wp_unslash( $_POST['_new_user_email'] ) );
			$password = sanitize_text_field( wp_unslash( $_POST['_new_user_password'] ) );

			$this->create_user( $email, $password );
		}
	}

	/**
	 * Create User.
	 *
	 * @param string $email user email.
	 * @param string $password user password.
	 */
	private function create_user( $email, $password ) {
		$userdata = [
			'user_pass' => $password,
			'user_login' => $email,
			'user_email' => $email,
		];
		$new_user = wp_insert_user( $userdata );
		if ( ! is_wp_error( $new_user ) ) {
			wp_set_current_user( $new_user );
			wp_set_auth_cookie( $new_user );
			wp_safe_redirect( get_bloginfo( 'url' ) . '/my-account?_new_user=true' );
		} else {
			$this->error_msg = $new_user->get_error_message();
			add_action( 'wp_footer', [ $this, 'error_modal' ] );
		}
	}

	/**
	 * If Error, insert modal into footer
	 *
	 * @return bool
	 */
	public function error_modal() {
		if ( ! $this->error_msg ) {
			return false;
		}

		echo '<div class="modal fade" id="sign-in-error-modal" tabindex="-1" role="dialog" aria-labelledby="SignInError">';
			echo '<div class="modal-dialog modal-lg" role="document">';
				echo '<div class="modal-content">';
					echo '<div class="modal-header"><h4 class="modal-title">ERROR</h4></div>';
					echo '<div class="modal-body">' . esc_html( $this->error_msg ) . '</div>';
					echo '<div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div>';
				echo '</div>';
			echo '</div>';
		echo '</div>';

	}


}
