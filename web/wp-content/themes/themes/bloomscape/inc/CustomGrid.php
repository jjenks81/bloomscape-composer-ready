<?php

namespace Bloomscape;

/**
 * Trait CustomGrid
 *
 * @package Bloomscape
 */
trait CustomGrid {
	/**
	 * Remove default woocomerce stuff
	 */
	public function remove_default_woo_stuff() {
		add_filter( 'woocommerce_show_page_title', [ $this, 'remove_shop_title' ] );
		add_filter( 'after_setup_theme', [ $this, 'remove_default_sort_stuff' ] );
		add_filter( 'after_setup_theme', [ $this, 'remove_add_to_cart_button' ] );
		add_filter( 'after_setup_theme', [ $this, 'remove_pagination' ] );
		add_action( 'woocommerce_shop_loop_item_title', [ $this, 'append_size_attribute' ], 9 );
		add_action( 'woocommerce_after_shop_loop', [ $this, 'append_modules' ],100 );
	}

	/**
	 * Remove shop title
	 *
	 * @return bool
	 */
	public function remove_shop_title() {
		return false;
	}

	/**
	 * Remove sorting stuff shop filters
	 */
	public function remove_default_sort_stuff() {
		remove_action( 'woocommerce_after_shop_loop', 'storefront_sorting_wrapper', 9 );
		remove_action( 'woocommerce_before_shop_loop', 'storefront_sorting_wrapper', 9 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
		remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'storefront_woocommerce_pagination', 30 );
		remove_action( 'woocommerce_after_shop_loop', 'storefront_woocommerce_pagination', 30 );
	}

	/**
	 * Remove add to cart button
	 */
	public function remove_add_to_cart_button() {
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	}

	/**
	 * Remove pagination
	 */
	public function remove_pagination() {
		remove_action( 'homepage', 'storefront_product_categories',    20 ); // home!
		remove_action( 'homepage', 'storefront_recent_products',    30 ); // home!
		remove_action( 'homepage', 'storefront_featured_products',     40 );
		remove_action( 'homepage', 'storefront_popular_products',      50 );
		remove_action( 'homepage', 'storefront_on_sale_products',      60 );
		remove_action( 'homepage', 'storefront_best_selling_products', 70 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_pagination', 30 );
		remove_action( 'woocommerce_before_shop_loop', 'storefront_woocommerce_pagination', 30 );
	}

	/**
	 * Append size attribute to grid
	 */
	public function append_size_attribute() {
		global $post;
		echo '<div class="bloomscape-size">' . get_field( 'plant_size', $post->ID ) . '</div>'; // WPCS: XSS OK.
	}

	/**
	 * Append extra modules to shop all page
	 */
	public function append_modules() {
		get_template_part( '/partials/three-column-image' );
		get_template_part( '/partials/hero-footer-image-module' );
	}

}
