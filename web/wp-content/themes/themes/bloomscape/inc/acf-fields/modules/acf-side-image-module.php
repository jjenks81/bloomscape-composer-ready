<?php

	// Location array for ACF module, default to Post Type Product.
	$location = [];

	// Pages by slug or ID that want to be added.
	$pages = [ 'careers', 'contacts', 'talk-to-plantmom' ];

foreach ( $pages as $page ) {
	if ( is_int( $page ) ) {
		$page_id = (string) $page;
	} else {
		$page_id = get_page_by_path( $page );
		if ( $page_id ) {
			$page_id = $page_id->ID;
		}
	}
	if ( $page_id ) {
		$location[] = array(
			array(
				'param'    => 'page',
				'operator' => '==',
				'value'    => (string) $page_id,
			),
		);
	}
}
		acf_add_local_field_group(
			array(
				'key' => 'group_5a82e2e29481d',
				'title' => 'Inner Page Content',
				'fields' => array(
					array(
						'key' => 'field_5a82e2edb9673',
						'label' => 'Text Content',
						'name' => 'bloom_inner_text_content',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
						'delay' => 1,
					),
					array(
						'key' => 'field_5a82e31cb9674',
						'label' => 'Side Image',
						'name' => 'bloom_inner_side_image',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '550',
						'max_size' => '',
						'mime_types' => 'jpg,png',
					),
				),
				'location' => $location,
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			)
		);
