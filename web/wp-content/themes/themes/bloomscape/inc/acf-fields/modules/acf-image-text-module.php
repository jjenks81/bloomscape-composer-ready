<?php

// Location array for ACF module, default to Post Type Product.
$location = array(
	array(
		array(
			'param' => 'post_type',
			'operator' => '==',
			'value' => 'product',
		),
	),
);

// Pages by slug or ID that want to be added.
$pages = [ 'about', 'cart' ];

foreach ( $pages as $page ) {
	if ( is_int( $page ) ) {
		$page_id = (string) $page;
	} else {
		$page_id = get_page_by_path( $page );
		if ( $page_id ) {
			$page_id = $page_id->ID;
		}
	}
	if ( $page_id ) {
		$location[] = array(
			array(
				'param' => 'page',
				'operator' => '==',
				'value' => (string) $page_id,
			),
		);
	}
}

acf_add_local_field_group(
	array(
		'key' => 'group_5a819d26586d8',
		'title' => 'Image & Text Modules',
		'fields' => array(
			array(
				'key' => 'field_5a819df3cd7ae',
				'label' => 'Image Text Modules',
				'name' => 'image_text_modules',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'row',
				'button_label' => 'Add Module',
				'sub_fields' => array(
					array(
						'key' => 'field_5a819e13cd7af',
						'label' => 'Image Placement',
						'name' => 'image_placement',
						'type' => 'radio',
						'instructions' => 'Where do you want the image(s) placed?',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array(
							'left' => 'Left',
							'right' => 'Right',
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '',
						'layout' => 'vertical',
						'return_format' => 'value',
					),
					array(
						'key' => 'field_5a819e31cd7b0',
						'label' => 'Images',
						'name' => 'images',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 1,
						'max' => 3,
						'layout' => 'table',
						'button_label' => 'Add Image',
						'sub_fields' => array(
							array(
								'key' => 'field_5a819e4fcd7b1',
								'label' => 'image',
								'name' => 'image',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'id',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
						),
					),
					array(
						'key' => 'field_5a819e6bcd7b2',
						'label' => 'Text',
						'name' => 'text',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 0,
						'delay' => 1,
					),
				),
			),
		),
		'location' => $location,
		'menu_order' => 3,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	)
);
