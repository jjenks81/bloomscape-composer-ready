<?php

	// Pages by slug or ID that want to be added.
	$pages = [ 'about', 'our-story' ];

foreach ( $pages as $page ) {
	if ( is_int( $page ) ) {
		$page_id = (string) $page;
	} else {
		$page_id = get_page_by_path( $page );
		if ( $page_id ) {
			$page_id = $page_id->ID;
		}
	}
	if ( $page_id ) {
		$location[] = array(
			array(
				'param'    => 'page',
				'operator' => '==',
				'value'    => (string) $page_id,
			),
		);
	}
}

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5a820450041e2',
			'title'                 => 'Images',
			'fields'                => array(
				array(
					'key'               => 'field_5a820573bdc52',
					'label'             => 'Hero Image',
					'name'              => 'bloom_custom_hero_image',
					'type'              => 'image',
					'instructions'      => 'Please add a 1200 x 712 or 769 image',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'return_format'     => 'array',
					'preview_size'      => 'thumbnail',
					'library'           => 'all',
					'min_width'         => 1200,
					'min_height'        => 712,
					'min_size'          => '',
					'max_width'         => 1200,
					'max_height'        => 769,
					'max_size'          => '',
					'mime_types'        => 'jpg,png',
				),
			),
			'location'              => $location,
			'menu_order'            => 1,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);
