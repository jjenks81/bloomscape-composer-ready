<?php

		acf_add_local_field_group(
			array(
				'key' => 'group_5a8ddb3ceffd0',
				'title' => 'FAQ',
				'fields' => array(
					array(
						'key' => 'field_5a8ddb767dd2b',
						'label' => 'Accordion',
						'name' => 'bloom_accordion',
						'type' => 'repeater',
						'instructions' => 'Fill the FAQs',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 50,
						'layout' => 'table',
						'button_label' => '',
						'sub_fields' => array(
							array(
								'key' => 'field_5a8ddd558ced1',
								'label' => 'title',
								'name' => 'bloom_faq_title',
								'type' => 'text',
								'instructions' => '',
								'required' => 1,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array(
								'key' => 'field_5a8ddd6b8ced2',
								'label' => 'answer',
								'name' => 'bloom_faq_answer',
								'type' => 'wysiwyg',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'tabs' => 'all',
								'toolbar' => 'full',
								'media_upload' => 1,
								'delay' => 1,
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'page_template',
							'operator' => '==',
							'value' => 'accordion-page.php',
						),
					),
					array(
						array(
							'param' => 'page_template',
							'operator' => '==',
							'value' => 'plant-care-archive.php',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			)
		);
