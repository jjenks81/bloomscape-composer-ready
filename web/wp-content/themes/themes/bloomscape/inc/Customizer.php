<?php

namespace Bloomscape;

/**
 * Class Customizer
 *
 * @package Bloomscape
 */
class Customizer {

	use Singleton;

	/**
	 * Customizer constructor.
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'bloomscape_scripts' ], 1 );
		$this->register_logo();
		$this->register_menus();
		$this->add_acf_settings();
	}

	/**
	 * Bloomscape Scripts
	 */
	public function bloomscape_scripts() {
		wp_enqueue_script(
			'app', get_stylesheet_directory_uri() . '/build/app.bundle.js', [ 'jquery' ], '1.0.0',
			true
		);
		wp_localize_script(
			'app', 'filters_app', [
				'root'  => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' ),
			]
		);
	}

	/**
	 * Register Logo
	 */
	public function register_logo() {
		add_theme_support(
			'custom-logo', array(
				'height'     => 110,
				'width'      => 470,
				'flex-width' => true,
			)
		);
	}

	/**
	 * Register menus into worddpress
	 */
	public function register_menus() {
		register_nav_menus(
			array(
				'primary'           => __( 'Primary Menu', 'bloomscape' ),
				'secondary'         => __( 'Secondary Menu', 'bloomscape' ),
				'handheld'          => __( 'Handheld Menu', 'bloomscape' ),
				'plantcare-submenu' => __( 'Plant Care Sub Menu', 'bloomscape' ),
				'legals'            => __( 'Legals', 'bloomscape' ),
				'company'           => __( 'Company', 'bloomscape' ),
				'plants'            => __( 'Plant questions?', 'bloomscape' ),
				'support'           => __( 'Support', 'bloomscape' ),
			)
		);
	}

	/**
	 * Bloomscape hero block
	 *
	 * @param string $content Home Page Content.
	 * @return string
	 */
	public function bloomscape_homepage_hero( $content = '' ) {
		$link          = get_field( 'cta_hero_homepage' );
		$homepage_hero = '<div class="entry-hero">';
		if ( $link ) {
			$url = esc_url( $link['url'] );
			$title = esc_html( $link['title'] );
			$homepage_hero .= '<div class="hero-text">' . $content . '</div>';
			$homepage_hero .= <<<CTA
<a class="btn btn-bloom" href="${url}"
                   target="${link['target']}">{$title}</a>
CTA;
		}
		$homepage_hero .= '</div>';
		return $homepage_hero;
	}

	/**
	 * Get hero background size image
	 */
	public function bloomscape_homepage_content_styles() {
		$featured_image   = get_the_post_thumbnail_url( get_the_ID() );
		$background_image = '';

		if ( $featured_image ) {
			$background_image = 'url(' . esc_url( $featured_image ) . ')';
		}

		$styles = array();

		if ( '' !== $background_image ) {
			$styles['background-image'] = $background_image;
			$styles['width']            = '100%';
		}

		foreach ( $styles as $style => $value ) {
			echo esc_attr( $style . ': ' . $value . '; ' );
		}
	}

	/**
	 * Add ACF settings
	 */
	public function add_acf_settings() {
		if ( function_exists( 'acf_add_options_page' ) ) {

			acf_add_options_page( 'Bloomscape Settings' );

		}
	}

}
