<?php
	/**
	 * The template for displaying search results pages.
	 *
	 * @package storefront
	 */
	get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<div class="page-header">
					<h2 class="page-title">Search Results for: <?php echo '<span>' . esc_html( get_search_query() ) . '</span>'; ?></h2>
				</div><!-- .page-header -->

				<?php
				get_template_part( 'loop' );
			else :
				get_template_part( 'content', 'none' );
			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
	do_action( 'storefront_sidebar' );
	get_footer();
