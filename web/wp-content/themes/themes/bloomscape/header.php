<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package bloomscape
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport"
		  content="width=device-width, initial-scale=1, maximum-scale=1.0,minimum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>
	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
		<div class="nav-mobile">
			<a href="#toggle-nav" class="menu-mobile">
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/MenuHandler@2x.png'; // WPCS: XSS OK. ?>"
					 alt="Menu Open">
			</a>
		</div>
		<div class="row top-header">
			<nav class="col-md-4 main-nav top-nav" role="navigation">
				<?php
					wp_nav_menu(
						[
							'theme_location'  => 'primary',
							'container_class' => 'primary-navigation',
						]
					);
				?>
			</nav>
			<div class="col-md-4 top-logo">
				<div class="logo-wrapper">
					<?php storefront_site_title_or_logo(); ?>
				</div>
			</div>
			<nav class="col-md-4 secondary-nav top-nav" role="navigation">
				<?php
					wp_nav_menu(
						[
							'theme_location' => 'secondary',
						]
					);
				?>
				<div class="cart-desktop">
					<a href="<?php echo esc_url( wc_get_cart_url() ); ?>"
					   title="<?php esc_attr_e( 'View your shopping cart', 'bloomscape' ); ?>" class="menu-cart-mobile">
						<span class="cart-total count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ); ?></span>
					</a>
				</div>
			</nav>
		</div>
		<div class="cart-mobile">
			<a href="<?php echo esc_url( wc_get_cart_url() ); ?>"
			   title="<?php esc_attr_e( 'View your shopping cart', 'bloomscape' ); ?>" class="menu-cart-mobile">
				<span class="cart-total count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ); ?></span>
			</a>
		</div>
	</header><!-- #masthead -->
	<div id="overlay" class="overlay">

	</div>
	<div id="sidebar" class="row mobile-menu sidebar">
		<a href="javascript:void(0)" class="close-mobile-nav">&times;</a>
		<nav class="col-md-4 mobile-navigation" role="navigation">
			<?php
				wp_nav_menu(
					[
						'theme_location'  => 'primary',
						'container_class' => 'primary-navigation',
					]
				);
			?>
			<?php
				wp_nav_menu(
					[
						'theme_location' => 'secondary',
					]
				);
			?>
		</nav>
	</div>
	<?php
		/**
		 * Functions hooked in to storefront_before_content
		 *
		 * @hooked storefront_header_widget_region - 10
		 */
		do_action( 'storefront_before_content' );
	?>

	<div id="content" class="site-content" tabindex="-1">


<?php
	/**
	 * Functions hooked in to storefront_content_top
	 *
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_content_top' );
