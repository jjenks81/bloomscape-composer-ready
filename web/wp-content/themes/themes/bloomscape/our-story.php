<?php
	/**
	 * Template Name: Our Story
	 */
	get_header();
	the_post();
?>

<?php get_template_part( '/partials/hero-image-module' ); ?>
	<div class="inner-wrapper">
		<?php
			the_content();
		?>
	</div>
	<div class="left-parallax-image-our-card rellax" data-rellax-speed="-4"></div>
	<div class="left-parallax-image-our-card-right rellax" data-rellax-speed="3"></div>
<?php
	get_footer();
