<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
 * Class MobileTest
 *
 * @package Tests\Browser
 */
class MobileTest extends DuskTestCase {

	/**
	 * Feature Mobile Test
	 *
	 * @throws \Exception Exception.
	 * @throws \Throwable Manage \Throwable Exceptions.
	 */
	public function test_if_home_renders_ok() {
		$this->browse(
			function ( Browser $browser ) {
				$browser->resize( 375, 812 );
				$browser->visit( '/' )
					->assertSee( 'bloomscape' );
			}
		);
	}
}
