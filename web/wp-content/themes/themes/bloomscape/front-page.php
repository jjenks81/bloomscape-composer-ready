<?php
/**
 * Template for the Home Page
 */
get_header();
the_post();
?>

<?php get_template_part( '/partials/hero-cta-module' ); ?>
<?php get_template_part( '/partials/three-column-image' ); ?>
<?php get_template_part( '/partials/youtube-module' ); ?>
<?php get_template_part( '/partials/three-card-module' ); ?>
<?php get_template_part( '/partials/newsletter-module' ); ?>
<?php get_template_part( '/partials/instagram-module' ); ?>


<?php
get_footer();
