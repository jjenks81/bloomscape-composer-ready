<?php
global $filtered_results, $wp_query, $unmatched_query;

if ( ! is_a( $filtered_results, 'WP_Query' ) ) {
	$filtered_results = $wp_query;
}

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header( 'shop' ); ?>

<?php
/**
 * Woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>

	<header class="woocommerce-products-header">

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
		/**
		 * Woocommerce_archive_description hook.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action( 'woocommerce_archive_description' );
		?>

	</header>

<?php
/**
 * Woocommerce_before_shop_loop hook.
 *
 * @hooked wc_print_notices - 10
 * @hooked woocommerce_result_count - 20
 * @hooked woocommerce_catalog_ordering - 30
 */
do_action( 'woocommerce_before_shop_loop' );
?>

<?php woocommerce_product_loop_start(); ?>


	<ul id="preload-shop-items">
		<?php
		while ( have_posts() ) :
			the_post();
?>

			<?php
			/**
			 * Woocommerce_shop_loop hook.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );
			?>

			<?php wc_get_template_part( 'content', 'product' ); ?>

		<?php endwhile; // end of the loop. ?>
	</ul>

	<div id="filter-results" style="clear: both;">
		<script id="filter-results-template" type="text/x-handlebars-template">
			{{#each filtered_results}}
			<li class="post-{{ID}} product">
				<a href="{{product_data.link}}"
				   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
					{{{product_data.image}}}
					<div class="bloomscape-size">{{product_data.size}}</div>
					<h2 class="woocommerce-loop-product__title">{{post_title}}</h2>
					<span class="price"><span class="woocommerce-Price-amount amount"><span
									class="woocommerce-Price-currencySymbol">$</span>{{product_data.price}}</span></span>
				</a>
			</li>
			{{/each}}

			{{#each unmatched_results}}
			<li class="post-{{ID}} product unmatched">
				<a href="{{product_data.link}}"
				   class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
					{{{product_data.image}}}
					<div class="bloomscape-size">{{product_data.size}}</div>
					<h2 class="woocommerce-loop-product__title">{{post_title}}</h2>
					<span class="price"><span class="woocommerce-Price-amount amount"><span
									class="woocommerce-Price-currencySymbol">$</span>{{product_data.price}}</span></span>
				</a>
			</li>
			{{/each}}
		</script>
	</div>


<?php woocommerce_product_loop_end(); ?>

<?php
/**
 * Woocommerce_after_shop_loop hook.
 *
 * @hooked woocommerce_pagination - 10
 */
do_action( 'woocommerce_after_shop_loop' );
?>

<?php
/**
 * Woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
?>

<?php get_footer( 'shop' ); ?>
