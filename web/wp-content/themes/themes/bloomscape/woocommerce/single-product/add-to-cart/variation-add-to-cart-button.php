<?php
/**
 * Single variation cart button
 *
 * @see    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input(
		array(
			'min_value'   => apply_filters(
				'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(),
				$product
			),
			'max_value'   => apply_filters(
				'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(),
				$product
			),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( sanitize_text_field( wp_unslash( $_POST['quantity'] ) ) ) : $product->get_min_purchase_quantity(),
		)
	);
	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
</div>
