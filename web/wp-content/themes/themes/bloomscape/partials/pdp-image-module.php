<?php
$pdp_image_module = get_field( 'image_module' );

if ( $pdp_image_module ) :
	$img = wp_get_attachment_image_src( $pdp_image_module, 'full' );
?>
	<div id="pdp-image-module-wrapper" class="pdp-image-module-wrapper">
		<div class="image-container">
			<?php if ( is_array( $img ) ) : ?>
				<img src="<?php echo esc_url( $img[0] ); ?>" alt="Bloomscape">
			<?php endif; ?>
		</div>
	</div>
<?php
endif;
?>
