<?php if ( have_rows( 'three_card' ) ) : ?>
<div id="three-card-module-wrapper" class="three-card-module-wrapper">
<div class="left-parallax-image-leafs-cards rellax" data-rellax-speed="-4"></div>
	<?php
	$i = 0;
	while ( have_rows( 'three_card' ) ) :
		the_row();
	?>
		<div class="three-card-single card-<?php echo esc_html( $i ); ?>">
			 <div class="three-card-image">
					<?php $img = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' ); ?>
					<?php if ( is_array( $img ) ) : ?>
				 <img alt="Bloomscape" src="<?php echo esc_url( $img[0] ); ?>" >
					<?php endif; ?>
				 <a class="btn btn-bloom" href="<?php echo esc_url( get_sub_field( 'link_url' ) ); ?>"><?php echo esc_html( get_sub_field( 'link_text' ) ); ?></a>
			 </div>
		 </div>
	<?php
	$i++;
endwhile;
?>

</div>
<?php endif; ?>
