<?php if ( have_rows( 'bloom_accordion' ) ) : ?>
	<div id="accordion-module-wrapper" class="accordion-module-wrapper">
		<?php
			$i = 0;
		while ( have_rows( 'bloom_accordion' ) ) :
			the_row();
			?>
			<div class="faq panel">
					<a class="accordion-title" data-toggle="collapse" data-parent="#accordion-module-wrapper" href="#collapse_<?php echo esc_html( $i ); ?>">
							<?php echo esc_html( get_sub_field( 'bloom_faq_title' ) ); ?>
					</a>
				<div id="collapse_<?php echo esc_html( $i ); ?>" class="faq-body collapse <?php echo (0 === $i) ? 'in' : ''; ?>">
					<?php echo wp_kses_post( get_sub_field( 'bloom_faq_answer' ) ); ?>
				</div>
				</div>
				<?php
				$i++;
			endwhile;
		?>
	</div>
<?php endif; ?>
