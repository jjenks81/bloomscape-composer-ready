<?php if ( have_rows( 'bloom_title_box' ) ) :
	while ( have_rows( 'bloom_title_box' ) ) :
		the_row();
		$title = get_sub_field( 'bloom_title_box' );
		$description = get_sub_field( 'bloom_content_box' );
?>
	<div class="title-box-module-wrapper">
			<h2><?php echo esc_html( $title ); ?></h2>
			<p><?php echo esc_html( $description ); ?></p>
	</div>
		<div class="left-parallax-image-about-card rellax" data-rellax-speed="-4"></div>
		<div class="left-parallax-image-about-card-right rellax" data-rellax-speed="3"></div>
<?php endwhile; ?>
<?php endif; ?>
