<?php
	$bloomscape_hero_image_field = get_field( 'bloom_custom_hero_image' );
?>
<?php if ( $bloomscape_hero_image_field ) : ?>
	<div class="hero-image-module-wrapper">
		<?php $img = wp_get_attachment_image_src( $bloomscape_hero_image_field['ID'], 'full' ); ?>
		<?php if ( is_array( $img ) ) : ?>
			<img alt="<?php echo esc_html( $bloomscape_hero_image_field['alt'] ); ?>" src="<?php echo esc_url( $img[0] ); ?>">
		<?php endif; ?>
	</div>
<?php endif; ?>
