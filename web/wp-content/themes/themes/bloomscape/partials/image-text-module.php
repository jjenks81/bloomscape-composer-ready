<?php
/**
 * Image & Text Module - powered by acf-fields/modules/acf-image-text-module.php
 */

if ( have_rows( 'image_text_modules' ) ) :
?>
	<div id="image-text-modules-wrapper" class="image-text-modules-wrapper">
		<?php
		while ( have_rows( 'image_text_modules' ) ) :
			the_row();
		?>
		<div class="image-text-module <?php echo esc_html( get_sub_field( 'image_placement' ) ); ?>">
			<div class="images">
				<?php
				if ( have_rows( 'images' ) ) :
					while ( have_rows( 'images' ) ) :
						the_row();
					?>
					<?php $img = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' ); ?>
						<?php if ( is_array( $img ) ) : ?>
					<img src="<?php echo esc_html( $img[0] ); ?>" alt="Bloomscape">
						<?php endif; ?>
					<?php
					endwhile;
					endif;
				?>
				</div>
				<div class="text">
				<?php echo wp_kses_post( get_sub_field( 'text' ) ); ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php
endif;
?>
