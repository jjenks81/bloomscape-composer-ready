<?php if ( have_rows( 'bloom_social_networks', 'option' ) ) : ?>
	<?php
	while ( have_rows( 'bloom_social_networks','option' ) ) :
		the_row();
		?>
<div class="col-md-12">
	<div class="social-footer">
		<ul>
			<li class="social-icon"><a href="<?php echo esc_url( get_sub_field( 'bloom_instagram' ) ); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li class="social-icon"><a href="<?php echo esc_url( get_sub_field( 'bloom_facebook' ) ); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li class="social-icon"><a href="<?php echo esc_url( get_sub_field( 'bloom_twitter' ) ); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li class="social-icon"><a href="<?php echo esc_url( get_sub_field( 'bloom_pinterest' ) ); ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
		</ul>
	</div>
</div>
		<?php endwhile; ?>
<?php endif; ?>
