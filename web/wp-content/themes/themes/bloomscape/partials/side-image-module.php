<?php
	$inner_image_module = get_field( 'bloom_inner_side_image' );
if ( $inner_image_module ) :
	$img = wp_get_attachment_image_src( $inner_image_module['ID'], 'full' );
	?>
	<div class="side-image-module-wrapper">
		<div class="image-container">
			<?php if ( is_array( $img ) ) : ?>
					<img src="<?php echo esc_url( $img[0] ); ?>" alt="Side image">
				<?php endif; ?>
		</div>
		</div>
	<?php
	endif;
?>
