<div class="plants-made">
	<div class="row">
		<div class="col-md-12">
			<h3><?php the_field( 'bloom_plants_made_simple_title', 'option' ); ?></h3>
		</div>
	</div>
	<?php if ( have_rows( 'bloom_plants_made_simple_plant_box_repeater' ,'option' ) ) : ?>
		<div class="row">
			<?php
			while ( have_rows( 'bloom_plants_made_simple_plant_box_repeater','option' ) ) :
				the_row();
				$image = get_sub_field( 'image' );
				?>
				<div class="col-md-4 content-box">
					<div class="box-image">
						<img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo esc_html( $image['alt'] ); ?>">
					</div>
					<div class="box-label">
						<h4><?php echo esc_html( get_sub_field( 'label' ) ); ?></h4>
					</div>
					<div class="box-text">
						<p><?php echo esc_html( get_sub_field( 'description' ) ); ?></p>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php else : ?>
	no rows
	<?php endif; ?>
</div>
