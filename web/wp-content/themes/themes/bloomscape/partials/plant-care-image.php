<?php

	$image_field = get_field( 'bloom_post_image' );
	$image = ( $image_field ) ? wp_get_attachment_image_src( $image_field['ID'], 'full' ) : null;
?>

<div id="plant-care-post-image" class="plant-care-post-image">
	<?php if ( is_array( $image ) ) : ?>
	<img src="<?php echo esc_url( $image[0] ); ?>" alt="Post Image">
	<?php endif; ?>
</div>
