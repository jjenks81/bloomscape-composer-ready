<div class="meta-info">
	<div class="author-date">
		<?php
			echo '<div class="label">' . esc_attr( __( 'Written by', 'storefront' ) ) . '</div>';
			echo sprintf( '<a href="%1$s" class="url fn" rel="author">%2$s</a>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_the_author() );
		?>
	</div>
	<div class="social-share">
		<ul>
			<li class="social-icon"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li class="social-icon"><a href="https://twitter.com/intent/tweet?url=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li class="social-icon"><a href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
		</ul>
	</div>
</div>
