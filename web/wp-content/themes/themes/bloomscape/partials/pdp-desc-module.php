<?php
$pdp_botanical_name = get_field( 'pdp_botanical_name' );
$pdp_common_names = get_field( 'pdp_common_names' );
$pdp_dimensions = get_field( 'pdp_dimensions' );
$pdp_description_image = get_field( 'description_image' );

if ( $pdp_botanical_name || $pdp_common_names || $pdp_dimensions || have_rows( 'pdp_whats_included_module_items' ) ) {
	echo '<div id="pdp-description-module-wrapper" class="pdp-description-module-wrapper">';
	echo '<div class="pdp-description-image">';
	$img = wp_get_attachment_image_src( $pdp_description_image, 'full' );
	if ( is_array( $img ) ) {
		echo '<img src="' . esc_url( $img[0] ) . '" alt="Image PDP">';
	}
	echo '</div>';
	echo '<div class="pdp-description-info">';
}


// Botanical Name Module.
if ( $pdp_botanical_name ) :
	echo '<div id="pdp-botanical-name-wrapper" class="pdp-botanical-name-wrapper">';
		echo '<h2>botanical name</h2>';
		echo wp_kses_post( $pdp_botanical_name );
	echo '</div>';
endif;

// Common Names Module.
if ( $pdp_common_names ) :
	echo '<div id="pdp-botanical-name-wrapper" class="pdp-botanical-name-wrapper">';
		echo '<h2>common names</h2>';
		echo wp_kses_post( $pdp_common_names );
	echo '</div>';
endif;

echo '<div id="pdp-description-wrapper" class="pdp-description-wrapper">';
	echo '<h2>full description</h2>';
	the_content();
echo '</div>';

// Dimensions Module.
if ( $pdp_dimensions ) :
	echo '<div id="pdp-botanical-name-wrapper" class="pdp-botanical-name-wrapper">';
		echo '<h2>dimensions</h2>';
		echo wp_kses_post( $pdp_dimensions );
	echo '</div>';
endif;

// What's Included Module.
if ( have_rows( 'pdp_whats_included_module_items' ) ) :
	echo '<div id="pdp-whats-included-wrapper" class="pdp-whats-included-wrapper">';
	echo '<div class="pdp-whats-included-subwrapper">';
		echo '<h3>What\'s included</h3>';
		echo '<ul>';
	while ( have_rows( 'pdp_whats_included_module_items' ) ) :
		the_row();
		echo '<li>' . esc_html( get_sub_field( 'whats_included_item' ) ) . '</li>';
			endwhile;
		echo '</ul>';
	echo '</div>';
	echo '</div>';
endif;

if ( $pdp_botanical_name || $pdp_common_names || $pdp_dimensions ) {
	echo '</div>';
	echo '</div>';
}
