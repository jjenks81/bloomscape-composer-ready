<div class="people-reviews-module review-slider">
	<?php if ( have_rows( 'customer_reviews', 'option' ) ) : ?>
		<div class="label-module">
			<h3><?php echo esc_html( get_field( 'bloom_label_customer_reviews','option' ) ); ?></h3>
		</div>
		<div class="module-carousel">
			<?php
			while ( have_rows( 'customer_reviews', 'option' ) ) :
				the_row();
				?>
				<div class="review-box">
					<div class="box-wrapper">
						<h4 class="review-title">
							<?php echo esc_html( get_sub_field( 'bloom_review_people_title' ) ); ?>
						</h4>
						<div class="review-name">
							<span><?php echo esc_html( get_sub_field( 'bloom_name_of_reviewer' ) ); ?></span>
						</div>
					</div>
					</div>
				<?php endwhile; ?>
		</div>
	<?php endif; ?>
</div>
