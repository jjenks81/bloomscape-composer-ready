<?php if ( have_rows( 'instagram_module_images' ,'option' ) ) : ?>
	<div id="instagram-module-wrapper" class="instagram-module-wrapper">
		<h2 class="instagram-module-title"><?php echo esc_html( get_field( 'instagram_module_title','option' ) ); ?></h2>
		<div class="instagram-pictures image-carousel">
		<?php
		$i = 0;
		while ( have_rows( 'instagram_module_images','option' ) ) :
			the_row();
			$img = wp_get_attachment_image_src( get_sub_field( 'image' ), 'full' );
?>
		<?php if ( is_array( $img ) ) : ?>
					<div class="instagram-module-single instagram-single-<?php echo esc_html( $i ); ?>">
						<a href="<?php echo esc_url( get_sub_field( 'image_url' ) ); ?>">
							<img src="<?php echo esc_url( $img[0] ); ?>" alt="Instagram image" >
						</a>
					</div>
			<?php endif; ?>
				<?php
				$i++;
endwhile;
?>
		</div>
		<a class="instagram-link" target="_blank" href="<?php echo esc_url( get_field( 'instagram_module_link_url','option' ) ); ?>">
			<?php echo esc_html( get_field( 'instagram_module_link_title' , 'option' ) ); ?>
		</a>
		<div class="left-parallax-image-insta-cards-top rellax" data-rellax-speed="-2" data-rellax-zindex="8"></div>
		<div class="left-parallax-image-insta-cards-bottom rellax" data-rellax-speed="-3" data-rellax-zindex="3"></div>
	</div>

<?php endif; ?>
