<?php $newsletter_module_title = get_field( 'newsletter_module_title' ); ?>
<div id="newsletter-module-wrapper" class="newsletter-module-wrapper">
	<div class="newsletter-inner">
		<?php if ( $newsletter_module_title ) : ?>
			<h3 class="newsletter-module-title">
				<?php echo esc_html( $newsletter_module_title ); ?>
			</h3>
		<?php endif; ?>

		<!-- FORM FROM MAILCHIMP FOR LIST: Prelaunch Sign Up -->
		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup">
			<form action="https://bloomscape.us17.list-manage.com/subscribe/post?u=bbb1b5deb6c86117fbf04e5c6&amp;id=9398128580" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
					<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="enter your email" required>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bbb1b5deb6c86117fbf04e5c6_9398128580" tabindex="-1" value=""></div>
					<div class="newsletter-button">
						<a href="#submit" class="bloomscape-button"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</form>
		</div>
		<!--End mc_embed_signup-->
	</div>
</div>
