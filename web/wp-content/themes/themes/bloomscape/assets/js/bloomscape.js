window.$ = jQuery = window.jQuery = require('jquery');
require('bootstrap-sass');
require('bootstrap-select');
require('slick-carousel');
let Rellax = require('rellax/rellax');

let bloomscape = bloomscape || {};
let throttle = require('lodash/throttle');

const SIDEBAR_WIDTH = 250;
const MOBILE_VIEWPORT = 480;
const TABLET_VIEWPORT = 768;
const DESKTOP_VIEWPORT = 1024;
const DESKTOP_LARGE_VIEWPORT = 1280;
const DESKTOP_ULTRA_VIEWPORT = 2024;
const SCROLL_THROTTLE = 10;

(function ($) {
    bloomscape.app = (function (app) {

        app.body = $(document.body);
        app.sidebar = $('#sidebar');
        app.overlay = $('#overlay');
        app.mobileMenuButton = $('.menu-mobile');
        app.mobileMenuCloseButton = $('.close-mobile-nav');
        app.pdpQuantitySelect = $('.bloom-checkout select');
        app.pdpMobileHelper = $('.bottom-helper a');
        app.pdpAttributesWrapper = $('#plant-custom-attrs-wrapper');
        app.pdpSlider = $('.slider');
        app.pdpVariationsWrapper = $('.swatch-wrapper');
        app.cartCount = $('.menu-cart-mobile span.count');
        app.cartButton = $('.menu-cart-mobile');
        app.sideSortMenu = $('.side-wrapper');
        app.sliders = $('.module-carousel');
        app.slidersImg = $('.image-carousel');
        app.customButtons = $('.bloomscape-button');
        app.cartTextButton = $('#menu-secondary-menu li.menu-item:last-child a');
        app.imageGrid = $('.bloom-grid');
        app.modalLogin = $('#sign-in-error-modal');
        app.sideSortMenuSize = $('.bloom_woo_filters').height();
        app.searchBtn = $('.bloom-search a');
        app.searchForm = $('.search-form');
        app.searchField = $('.search-field');
        app.closeSearchForm = $('.close-action');
        app.selectQuantity = $('.qty');

        $(document).ready(function () {
            app.init();
        });

        app.init = () => {
            app.pdpQuantitySelect.selectpicker();
            app.registerEvents();
        };

        app.registerEvents = () => {

            app.overlay.click(event => {
                app.toggleMenu();
            });

            app.mobileMenuButton.click(event => {
                app.toggleMenu();
            });

            app.mobileMenuCloseButton.click(event => {
                app.toggleMenu();
            });

            app.body.on('added_to_cart', function (event, payload) {
                let cartResponse = payload['a.footer-cart-contents'];
                let total = cartResponse.match(/<span class="count">(.*)<\/span>/);
                app.cartCount.text(total[1]);
            });

            app.body.on('wc_fragments_loaded', function () {
                let quantity = $('.xoo-wsc-qty').map(function () {
                    return parseInt($(this).val());
                }).toArray().reduce(function (a, b) {
                    return a + b;
                }, 0);
                let total = (quantity === undefined) ? 0 : quantity;
                app.cartCount.text(total);
            });

            app.pdpMobileHelper.click(event => {
                if (app.pdpAttributesWrapper.is(':visible')) {
                    app.pdpAttributesWrapper.hide();
                } else {
                    app.pdpAttributesWrapper.show();
                }
            });

            app.selectQuantity.change(function () {
                $('.wrapper-cart-button, .button-cta').find('.quantity').val($(this).val());
            });

            app.handleShopScroll();


            app.handlePDPSlider();

            app.cartButton.click(function (e) {
                e.preventDefault();
                app.toggleSideCar();
            });

            app.cartTextButton.click(function (e) {
                e.preventDefault();
                app.toggleSideCar();
            });

            app.sliders.slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: MOBILE_VIEWPORT,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: TABLET_VIEWPORT,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: DESKTOP_VIEWPORT,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    }
                ],
                prevArrow: '<a href="#movePrev" class="prevArrow"><i class="fa fa-chevron-left"></i></a>',
                nextArrow: '<a href="#moveNext" class="nextArrow"><i class="fa fa-chevron-right"></i></a>',
            });

            app.slidersImg.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: MOBILE_VIEWPORT,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: TABLET_VIEWPORT,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: DESKTOP_VIEWPORT,
                        settings: 'unslick'
                    },
                    {
                        breakpoint: DESKTOP_ULTRA_VIEWPORT,
                        settings: 'unslick'
                    }
                ],
                prevArrow: '<a href="#movePrev" class="prevArrow"><i class="fa fa-chevron-left"></i></a>',
                nextArrow: '<a href="#moveNext" class="nextArrow"><i class="fa fa-chevron-right"></i></a>',
            });

            app.customButtons.click(function (e) {
                e.preventDefault();
                $(this).closest('form').submit();
            });

            $('body').on('click', '.sign-in-init a', app.signInInit);

            if (app.modalLogin.length) {
                app.modalLogin.modal('show');
            }

            app.searchBtn.click(function (e) {
                e.preventDefault();
                app.searchForm.addClass('visible');
                app.searchField.focus();
            });

            app.closeSearchForm.click(function (e) {
                app.searchForm.removeClass('visible');
            });

            new Rellax('.rellax', {

            });

        };

        app.toggleMenu = () => {
            if (app.sidebar.width() === SIDEBAR_WIDTH) {
                app.sidebar.css('width', '0');
                app.overlay.css('right', '-100%');
            } else {
                app.sidebar.css('width', '250px');
                app.overlay.css('right', '0');
            }
        };

        app.handleShopScroll = () => {

            let max = app.imageGrid.height() - app.sideSortMenuSize;

            if ($(window).width() > MOBILE_VIEWPORT) {
                window.addEventListener('scroll', throttle(event => {
                    let top = $(document).scrollTop();
                    if (top < max)
                        app.sideSortMenu.animate({'top': top}, 10);
                }, SCROLL_THROTTLE));
            }
        };

        app.handlePDPSlider = () => {
            if (app.pdpSlider.length > 0) {
                app.pdpVariationsWrapper.click(function () {
                    app.pdpSlider[0].slick.slickGoTo(0, false);
                })
            }
        };

        app.toggleSideCar = (toggle_type) => {
            let toggle_element = $('.xoo-wsc-modal , body'),
                toggle_class = 'xoo-wsc-active';

            if (toggle_type == 'show') {
                toggle_element.addClass(toggle_class);
            }
            else if (toggle_type == 'hide') {
                toggle_element.removeClass(toggle_class);
            }
            else {
                toggle_element.toggleClass('xoo-wsc-active');
            }
        };

        app.signInInit = function (e) {
            e.preventDefault();
            $('#sign-in-modal').modal('show');
        }

    }(bloomscape.app || {}));
}(jQuery));