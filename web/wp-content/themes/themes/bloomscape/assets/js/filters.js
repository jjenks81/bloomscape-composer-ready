const Handlebars = require('handlebars');
let shop_filters = shop_filters || {};

(function($){

    shop_filters.app = (function(app){

        app.$form = $('#bloom_woo_filters');
        app.api_url = filters_app.root + 'bloomscape/v1/filters';
        app.nonce = filters_app.nonce;
        app.data_attrs = [ 'plant_size', 'plant_difficulty', 'plant_light_level', 'plant_pet_friendly', 'plant_air_cleaner' ];

        $(document).ready(function(){
            if ( app.$form.length > 0 ) {
                app.init();
            }
        });

        app.init = () => {

            // Handlebars
            app.results_template = $('#filter-results-template').html();
            app.results_output = Handlebars.compile(app.results_template);

            app.$form.on('submit', app.form_submit);
            app.$form.find('input[type="checkbox"]').on('change', app.form_change);

            if ( true === app.$form.data('init') ) {
                app.$form.trigger( 'submit' );
            }
        };

        app.form_change = () => {
            clearTimeout( app.timer );
            app.timer = setTimeout(function(){
                app.$form.trigger('submit');
            }, 1000)
        };

        app.form_submit = (e) => {
            if ( e ) {
                e.preventDefault();
            }
            const form_data = app.$form.serializeArray();
            let data = {};

            $.each( form_data, ( key, value ) => {
                if ( data[value.name] ) {
                    data[value.name].push( value.value );
                } else {
                    data[value.name] = [value.value];
                }
            });

            app.change_url( data );

            $.ajax({
                method: 'POST',
                data: data,
                url: app.api_url,
                success: app.api_callback
            });
        };

        app.api_callback = ( res ) => {
            if ( res ) {
                $('#preload-shop-items').remove();
                $('#filter-results').html( app.results_output( res ) );
            }
        };

        app.change_url = ( data ) => {
            if ( ! window.history.replaceState ) { return false; }
            let url = '';
            $.each( app.data_attrs, ( key, value ) => {
                if ( data[value] ) {
                    url += '/' + data[value].join( '-' );
                } else {
                    url += '/any';
                }
            });
            window.history.replaceState( {} , 'filter', '/shop' + url );
        };


        return app;
    }(shop_filters.app || {}))


}(jQuery));