'use strict';

// Load dependencies.
const gulp = require('gulp');
const sass = require('gulp-sass');
const gutil = require('gulp-util');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
// Used to minify the CSS
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const sassLint = require('gulp-sass-lint');
// Execute gulp commands in sequence, one after the the other.
const runSequence = require('run-sequence');
const eslint = require('gulp-eslint');
const path = require('path');

gulp.task('build', ['styles:build', 'js:build']);
gulp.task('build:dev', ['styles:dev', 'js:dev']);
// List of tasks see each function for more details about each.
gulp.task('styles', styles);
gulp.task('styles:dev', styles);
gulp.task('styles:build', build);
gulp.task('styles:prefix', stylesPrefix);
gulp.task('styles:minify', stylesMinify);
gulp.task('styles:watch', stylesWatch);
// Linter tasks
gulp.task('lint', ['styles:lint']);
gulp.task('styles:lint', stylesLint);
// JS Tasks
gulp.task('js', js);
gulp.task('js:dev', js);
gulp.task('js:build', jsBuild);
gulp.task('js:watch', jsWatch);
gulp.task('js:lint', jsLint);
gulp.task('watch', ['js:watch', 'styles:watch']);

// General Configurations
// Directories where JS and SCSS is placed.
const appDirectories = ['assets/sass'];
// CSS configuration values.
const sassEntryFile = './assets/sass/style.scss';
const cssDestination = '';
const cssGeneratedFile = './style.css';
// Patterns from where all the sass files are located.
const sassFiles = [
    sassEntryFile,
    './{' + appDirectories.join(',') + '}/**/*.scss',
];
// Browser where prefixers required to be added.
const supportedBrowsers = ['Explorer >= 11', 'iOS >= 7', 'Safari >= 9'];
// Where the sass files are located, used to watch changes on these directories.
const loaders = [{
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: {
        presets: ['es2015']
    },
}];

/**
 * Function that creates a build file:
 *
 * 1. Compiles the sass file to CSS
 * 2. Minify the generated CSS File
 * 3. Adds prefixes to the final file.
 */
function build(callback) {
    runSequence('styles', 'styles:minify', callback);
}

/**
 * Compiles the sassEntryFile into CSS, also it adds the source maps of the
 * styles in order to locate easily where the styles where defined.
 */
function styles() {

    log.success('Style compilation of sass into css file started ');
    return gulp.src(sassEntryFile)
        .pipe(sass()).on('error', sass.logError)
        .pipe(gulp.dest(cssDestination));
}

/**
 * Function used to minify the cssGeneratedFile it also adds the prefixes
 * to the minified file based on the supportedBrowsers array.
 */
function stylesMinify() {
    const minifyOptions = {
        level: {
            1: {
                cleanupCharsets: true, // controls `@charset` moving to the front of a stylesheet; defaults to `true`
                normalizeUrls: true, // controls URL normalization; defaults to `true`
                optimizeBackground: true, // controls `background` property optimizations; defaults to `true`
                optimizeBorderRadius: true, // controls `border-radius` property optimizations; defaults to `true`
                optimizeFilter: true, // controls `filter` property optimizations; defaults to `true`
                optimizeFont: true, // controls `font` property optimizations; defaults to `true`
                optimizeFontWeight: true, // controls `font-weight` property optimizations; defaults to `true`
                optimizeOutline: true, // controls `outline` property optimizations; defaults to `true`
                removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
                removeNegativePaddings: true, // controls removing negative paddings; defaults to `true`
                removeQuotes: true, // controls removing quotes when unnecessary; defaults to `true`
                removeWhitespace: true, // controls removing unused whitespace; defaults to `true`
                replaceMultipleZeros: true, // contols removing redundant zeros; defaults to `true`
                replaceTimeUnits: true, // controls replacing time units with shorter values; defaults to `true`
                replaceZeroUnits: true, // controls replacing zero values with units; defaults to `true`
                roundingPrecision: false, // rounds pixel values to `N` decimal places; `false` disables rounding; defaults to `false`
                selectorsSortingMethod: 'standard', // denotes selector sorting method; can be `'natural'` or `'standard'`, `'none'`, or false (the last two since 4.1.0); defaults to `'standard'`
                specialComments: 'all', // denotes a number of /*! ... */ comments preserved; defaults to `all`
                tidyAtRules: true, // controls at-rules (e.g. `@charset`, `@import`) optimizing; defaults to `true`
                tidyBlockScopes: true, // controls block scopes (e.g. `@media`) optimizing; defaults to `true`
                tidySelectors: true, // controls selectors optimizing; defaults to `true`,
            },
            2: {
                mergeAdjacentRules: true, // controls adjacent rules merging; defaults to true
                mergeIntoShorthands: true, // controls merging properties into shorthands; defaults to true
                mergeMedia: true, // controls `@media` merging; defaults to true
                mergeNonAdjacentRules: true, // controls non-adjacent rule merging; defaults to true
                mergeSemantically: false, // controls semantic merging; defaults to false
                overrideProperties: true, // controls property overriding based on understandability; defaults to true
                removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
                reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                removeUnusedAtRules: false, // controls unused at rule removing; defaults to false (available since 4.1.0)
                restructureRules: false, // controls rule restructuring; defaults to false
                skipProperties: [] // controls which properties won't be optimized, defaults to `[]` which means all will be optimized (since 4.1.0)
            }
        }
    };
    log.success('CSS minification files started');
    return gulp.src(cssGeneratedFile)
        .pipe(cleanCSS(minifyOptions))
        .pipe(gulp.dest(cssDestination));
}

/**
 * Function used to add prefixes to the generated CSS file based on the
 * supportedBrowsers array.
 */
function stylesPrefix() {
    var prefixOptions = {
        browsers: supportedBrowsers,
        cascade: false,
    };
    log.success('Add prefixes to the generated CSS file, started');
    return gulp.src(cssGeneratedFile)
        .pipe(autoprefixer(prefixOptions))
        .pipe(gulp.dest(cssDestination));
}

/**
 * Watchs any change on sassDirectories path and on the sassEntryFile. If any
 * change is detected the `styles` task is triggered.
 */
function stylesWatch() {
    log.success('Start to watch for .scss changes');
    gulp.watch(sassFiles, ['styles']);
}

/**
 * Function that is used to lint on all the sass files and follow all the rules
 * specified on .sass-lint.json
 */
function stylesLint() {
    return gulp.src(sassFiles)
        .pipe(sassLint({
            configFile: './.sass-lint.yml'
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError());
}

/**
 * Function used to lint on the JS files, using the rules specified on
 * .eslintrc.json.
 */
function jsLint() {
    const jsFiles = [
        './assets/js/bloomscape.js',
        './{' + appDirectories.join(',') + '}/**/*.js',
    ];
    return gulp.src(jsFiles)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
}

const webpackConfig = require('./webpack.config.js');
const jsEntryFile = './assets/js/bloomscape.js';

/**
 * Function used to create the JS to be used on specifc pages, by default
 * creates a source map associated with each JS file to make easier to debug.
 */
function js() {
    let options = Object.assign(webpackConfig, {devtool: 'source-map'});
    return jsTask(options);
}

/**
 * Watches the changes of the JS and creates a single file with source maps,
 * the genreated JS is on dev mode.
 */
function jsWatch() {
    let options = Object.assign(webpackConfig, {
        devtool: 'source-map',
        watch: true,
        module: {
            loaders,
        }
    });
    return jsTask(options);
}

/**
 * Function that creates a Build file, in this case it creates a minifed version
 * of the generated JS useful for production purposes.
 */
function jsBuild() {
    let plugins = [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ];
    let options = Object.assign(webpackConfig, {
        plugins,
        module: {
            loaders,
        }
    });
    return jsTask(options);
}

/**
 * Function used to run webpack with a set of custom options based on the task
 * being executed.
 *
 * @param object options The options to be used to run webpack, by default uses webpack.config.js.
 * @return stream Returns a gulp task to be used.
 */
function jsTask(options) {
    return gulp.src(jsEntryFile)
        .pipe(webpackStream(options))
        .pipe(gulp.dest('./build/js'));
}

/*
 * Function used to log data to the console via gutil function
 */
const log = {
    success(msg) {
        gutil.log(gutil.colors.green(msg));
    }
}
