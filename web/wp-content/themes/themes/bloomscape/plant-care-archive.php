<?php
	/**
	 * Template Name: Plant care archive
	 */
	get_header();
	the_post();
?>

	<div class="content-area">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php get_template_part( '/partials/plant-care-image' ); ?>

				<div class="entry-header">
					<?php
						the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					?>
				</div>

				<div class="entry-content">
					<?php echo wp_kses_post( get_the_content() ); ?>
				</div>

				<div class="inner-wrapper">
					<div class="inner-content">
						<?php get_template_part( '/partials/faq-module' ); ?>
					</div>
				</div>

			</div>
		</main>
	</div>

<?php
	get_footer();
