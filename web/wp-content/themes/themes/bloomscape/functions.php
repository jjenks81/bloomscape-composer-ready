<?php

namespace Bloomscape;

include 'vendor/autoload.php';
include 'inc/acf-fields.php';

$bloomscape        = Customizer::init();

$product_page      = WooProductPage::init();
BloomscapePDPCustomizer::init();

$product_grid_page = BloomscapeProductGridCustomizer::init();
$bloom_api          = BloomscapeAPI::init();
$bloom_user         = NewUser::init();
$bloom_blog         = PlantCare::init();


/**
 * Sanitize
 *
 * @param mixed $array Array to sanitize.
 *
 * @return mixed
 */
function sanitize_array( $array ) {
	return array_map(
		function( $item ) {
				return sanitize_text_field( $item );
		},$array
	);
}

/**
 * Woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'Bloomscape\hide_shipping_when_free_is_available', 10,2 );

/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package.
 * @param array $package The package array/object being shipped.
 * @return array of modified rates.
 */
function hide_shipping_when_free_is_available( $rates, $package ) {

	$free = [];
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id || 'flat_rate' !== $rate->method_id ) {
			$free[ $rate_id ] = $rate;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
