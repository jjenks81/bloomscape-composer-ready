<?php
	/**
	 * Template Name: Legal page
	 */
	get_header();
	the_post();
?>

	<div class="inner-wrapper legal">
		<?php
			the_content();
		?>
	</div>

<?php
	get_footer();
