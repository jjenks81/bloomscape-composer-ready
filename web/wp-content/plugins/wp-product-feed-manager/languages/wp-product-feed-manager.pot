#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: WooCommerce Product Feed Manager\n"
"POT-Creation-Date: 2018-10-13 10:07+0200\n"
"PO-Revision-Date: 2018-08-20 20:39+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: wp-product-feed-manager.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: includes/application/class-feed-master.php:99
msgid "1428 - Failed to load the feed data"
msgstr ""

#. translators: %s: Folder where the feeds are stored
#: includes/application/class-feed-master.php:176
#, php-format
msgid ""
"1430 - %s is not a writable folder. Make sure you have admin rights to this "
"folder."
msgstr ""

#: includes/application/class-feed-master.php:442
#: includes/user-interface/class-i18n-scripts.php:71
msgid "Fill with a static value"
msgstr ""

#: includes/application/class-feed-master.php:443
#: includes/application/class-feed-master.php:448
msgid "Use the settings in the Merchant Center"
msgstr ""

#. translators: %s: Products id
#: includes/application/class-feed-processor.php:220
#, php-format
msgid ""
"Could not identify the correct categorymap for the product with ID: %s! \n"
"\t\t\t\t\tPlease check the category settings of this product."
msgstr ""

#: includes/application/wppfm-feed-processing-support.php:59
#, php-format
msgid ""
"2131 - Please try to refresh the page and open a support ticket at %s if the "
"issue persists."
msgstr ""

#: includes/data/class-ajax-calls.php:27
msgid "You are not allowed to do this!"
msgstr ""

#: includes/data/class-ajax-data.php:152
msgid ""
"Could not add custom fields because I could not identify the channel. \n"
"\t\t\t\t\t\t\t\t\tIf not already done add the correct channel in the Manage "
"Channels page. \n"
"\t\t\t\t\t\t\t\t\tAlso try to deactivate and then activate the plugin."
msgstr ""

#: includes/data/class-ajax-data.php:158
msgid ""
"Could not identify the channel. \n"
"\t\t\t\t\t\t\t\tTry to deactivate and then activate the plugin. \n"
"\t\t\t\t\t\t\t\tIf that does not work remove the plugin through the "
"Wordpress Plugins page and than reinstall and activate it again."
msgstr ""

#. translators: %s: Title of the feed file
#. translators: %s: Selected backup file
#: includes/data/class-ajax-file.php:109
#: includes/data/class-db-management.php:215
#, php-format
msgid "Could not find file %s."
msgstr ""

#: includes/data/class-ajax-file.php:111
#: includes/data/class-db-management.php:217
msgid ""
"Error deleting the feed. You do not have the correct authorities to delete "
"the file."
msgstr ""

#: includes/data/class-ajax-file.php:164
#: includes/user-interface/class-i18n-scripts.php:31
msgid ""
"Error writing the feed. You do not have the correct authorities to write the "
"file."
msgstr ""

#: includes/data/class-ajax-file.php:285
msgid "Feed processing data cleared"
msgstr ""

#. translators: clearing the feed data failed
#: includes/data/class-ajax-file.php:288
msgid "Clearing failed!"
msgstr ""

#. translators: %s: url to the wp marketingrobot server
#: includes/data/class-channels.php:133
#, php-format
msgid ""
"2832 - Failed to connect with the wpmarketingrobot server. Please wait a few "
"minutes and try again. If the issue persists, open a support ticket at %s."
msgstr ""

#. translators: %s: url to the support page
#: includes/data/class-channels.php:165
#, php-format
msgid "2141 - Please open a support ticket at %s for support on this issue."
msgstr ""

#. translators: %s: Name of a channel
#: includes/data/class-channels.php:209
#, php-format
msgid ""
"Channel %s is not installed correctly. Please try to Deactivate and then "
"Activate the Feed Manager Plugin in your Plugins page."
msgstr ""

#: includes/data/class-db-management.php:84
msgid ""
"A backup file with the selected name already exists. Please choose an other "
"name or delete the existing file first."
msgstr ""

#: includes/data/class-db-management.php:143
msgid ""
"The backup file is of an older version of the database and can not be "
"restored as it is not compatible with the current database."
msgstr ""

#: includes/data/class-db-management.php:198
msgid "A backup file with the selected name does not exists."
msgstr ""

#: includes/data/class-file.php:46 includes/data/class-file.php:89
msgid "Unable to open the file containing the categories"
msgstr ""

#. translators: %s: Folder that holds the backup files
#: includes/data/class-file.php:141
#, php-format
msgid ""
"1432 - %s is not a writable folder. Make sure you have admin rights to this "
"folder."
msgstr ""

#. translators: %s: Selected backup file
#: includes/data/class-file.php:151
#, php-format
msgid "1433 - Could not write the %s file."
msgstr ""

#. translators: %s: Selected channel name
#: includes/data/class-file.php:191
#, php-format
msgid ""
"The installation of channel %s failed. Unable to unpack the channel file in "
"folder %s. Please make sure you have read/write permission for this folder."
msgstr ""

#. translators: %s: Folder that contains the channel data
#: includes/data/class-ftp.php:36
#, php-format
msgid ""
"You have no read/write permission to the %s folder. \n"
"\t\t\t\t\tPlease update the file permissions of this folder to make it "
"writable and then try installing a channel again."
msgstr ""

#: includes/libraries/wpmr_plugin_updater.php:188
#, php-format
msgid ""
"There is a new version of %1$s available. %2$sView version %3$s details%4$s."
msgstr ""

#: includes/libraries/wpmr_plugin_updater.php:196
#, php-format
msgid ""
"There is a new version of %1$s available. %2$sView version %3$s details%4$s "
"or %5$supdate now%6$s."
msgstr ""

#: includes/libraries/wpmr_plugin_updater.php:342
msgid "You do not have permission to install plugin updates"
msgstr ""

#: includes/libraries/wpmr_plugin_updater.php:342
msgid "Error"
msgstr ""

#. translators: %s: link to the support page
#: includes/libraries/wppfm-async-request.php:124
#, php-format
msgid ""
"2166 - Failed to make a wp_remote_post connection. Please open the Settings "
"page, click the Clear feed process button and try again. If the issue "
"persists, open a support ticket at %s."
msgstr ""

#: includes/libraries/wppfm-background-process.php:542
#, php-format
msgid "Every %d minute"
msgid_plural "Every %d minutes"
msgstr[0] ""
msgstr[1] ""

#. translators: %s: old channel folder
#: includes/setup/class-folders.php:51
#, php-format
msgid ""
"Unable to remove the %s folder. This folder is not required any more. Please "
"try removing this folder manually using ftp software or an equivalent "
"methode."
msgstr ""

#: includes/user-interface/class-add-feed-page.php:69
msgid "Open Feed List"
msgstr ""

#: includes/user-interface/class-add-options-page.php:54
msgid "Feed Manager Settings"
msgstr ""

#: includes/user-interface/class-admin-page.php:39
msgid "Manage your feeds with ease"
msgstr ""

#: includes/user-interface/class-admin-page.php:41
msgid "Click here for the documentation"
msgstr ""

#: includes/user-interface/class-admin-page.php:42
msgid "Something not working? Click here for support"
msgstr ""

#: includes/user-interface/class-admin-page.php:48
msgid "Like Our Plugin? We'd Love Your Feedback"
msgstr ""

#: includes/user-interface/class-admin-page.php:59
msgid "About Us"
msgstr ""

#: includes/user-interface/class-admin-page.php:60
msgid "Contact Us"
msgstr ""

#: includes/user-interface/class-admin-page.php:61
msgid "Terms and Conditions"
msgstr ""

#: includes/user-interface/class-admin-page.php:62
msgid "Documentation"
msgstr ""

#: includes/user-interface/class-admin-page.php:80
msgid ""
"Your key seems to be expired. If you are sure you have a valid key, please "
"open a ticket at wpmarketingrobot.com/support/"
msgstr ""

#: includes/user-interface/class-admin-page.php:82
msgid "Plugin License Options"
msgstr ""

#: includes/user-interface/class-admin-page.php:88
msgid "License Key"
msgstr ""

#: includes/user-interface/class-admin-page.php:96
msgid "I accept your Terms and Conditions"
msgstr ""

#: includes/user-interface/class-admin-page.php:100
#: includes/user-interface/class-admin-page.php:104
msgid "Activate License"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:45
msgid "Select your product source"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:69
msgid "-- Select your merchant --"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:75
msgid ""
"You first need to install a channel before you can add a feed. Open the "
"Manage Channels page and install at least one channel."
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:87
msgid "-- Select your target country --"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:98
msgid "Every"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:99
msgid "day(s) at"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:100
msgid "Every day at"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:104
msgid "for"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:106
msgid "time(s) a day"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:116
#: includes/user-interface/class-i18n-scripts.php:69
msgid "Category Mapping"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:125
msgid "Shop Category"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:126
msgid "Feed Category"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:127
#: includes/user-interface/class-main-admin-page.php:80
msgid "Products"
msgstr ""

#: includes/user-interface/class-feed-form-controls.php:180
msgid "No shop categories found."
msgstr ""

#: includes/user-interface/class-feed-form.php:21
msgid "Basic Feed Options"
msgstr ""

#: includes/user-interface/class-feed-form.php:35
#: includes/user-interface/class-main-admin-page.php:94
msgid "Feed List"
msgstr ""

#: includes/user-interface/class-feed-form.php:36
#: includes/user-interface/class-main-admin-page.php:95
msgid "Add or Edit Feed"
msgstr ""

#: includes/user-interface/class-feed-form.php:45
msgid "File Name"
msgstr ""

#: includes/user-interface/class-feed-form.php:47
msgid "Products source"
msgstr ""

#: includes/user-interface/class-feed-form.php:49
msgid "Channel"
msgstr ""

#: includes/user-interface/class-feed-form.php:52
msgid "Target Country"
msgstr ""

#: includes/user-interface/class-feed-form.php:54
msgid "Default Category"
msgstr ""

#: includes/user-interface/class-feed-form.php:56
msgid "Aggregator Shop"
msgstr ""

#: includes/user-interface/class-feed-form.php:58
msgid "Include Product Variations"
msgstr ""

#: includes/user-interface/class-feed-form.php:60
msgid "Feed Title"
msgstr ""

#: includes/user-interface/class-feed-form.php:62
msgid "Feed Description"
msgstr ""

#: includes/user-interface/class-feed-form.php:64
msgid "Update Schedule"
msgstr ""

#: includes/user-interface/class-feed-form.php:72
#: includes/user-interface/class-feed-form.php:97
msgid "Save & Generate Feed"
msgstr ""

#: includes/user-interface/class-feed-form.php:73
#: includes/user-interface/class-feed-form.php:98
msgid "Save Feed"
msgstr ""

#: includes/user-interface/class-feed-form.php:78
msgid "Attribute Mapping"
msgstr ""

#: includes/user-interface/class-feed-form.php:79
msgid "Required"
msgstr ""

#: includes/user-interface/class-feed-form.php:82
msgid "Highly recommended"
msgstr ""

#: includes/user-interface/class-feed-form.php:85
msgid "Recommended"
msgstr ""

#: includes/user-interface/class-feed-form.php:88
msgid "Optional"
msgstr ""

#: includes/user-interface/class-feed-form.php:91
msgid "Custom attributes"
msgstr ""

#: includes/user-interface/class-feed-form.php:108
msgid "Add to feed"
msgstr ""

#: includes/user-interface/class-feed-form.php:109
msgid "From WooCommerce source"
msgstr ""

#: includes/user-interface/class-feed-form.php:110
msgid "Condition"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:25
msgid "You are using characters in your file name that are not allowed!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:26
msgid "Saving the data to the database has failed! Please try again."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:27
msgid "no category required"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:28
msgid "no feed generated"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:29
msgid "Started processing your feed in the background."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:30
msgid ""
"Pushed the feed into the background queue. Processing starts after all other "
"feeds are processed."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:32
msgid "Generating the feed has failed! Error return code = %xmlResult%"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:33
msgid "The status of feed %feedname% is unknown."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:34
msgid "Product feed %feedname% is now ready. It contains %prodnr% products."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:35
msgid ""
"Still processing the feed in the background. You can wait for it to finish, "
"but you can also close this form if you want."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:36
msgid ""
"This feed has been added to the feed queue and will be processed when it is "
"next."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:37
msgid "Product feed %feedname% has some errors!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:38
msgid "Product feed %feedname% has failed!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:39
msgid ""
"The option to add product variations to the feed is not available in the "
"free version. Unlock this option by upgrading to the Premium plugin. For "
"more information goto https://www.wpmarketingrobot.com/."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:40
msgid "Select a sub-category"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:41
#, php-format
msgid "You already have a field %fieldname% defined!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:42
msgid "Make sure to select all source fields before adding a new one!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:43
msgid "Please fill in the current condition before adding a new one!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:44
msgid "Please select a source field first before you select the conditions."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:45
msgid "Please select a valid source before adding a condition to that source."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:46
msgid ""
"The Advanced Filter option is not available in the free version. Unlock the "
"Advanced Filter option by upgrading to the Premium plugin. For more "
"information goto http://www.wpmarketingrobot.com/."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:47
msgid "except the ones where"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:48
msgid "Please fill in the filter values before adding a new one"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:49
msgid "No separator"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:50
msgid "space"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:51
msgid "comma"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:52
msgid "point"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:53
msgid "semicolon"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:54
msgid "colon"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:55
msgid "dash"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:56
msgid "slash"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:57
msgid "backslash"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:58
msgid "double pipe"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:59
msgid "other"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:60
msgid "for all %other% products"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:61
msgid "edit values"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:62
msgid "and change values"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:63
msgid "remove value editor"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:64
msgid "to"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:65
msgid "with element name"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:66
msgid "Defined by the Category Mapping Table."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:67
msgid "Use advised source"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:68
msgid "Combine source fields"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:70
msgid "Select a source field"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:72
msgid "Map to Default Category"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:73
msgid "Use Shop Category"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:74
msgid "an empty field"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:75
msgid "Add recommended output"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:76
msgid "Add optional output"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:77
msgid ""
"You've not selected a Shop Category in the Category Mapping Table. With no "
"Shop Category selected, your feed will be empty. Are you sure you still want "
"to save this feed?"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:78
msgid "A file name is required!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:79
msgid ""
"Add at least one query in the previous change value row before adding a new "
"row."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:80
msgid "Please first fill in a change value option before adding a query to it."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:97
msgid "Processing the feed, please wait..."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:98
msgid "Processing failed, please try again"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:99
msgid "In processing queue"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:100
msgid "No data found"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:101
msgid "Auto-off"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:102
msgid "Auto-on"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:103
msgid "Edit"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:104
msgid "View"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:105
msgid "Other"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:106
msgid "Unknown"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:107
msgid "Ready (manual)"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:108
msgid "Processing"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:109
msgid "Has errors"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:110
msgid "Failed processing"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:111
#: includes/user-interface/class-i18n-scripts.php:194
msgid "Ready (auto)"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:112
msgid "Added a copy of feed %feedname% to the list."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:113
msgid "Please confirm you want to delete feed %feedname%."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:114
msgid "Feed %feedname% removed from the server."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:131
msgid "First enter a file name for the backup file."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:132
msgid "Please confirm you want to delete backup %backup_file_name%."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:133
#, php-format
msgid "%backup_file_name% deleted."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:134
msgid ""
"Are you sure you want to restore backup %backup_file_name%? This will "
"overwrite your current settings and feed data!"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:135
msgid "%backup_file_name% restored"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:136
#, php-format
msgid "%backup_file_name% duplicated"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:148
msgid "Restore"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:149
msgid "No backup found"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:166
msgid ""
"Please confirm you want to remove this channel! Removing this channel will "
"also remove all its feed files."
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:182
msgid "edit"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:183
msgid "select"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:184
msgid "selected"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:185
msgid "delete"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:186
msgid "remove"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:187
msgid "add"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:188
msgid "if"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:189
msgid "or"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:190
msgid "and"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:191
msgid ""
"All products from the selected Shop Categories will be included in the feed"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:192
msgid "Duplicate"
msgstr ""

#: includes/user-interface/class-i18n-scripts.php:193
msgid "Delete"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:77
msgid "Name"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:78
msgid "Url"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:79
msgid "Updated"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:81
msgid "Status"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:82
#: includes/user-interface/class-options-form.php:120
msgid "Actions"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:116
msgid ""
"This plugin only works in conjunction with the WooCommerce Plugin! \n"
"\t\t\t\tIt seems you have not installed the WooCommerce Plugin yet, so "
"please do so before using this Plugin."
msgstr ""

#: includes/user-interface/class-main-admin-page.php:127
#, php-format
msgid ""
"This plugin requires WooCommerce version 3.0.0 as a minimum! \n"
"\t\t\t\tIt seems you have installed WooCommerce version %s which is a "
"version that is not supported. \n"
"\t\t\t\tPlease update to at least version 3.0.0. ***"
msgstr ""

#: includes/user-interface/class-main-admin-page.php:147
msgid "Add New Feed"
msgstr ""

#. translators: %s: link to the support page
#: includes/user-interface/class-manage-channels-page.php:42
#, php-format
msgid ""
"2127 - Try refreshing the page and Please open a support ticket at %s if the "
"issue persists."
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:79
msgid "Installed Channels:"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:80
msgid "The following channels are ready to use"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:90
msgid "Uninstalled Channels:"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:91
msgid "The following channels are ready to be installed"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:104
msgid ""
"No channels available. Please check if your license is current and active on "
"the Feed Manager tab"
msgstr ""

#. translators: %s: link to the support page
#: includes/user-interface/class-manage-channels-page.php:108
#, php-format
msgid ""
"2965 - Please try to refreh the page and Please open a support ticket at %s "
"if the issue persists."
msgstr ""

#. translators: %s: Version number of installed channel
#: includes/user-interface/class-manage-channels-page.php:132
#: includes/user-interface/class-manage-channels-page.php:135
#, php-format
msgid "Version %s"
msgstr ""

#. translators: Channel version unknown
#: includes/user-interface/class-manage-channels-page.php:139
msgid "Version unknown"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:143
#, php-format
msgid "Version %1$s. New version %2$s available!"
msgstr ""

#. translators: %s: channel name
#: includes/user-interface/class-manage-channels-page.php:161
#, php-format
msgid "Remove the %s channel."
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:162
msgid "Remove"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:168
msgid "Update this channel to the latest version"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:169
msgid "Update"
msgstr ""

#. translators: %s: channel name
#: includes/user-interface/class-manage-channels-page.php:177
#, php-format
msgid "Install the %s channel."
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:178
#: includes/user-interface/class-manage-channels-page.php:182
msgid "Install"
msgstr ""

#: includes/user-interface/class-manage-channels-page.php:181
msgid ""
"The maximum number of channels for your license has been reached. Please "
"remove an unused channel or upgrade your license."
msgstr ""

#: includes/user-interface/class-options-form.php:57
msgid "Auto Feed Fix"
msgstr ""

#: includes/user-interface/class-options-form.php:62
msgid ""
"Automatically fix feeds that are failed (default on). Change this setting if "
"a feed keeps failing."
msgstr ""

#: includes/user-interface/class-options-form.php:66
msgid "Disable background processing"
msgstr ""

#: includes/user-interface/class-options-form.php:71
msgid ""
"Process feeds directly instead of in the background (default off). Try this "
"option when feeds keep getting stuck in processing."
msgstr ""

#: includes/user-interface/class-options-form.php:75
msgid "Batch process time limit"
msgstr ""

#: includes/user-interface/class-options-form.php:80
msgid "seconds."
msgstr ""

#: includes/user-interface/class-options-form.php:81
msgid ""
"Try to increase this time limit if your feeds keep failing in processing "
"(default 90 seconds)."
msgstr ""

#: includes/user-interface/class-options-form.php:85
msgid "Third Party Attributes"
msgstr ""

#: includes/user-interface/class-options-form.php:90
msgid ""
"Enter comma separated keywords and wildcards to use third party attributes."
msgstr ""

#: includes/user-interface/class-options-form.php:94
#: includes/user-interface/class-options-form.php:97
msgid "Clear feed process"
msgstr ""

#: includes/user-interface/class-options-form.php:99
msgid ""
"Use this option when feeds get stuck processing - does not delete your "
"current feeds or settings."
msgstr ""

#: includes/user-interface/class-options-form.php:103
msgid "Re-initialize"
msgstr ""

#: includes/user-interface/class-options-form.php:106
msgid "Re-initiate plugin"
msgstr ""

#: includes/user-interface/class-options-form.php:108
msgid ""
"Updates the tables if required, re-initiates the cron events and resets the "
"stored license - does not delete your current feeds or settings."
msgstr ""

#: includes/user-interface/class-options-form.php:112
msgid "Backups"
msgstr ""

#: includes/user-interface/class-options-form.php:118
msgid "File name"
msgstr ""

#: includes/user-interface/class-options-form.php:119
msgid "Backup date"
msgstr ""

#: includes/user-interface/class-options-form.php:125
msgid "Add new backup"
msgstr ""

#: includes/user-interface/class-options-form.php:130
msgid "Backup current feeds"
msgstr ""

#: includes/user-interface/class-options-form.php:131
msgid "Cancel backup"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:16
msgid "WP Feed Manager"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:17
msgid "Feed Manager"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:25
#: includes/user-interface/wppfm-admin-menu.php:26
msgid "Add Feed"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:39
#, php-format
msgid "%d channel update"
msgid_plural "%d channel updates"
msgstr[0] ""
msgstr[1] ""

#. translators: %d: Number of channel updates available
#: includes/user-interface/wppfm-admin-menu.php:46
#, php-format
msgid "Manage Channels %s"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:53
msgid "Manage Channels"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:64
#: includes/user-interface/wppfm-admin-menu.php:65
msgid "Settings"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:86
msgid "Starter Guide"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:89
msgid "Go Premium"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:91
msgid "Get Support"
msgstr ""

#: includes/user-interface/wppfm-admin-menu.php:176
msgid ""
"Due to the latest update your Feed Manager backups are no longer valid! "
"Please open the Feed Manager Settings page, remove all your backups in and "
"make a new one."
msgstr ""

#. translators: %s: link to the support page
#: includes/user-interface/wppfm-admin-menu.php:260
#, php-format
msgid ""
"2121 - Activating your license failed. Please open a support ticket at %s "
"for support on this issue."
msgstr ""

#. translators: %s: Error message from wp_remote_posts response
#: includes/user-interface/wppfm-admin-menu.php:275
#, php-format
msgid ""
"An error has occured. It is possible that the wpmarketingrobot website is "
"down. \n"
"\t\t\t\tPlease check if wpmarketingrobot.com is still active. If not, or if "
"this error consist, please contact\n"
"\t\t\t\tauke@wpmarketingrobot.com. Error Message: %s"
msgstr ""

#. translators: 1: Name of the plugin, 2: url to the wpmarketingrobot store, 3: license key
#: includes/user-interface/wppfm-admin-menu.php:312
#, php-format
msgid ""
"You have an invalid or expired license key for your %1$s plugin. \n"
"\t\t\tPlease <a href=\"%2$scheckout/?edd_license_key=%3$s&utm_source=client-"
"admin&utm_medium=renewal_link&utm_campaign=license_expired\">\n"
"\t\t\tclick here to go to the Licenses renewal page</a> to keep the plugin "
"working and your product feeds valid."
msgstr ""

#. translators: 1: Name of the plugin, 2: url to the wpmarketingrobot store, 3: license key
#: includes/user-interface/wppfm-admin-menu.php:324
#, php-format
msgid ""
"The license key for your %1$s plugin is expiring soon. \n"
"\t\t\tIf you wish you can <a href=\"%2$scheckout/?edd_license_key="
"%3$s&utm_source=client-"
"admin&utm_medium=renewal_link&utm_campaign=license_renewal\">\n"
"\t\t\tclick here</a> to renew your license key directly so you can be sure "
"your product feeds keep working."
msgstr ""

#. translators: %s: link to the support page
#: includes/user-interface/wppfm-admin-menu.php:362
#, php-format
msgid ""
"2122 - Checking your license failed. Please open a support ticket at %s for "
"support on this issue."
msgstr ""

#. translators: %s: Error message
#: includes/user-interface/wppfm-messaging.php:114
#, php-format
msgid ""
"There was an error but I was unable to store the error message in the log "
"file. The message was %s"
msgstr ""

#: wp-product-feed-manager.php:66
msgid "Cloning is not allowed"
msgstr ""

#: wp-product-feed-manager.php:74
msgid "Unserializing instances of this class is not allowed"
msgstr ""

#. translators: %s: Plugin name
#: wp-product-feed-manager.php:229
#, php-format
msgid "You have insufficient rights to use %s"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WooCommerce Product Feed Manager"
msgstr ""

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "https://www.wpmarketingrobot.com"
msgstr ""

#. Description of the plugin/theme
msgid ""
"An easy to use WordPress plugin that generates and submits your product "
"feeds to merchant centres."
msgstr ""

#. Author of the plugin/theme
msgid "Michel Jongbloed"
msgstr ""
