<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

$empty_cart_txt 	= isset( $options['sc-empty-text']) ? $options['sc-empty-text'] : __('Your cart is empty.','side-cart-woocommerce');

?>

<div class="xoo-wsc-content">
	<?php if(WC()->cart->is_empty()): ?>
		<span class="xoo-wsc-ecnt"><?php esc_attr_e($empty_cart_txt,'side-cart-woocommerce'); ?></span>
	<?php else: ?>

		<?php
		$show_price 	 = isset( $options['sc-show-price']) ? $options['sc-show-price'] : 1;
		$show_ptotal 	 = isset( $options['sc-show-ptotal']) ? $options['sc-show-ptotal'] : 1;
		$update_quantity = isset( $options['sc-update-qty']) ? $options['sc-update-qty'] : 1;

		do_action( 'woocommerce_before_calculate_totals', WC()->cart );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );


				
				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

				

				
				$product_name =  apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
				
										

				$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );

				$product_subtotal = apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );

				$attributes = '';

				//Variation
				$attributes .= $_product->is_type('variable') || $_product->is_type('variation')  ? wc_get_formatted_variation($_product) : '';
				// Meta data
				$attributes .=  WC()->cart->get_item_data( $cart_item );

		?>

				<div class="xoo-wsc-product" data-xoo_wsc="<?php echo $cart_item_key; ?>">
					<div class="xoo-wsc-img-col">
						<a href="<?php echo $product_permalink; ?>"><?php echo $thumbnail; ?></a>
					</div>
					<div class="xoo-wsc-sum-col">
						<a href="<?php echo $product_permalink ?>" class="xoo-wsc-pname"><?php echo $product_name; ?></a>
						<a class="xoo-wsc-remove xoo-wsc-icon-trash"></a>

						<?php echo $attributes ? $attributes : ''; ?> 

						<?php if($update_quantity == 1): ?>

							<?php if($show_price == 1): ?>
								<div class="xoo-wsc-price">
									<span><?php _e('Price:','side-cart-woocommerce') ?></span> <?php echo $product_price; ?>
								</div>

							<?php endif; ?>

							<div class="xoo-wsc-psrow">

								<?php if(!$_product->is_sold_individually()): ?>
									<div class="xoo-wsc-qtybox" style="margin-right: 10px;">
										<span class="xoo-wsc-minus  xoo-wsc-chng">-</span>
										<?php echo xoo_wsc_Cart_Data::quantity_input($cart_item['quantity'],$_product); ?>
										<span class="xoo-wsc-plus xoo-wsc-chng">+</span>
									</div>
								<?php endif; ?>

								<?php if($show_ptotal == 1): ?>
									<span class="xoo-wsc-ptotal"><?php echo $product_subtotal; ?></span>
								<?php endif; ?>
							</div>

						<?php else: ?>
							
							<div class="xoo-wsc-price">
								<span><?php echo $cart_item['quantity']; ?></span> X <span><?php echo $product_price; ?></span>

								<?php if($show_ptotal == 1): ?>
									= <span><?php echo $product_subtotal; ?></span>
								<?php endif; ?>
							</div>

						<?php endif; ?>

					</div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php endif; ?>

	<div class="xoo-wsc-updating">
		<span class="xoo-wsc-icon-spinner2" aria-hidden="true"></span>
		<span class="xoo-wsc-uopac"></span>
	</div>
</div>

