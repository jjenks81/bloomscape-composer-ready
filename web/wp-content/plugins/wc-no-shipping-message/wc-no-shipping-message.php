<?php
/**
 * Plugin Name: WooCommerce No Shipping Message
 * Description: Replaces "No shipping methods available ..." message with a provided text. Look for Settings -> Shipping -> No-shipping Message.
 * Version: 1.3.1
 * Author: dangoodman
 * Author URI: http://tablerateshipping.com
 */

call_user_func(function()
{
	define('WNSM_OPTION_MESSAGE_TYPE', 'wnsm_no_shipping_message_type');
	define('WNSM_OPTION_MESSAGE', 'wnsm_no_shipping_message');
	define('WNSM_LANG_DOMAIN', 'wc-no-shipping-methods');

    if ($message = get_option(WNSM_OPTION_MESSAGE)) {

    	$escape = null; {

		    $type = get_option(WNSM_OPTION_MESSAGE_TYPE);
		    if (!$type) {
			    $type = 'html';
			    update_option(WNSM_OPTION_MESSAGE_TYPE, $type);
		    }

		    $escape = ($type !== 'html');
	    }

        foreach (array('woocommerce_cart_no_shipping_available_html', 'woocommerce_no_shipping_available_html') as $hook) {
            add_filter($hook, function() use($message, $escape) {
                $message = __($message, WNSM_LANG_DOMAIN);
                if ($escape) {
                	$message = htmlspecialchars($message, ENT_QUOTES, 'UTF-8');
                }
                return $message;
            });
        }
    }

    if (is_admin()) {
        add_filter('woocommerce_shipping_settings', function ($settings) {
            $noShippingMessageFields = array(
            	array(
	                'id' => WNSM_OPTION_MESSAGE,
	                'type' => 'textarea',
	                'title' => __('No-shipping message', WNSM_LANG_DOMAIN),
	                'desc' =>
		                __('Message shown to the customer when no shipping methods available.', WNSM_LANG_DOMAIN) . '<br>' .
	                    __('Leave empty for the default text.', WNSM_LANG_DOMAIN) . '<br><br>',
	                'default' => '',
	            ),
	            array(
		            'id' => WNSM_OPTION_MESSAGE_TYPE,
		            'type' => 'select',
		            'title' => __('No-shipping message type', WNSM_LANG_DOMAIN),
		            'options' => array(
			            'text' => __('Plain text', WNSM_LANG_DOMAIN),
			            'html' => __('HTML', WNSM_LANG_DOMAIN),
		            ),
	            ),
            );

            $maybeSectionEnd = end($settings);
            $newFieldPosition = @$maybeSectionEnd['type'] == 'sectionend' ? -1 : count($settings);
            array_splice($settings, $newFieldPosition, 0, $noShippingMessageFields);

            return $settings;
        });

        add_filter('plugin_action_links_' . plugin_basename(wp_normalize_path(__FILE__)), function($links) {
            $settingsUrl = admin_url('admin.php?page=wc-settings&tab=shipping&section=options#' . urlencode(WNSM_OPTION_MESSAGE));
            array_unshift($links, '<a href="'.esc_html($settingsUrl).'">'.__('Edit message', WNSM_LANG_DOMAIN).'</a>');
            return $links;
        });
    }
});