=== WooCommerce Sync for QuickBooks Online - by MyWorks Software ===
Contributors: myworksdesign
Donate link: https://myworks.software
Tags: woocommerce, quickbooks, quickbooks online, quickbooks desktop, quickbooks pos, woocommerce quickbooks, woocommerce sync, woocommerce quickbooks sync, quickbooks integration
Requires at least: 4.0
Tested up to: 4.9.8
Stable tag: 1.6.1
Requires PHP: 5.6
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Automatically sync your customers, orders, inventory and more in real time between your WooCommerce store and QuickBooks! Requires a MyWorks account.

== Description ==

**Requires a MyWorks trial or paid plan to connect to QuickBooks.**

Sync your WooCommerce store with QuickBooks Online! 
[*Using QuickBooks Desktop or POS? Visit MyWorks to create an account and set up your sync.*](https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop)

Real time sync your customers, orders, products and inventory - all automatically.

The only WooCommerce plugin to automatically two-way sync your WooCommerce store with QuickBooks Online, all in real-time! Easily sync your products, orders, customers, inventory and more between your WooCommerce store and QuickBooks Online.

**Check our our 5-star reviews in the Intuit App Store!**

= Key Features =

* Unlimited Real Time Sync
* Complete 2-way Sync
* Global Tax & Currency Support
* Global QuickBooks Support
* Completely Automatic Sync
* Manually push & pull historic products, orders and more
* Complimentary setup call/screenshare - included with all trials/plans
* Unmatched security - your data passes directly to QuickBooks, not through us

= More Information =

Requires a MyWorks account to connect to QuickBooks Online. Visit https://myworks.software to read more about MyWorks Sync, including documentation, installation instructions, pricing and plans.

Pricing is $39/month (billed annually), or $49/month (billed monthly). We do offer lower-volume and non-profit discounts.

= Connection Information =

**This plugin enables you to connect your WooCommerce store to QuickBooks Online through your MyWorks account by utilizing the MyWorks Sync QuickBooks App. We (MyWorks Sync) are a service provider that allows you to connect your site to your QuickBooks Online company, and this plugin establishes this connection for you on your WooCommerce Store.**

You will be able to connect to QuickBooks Online from within your account with us, and use this connection in our MyWorks Sync plugin on your WooCommerce Store to sync your store with QuickBooks Online. MyWorks only stores your connection keys/details - and does so securely. All other data is handled and stored by Intuit. All data transmitted passes directly to QuickBooks Online from your website, and does not pass through our systems.

Our QuickBooks App:
<https://apps.intuit.com/woocommerce_quickbooks_online_automatic_sync_myworks_software>

Our Terms of Use:
<http://myworks.software/terms-of-use>

QuickBooks Online Privacy Policy
<https://security.intuit.com/index.php/privacy>

MyWorks Privacy Policy
<http://myworks.software/privacy>

== Installation ==

1. Install MyWorks Sync either via the Wordpress.org plugin repository, or by uploading the plugin files to wp-content/plugins on your site.
2. Create an account with us (if you don't already have one) at <https://myworks.software>
3. Connect to QuickBooks Online from within your account with us.
3. Visit MyWorks Sync > Connection in your WordPress Admin to finalize the connection.
4. Follow the step by step wizard or visit <https://docs.myworks.software> to view our docs.

== Frequently Asked Questions ==

= What are the requirements to use MyWorks Sync? =
MyWorks Sync requires a recent version of Wordpress (4.2+) with WooCommerce (2.2+) installed. Your server should run on Linux and have at least PHP 5.6. You also need an active trial or paid plan with MyWorks at <https://myworks.software/account>

= What QuickBooks countries/versions do you support? =
MyWorks Sync supports any version/country of QuickBooks Online, including any tax and currency for your locale. We also support QuickBooks Desktop 2015 and later. If you have QuickBooks Desktop, visit our site at <https://myworks.software> to sign up and learn more.

= Do you support QuickBooks Desktop or POS? =
We do support QuickBooks Desktop Pro, Premier and Enterprise - 2015 and later. We also support QuickBooks POS Basic, Pro & Multi-Store - V8 and later. Visit our site below to sign up.
<https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop>

= Do you have a free trial? =
We sure do. We offer a 14 day free trial for you to try our real-time automatic sync without making any commitment! You won’t even have to enter a payment method or make any billing agreements. Visit our site at <https://myworks.software> to learn more and sign up.

= How does the sync work? =
Our sync is all in real time, which means that once it's been set up, we'll automatically sync new customers, orders, products and inventory from WooCommerce to QuickBooks Online. You can choose what data you'd like to real time sync in our Sync Settings at any time. We do not automatically sync old (existing) data, but you can push it manually.

= How does inventory sync work? =
Once our sync is set up, you can choose which directions to sync inventory (each direction, or just a single direction). Once this is set, inventory will real time sync to the other system whenever it's changed on the other system. All inventory updates sync in real time. 

= So, new data is real time synced. How about existing data? =
We have push and pull sections in our sync that allow you to manually select and push existing customers, orders, products and inventory levels from WooCommerce to QuickBooks. You can also pull products and inventory levels from QuickBooks into WooCommerce!

= How will orders sync into QuickBooks? =
You'll be able to choose whether to sync orders into QuickBooks as either an Invoice and Payment, or a Sales Receipt.

= Can I sync multiple stores to one QuickBooks Online account? =
Yes, you can sync as many WooCommerce stores as you’d like to the same QuickBooks Online account. You will just need a paid plan for each store.

= Do you offer support? =
Absolutely! We include 24/7 support with all our plans - by support ticket and email. We also include one complimentary setup call/screenshare with each of our plans.

= Are there any more FAQ or Docs? =
There sure are! Visit our growing docs site at <https://docs.myworks.software>

== Screenshots ==
 
1. This screen shot description corresponds to screenshot-1.(png|jpg). This describes plugin's dashboard

== Changelog ==


= v1.6.1 =
* Confirmed support for WooCommerce 3.5
* Improved handling of inventory sync from WooCommerce to QuickBooks in rare cases when inventory is synced alongside an order
* Improved handling for syncing payments from QuickBooks to update WooCommerce order status (for full and partial payments)
* Improved Map/Push > Customer interface to only show customers from excluded roles when setting to sync all orders to one QuickBooks customer is enabled
* Added support for automatically syncing customer updates from WooCommerce to QuickBooks


= v1.6.0 =
* Improved Dashboard metrics and set default graph display to Month
* Fixed bug with sales receipts not correctly voiding in QB when order is cancelled in WooCommerce
* Added initial support for easier QuickBooks Connection process (and automatic reconnection)
* Plugin updates will be automatically pushed from wordpress.org from this point forward


= v1.5.6 =
- Changed plugin folder name to myworks-woo-sync-for-quickbooks-online (upgrades will convert the name automatically)
- Officially launched plugin on wordpress.org  


= v1.5.5 =
- Update to add compatibility for future updates pushed from wordpress.org repo


= v1.5.4 =
- Added setting to choose whether to change Stock Status to In-Stock/Out of Stock based on our sync updating inventory level to or from 0
- Improved handling of inventory sync when quick-editing product inventory on main Product page in admin
- Added setting to Map > Payment Methods to adjust the due date of the order in QuickBooks
- Changed the default syncing mode for first time users to Queue Sync


= v1.5.2 =
- Improved class support for per-line transactions, and bank deposits
- Improved refund sync settings, to control per-gateway (removed redundant setting from Settings > Automatic Sync)
- Greatly improved inventory sync from QuickBooks > WooCommerce, building in support for wp-cron
- Greatly improved automatic batch support for bank deposits, building in support for wp-cron
- Added support to pull product information from QuickBooks to update mapped variations in Woo
- Added order # and transaction ID info to memo field for payments and bank deposits in QuickBooks
- Resolved issue where order updates weren't syncing to QuickBooks while Queue Sync was enabled
- Improved setting to only show products with different inventory levels in Push > Inventory Levels for variations
- Improved setting for Default QuickBooks Customer Display Name - to send First + Last Name if company name doesn't exist
- Updated library to use QuickBooks API minor version 23