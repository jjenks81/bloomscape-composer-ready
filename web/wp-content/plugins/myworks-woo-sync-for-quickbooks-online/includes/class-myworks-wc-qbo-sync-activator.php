<?php

/**
 * Fired during plugin activation
 *
 * @link       http://myworks.design/software/wordpress/woocommerce/myworks-wc-qbo-sync
 * @since      1.0.0
 *
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    MyWorks_WC_QBO_Sync
 * @subpackage MyWorks_WC_QBO_Sync/includes
 * @author     My Works <support@myworks.design>
 */
class MyWorks_WC_QBO_Sync_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		/*
		if (!extension_loaded('mcrypt'))
		die(__('This plugin requires <a target="_blank" href="http://php.net/manual/en/book.mcrypt.php">PHP Mcrypt Extension loaded into your server</a> to be active!', 'mw_wc_qbo_sync'));
		*/
	    
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		
		/*
		if(MyWorks_WC_QBO_Sync_Activator::activation_invalid_chars_in_db_conn_info()){

			$error_message = __('MyWorks QuickBooks Online Sync for WooCommerce does not support these special characters in your database password in your wp-config.php file:  (+ / # % ‘ ?)  Please update your database password to not include these characters.', 'mw_wc_qbo_sync');
			die($error_message);
		}
		*/
		
		if (MyWorks_WC_QBO_Sync_Activator::check_if_woocommerce_active()) {
			
			//MyWorks_WC_QBO_Sync_Activator::create_custom_role_and_capabilities();

			if ( is_multisite() ) { 
		        global $wpdb;
		        foreach ($wpdb->get_col("SELECT blog_id FROM {$wpdb->blogs}") as $blog_id) {
		            switch_to_blog($blog_id);
		            $active_plugins = array();
		            $active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
		            $active_plugins[] = 'woocommerce/woocommerce.php';
		            update_option('active_plugins',$active_plugins);
		            MyWorks_WC_QBO_Sync_Activator::create_databases();
		            restore_current_blog();
		        }
		        MyWorks_WC_QBO_Sync_Activator::do_after_activate();
		    } else {
		        MyWorks_WC_QBO_Sync_Activator::create_databases();
				MyWorks_WC_QBO_Sync_Activator::do_after_activate();
		    }

			return true;
		} else {

			$error_message = __('This plugin requires <a target="_blank" href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> plugin to be active!', 'mw_wc_qbo_sync');
			die($error_message);
		}
	}

	protected function activation_invalid_chars_in_db_conn_info(){

		$invalid_chars = array('+','/','#','%','\'','?');
		foreach($invalid_chars as $char){
			if( strpos( DB_USER, $char ) !== false || strpos( DB_PASSWORD, $char ) !== false || strpos( DB_HOST, $char ) !== false || strpos( DB_NAME, $char ) !== false) {
				return true;
			}
		}
		return false;
	}
	
	protected static function check_if_woocommerce_active() { 
	if ( is_multisite() ) {
		if(class_exists( 'WooCommerce' )) {
			return true;
		}
		return false;
	}else{
		if(class_exists( 'WooCommerce' ) && in_array('woocommerce/woocommerce.php',apply_filters( 'active_plugins', get_option( 'active_plugins' ) ))){
			return true;
		}
	}
	return false;
	}

	/*protected static function create_custom_role_and_capabilities() { 
		$role = add_role( 'mw_qbo_sync_online', 'MyWork Sync QBO Online' );
		if( null === $role ) return false;
		$role = get_role( 'mw_qbo_sync_online' );
		$role->add_cap( 'manage_woocommerce' );
		$role->add_cap( 'view_woocommerce_report' );
	  	return true;
	}*/

	protected static function create_databases(){

		global $wpdb;
		$sql = array();

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_real_time_sync_queue` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`item_type` varchar(255) NOT NULL,
		`item_action` varchar(255) NOT NULL,
		`woocommerce_hook` varchar(255) NOT NULL,
		`item_id` int(11) NOT NULL,
		`run` int(1) NOT NULL,
		`success` int(1) NOT NULL,
		`added_date` datetime NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_real_time_sync_history` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`item_type` varchar(255) NOT NULL,
		`item_action` varchar(255) NOT NULL,
		`woocommerce_hook` varchar(255) NOT NULL,
		`item_id` int(11) NOT NULL,
		`run` int(1) NOT NULL,
		`success` int(1) NOT NULL,
		`added_date` datetime NOT NULL,
		`dt_str` varchar(255) NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_customer_pairs` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_customerid` int(11) NOT NULL,
		`qbo_customerid` int(11) NOT NULL,
		PRIMARY KEY (`id`)
		)";
		
		/*
		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_vendor_pairs` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_customerid` int(11) NOT NULL,
		`qbo_vendorid` int(11) NOT NULL,
		PRIMARY KEY (`id`)
		)";
		*/
		
		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_payment_id_map` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_payment_id` int(11) NOT NULL,
		`qbo_payment_id` int(11) NOT NULL,
		/*Peter added below on 08/04/17 */
		`is_wc_order` INT(1) NOT NULL DEFAULT '0',
		PRIMARY KEY (`id`)
		)";
		/*
		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_promo_code_product_map` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`promo_id` int(11) NOT NULL,
		`qbo_product_id` int(11) NOT NULL,
		`class_id` VARCHAR( 255 ) NOT NULL,
		PRIMARY KEY (`id`)
		)";
		*/

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_shipping_product_map` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_shippingmethod` varchar(255) NOT NULL,
		`qbo_product_id` int(11) NOT NULL,
		`class_id` VARCHAR( 255 ) NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_paymentmethod_map` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_paymentmethod` varchar(255) NOT NULL,
		`qbo_account_id` int(11) NOT NULL,
		`currency` varchar(255) NOT NULL,
		`enable_transaction` int(11) NOT NULL,
		`txn_expense_acc_id` int(11) NOT NULL,
		`enable_payment` int(1) NOT NULL,
		`txn_refund` int(1) NOT NULL,
		`enable_refund` int(1) NOT NULL,
		`enable_batch` int(1) NOT NULL,
		`udf_account_id` int(11) NOT NULL,
		`vendor_id` int(11) NOT NULL,
		`qb_p_method_id` int(11) NOT NULL,
		`lump_weekend_batches` int(1) NOT NULL,
		`term_id` int(11) NOT NULL,
		`ps_order_status` varchar(255) NOT NULL,
		`individual_batch_support` int(1) NOT NULL,
		`deposit_cron_utc` varchar(255) NOT NULL,
		`inv_due_date_days` int(3) NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_tax_map` (
		`id` int(11) NOT NULL AUTO_INCREMENT,              
		`wc_tax_id` int(11) NOT NULL,
		`qbo_tax_code` varchar(255) NOT NULL,
		`wc_tax_id_2` int(11) NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_log` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`log_title` varchar(255) NOT NULL,
		`details` text NOT NULL,
		`success` int(11) NOT NULL,
		`log_type` varchar(255) NOT NULL,
		`added_date` datetime NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_qbo_customers` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`qbo_customerid` int(11) NOT NULL,
		`first` varchar(255) NOT NULL,
		`middle` varchar(255) NOT NULL,
		`last` varchar(255) NOT NULL,
		`company` varchar(255) NOT NULL,
		`dname` varchar(255) NOT NULL,
		`email` varchar(255) NOT NULL,
		PRIMARY KEY (`id`)
		)";
		
		/*
		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_qbo_vendors` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`qbo_vendorid` int(11) NOT NULL,
		`first` varchar(255) NOT NULL,
		`middle` varchar(255) NOT NULL,
		`last` varchar(255) NOT NULL,
		`company` varchar(255) NOT NULL,
		`dname` varchar(255) NOT NULL,
		`pocname` varchar(255) NOT NULL,
		`email` varchar(255) NOT NULL,
		PRIMARY KEY (`id`)
		)";
		*/

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_product_pairs` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_product_id` int(11) NOT NULL,
		`quickbook_product_id` int(11) NOT NULL,
		`class_id` VARCHAR( 255 ) NOT NULL,
		PRIMARY KEY (`id`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_qbo_items` (
		`ID` int(11) NOT NULL AUTO_INCREMENT,
		`itemid` int(11) NOT NULL,
		`name` varchar(255) NOT NULL,
		`sku` varchar(255) NOT NULL,
		`product_type` varchar(255) NOT NULL,
		`parent_id` int(11) NOT NULL,
		PRIMARY KEY (`ID`)
		)";

		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_variation_pairs` (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`wc_variation_id` int(11) NOT NULL,
		`quickbook_product_id` int(11) NOT NULL,
		`class_id` varchar(255) NOT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		
		$sql[] = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}mw_wc_qbo_sync_wq_cf_map` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,								  
		  `wc_field` varchar(255) NOT NULL,
		  `qb_field` varchar(255) NOT NULL,
		  `ext_data` TEXT NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

		if($sql){

			foreach($sql as $query){

				$wpdb->query($query);

			}

		}
		
		//Default Settings
		update_option( 'mw_wc_qbo_sync_save_log_for', 30);
		update_option( 'mw_wc_qbo_sync_tax_format', 'TaxExclusive');
		update_option( 'mw_wc_qbo_sync_connection_number', 1);
		
		update_option( 'mw_wc_qbo_sync_rt_push_enable', 'true');
		//update_option( 'mw_wc_qbo_sync_select2_ajax', 'true');
		update_option( 'mw_wc_qbo_sync_select2_status', 'true');
		update_option( 'mw_wc_qbo_sync_rt_push_items', 'Customer,Invoice,Category,Payment,Refund');
		update_option( 'mw_wc_qbo_sync_store_currency', get_option('woocommerce_currency'));
		
		update_option( 'mw_wc_qbo_sync_specific_order_status', 'wc-processing,wc-completed');
		update_option( 'mw_wc_qbo_sync_fresh_install', 'true');
		
		//
		update_option( 'mw_wc_qbo_sync_invnt_pull_set_prd_stock_sts', 'true');

		if(get_option('mw_wc_qbo_sync_order_as_sales_receipt')=='') update_option( 'mw_wc_qbo_sync_order_as_sales_receipt', 'true');
		
		
		update_option( 'mw_wc_qbo_sync_pause_up_qbo_conection', 'true');
		
		//01-08-2017
		if(isset($_SESSION['mw_wc_qbo_sync_qbo_con_creds'])){
			unset($_SESSION['mw_wc_qbo_sync_qbo_con_creds']);
		}
		
		if(isset($_SESSION['mw_wc_qbo_sync_qbo_is_connected_rts'])){
			unset($_SESSION['mw_wc_qbo_sync_qbo_is_connected_rts']);
		}
		
		if(isset($_SESSION['mw_wc_qbo_sync_rts_license_data'])){
			unset($_SESSION['mw_wc_qbo_sync_rts_license_data']);
		}
		
		if(isset($_SESSION['mw_wc_qbo_sync_mwqs_session_msg'])){
			unset($_SESSION['mw_wc_qbo_sync_mwqs_session_msg']);
		}
		

	}
	
	protected static function do_after_activate(){
		//ob_start();
		$url = get_bloginfo('url');
		//wpurl
		
		$company = get_bloginfo('name');
		$email = get_bloginfo('admin_email');
		$wordpress_version = get_bloginfo('version');
		
		//global $current_user;
		//get_currentuserinfo();
		$MyWorks_WC_QBO_Sync = new MyWorks_WC_QBO_Sync();
		$version = $MyWorks_WC_QBO_Sync->get_version();
		
		global $woocommerce;
		$woocommerce_version = $woocommerce->version;
		
		$message = '';
		$message .= "<b>WooCommerce Sync for QuickBooks Online</b></br>";
		$message .= "</br>";
		$message .= "<b>Company:</b> ".$company."</br>";
		$message .= "<b>Email:</b> ".$email."</br>";
		$message .= "<b>WooCommerce URL:</b> ".$url ."</br>";
		$message .= "<b>Wordpress Version:</b> ".$wordpress_version ."</br>";
		$message .= "<b>WooCommerce Version:</b> ".$woocommerce_version ."</br>";
		
		
		$headers = array(
			'MIME-Version: 1.0',
			'Content-type:text/html;charset=UTF-8',
		);
		
		$to = 'notifications@myworks.design';		
		
		wp_mail($to, 'New Install - WooCommerce Sync', $message, $headers);
		
		$post_url = 'https://myworks.design/dashboard/api/dashboard/product/saveModule';
		
		$params = array(
			'api_version'=>'0.1',
			'result_type'=>'json',
			'process'=>'activated',
			'version'=>$version,
			'company'=>$company,
			'email'=>$email,
			'system_url'=>$url
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $post_url); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128); curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$response = curl_exec($ch);
		curl_close($ch);
		
		//$output_buffer = ob_end_clean();
		
		/*$dirpath = dirname(__FILE__);
		$log_file = fopen($dirpath.DIRECTORY_SEPARATOR.'log.txt','w');
		$txt = $output_buffer;
		fwrite($log_file, $txt);
		fclose($log_file);*/
	}
}