<?php
global $MSQS_QL;
?>
<?php echo '<link href="'.esc_url( plugins_url( "css/dash-board-sec.css", dirname(__FILE__) ) ).'" rel="stylesheet" type="text/css">' ?>
<div class="mw-qbo-sync-welcome">
	<div class="mw-qbo-sync-title">
		<img width="225"  alt="mw-qbo-sync" src="<?php echo plugins_url( 'myworks-woo-sync-for-quickbooks-online/admin/image/mwd-logo.png' ) ?>" class="mw-qbo-sync-logo"><small><sup>v<?php echo MyWorks_WC_QBO_Sync_Admin::return_plugin_version() ?></sup></small>
		<span class="baseline" style="font-size:25px">Simply follow the setup steps below to start syncing!</span>
		<?php if((int) $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_connected') && $MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_refreshed') && $MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_default_settings') && $MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_data_mapped')){ ?>
		<a title="Dismiss this notice" class="mw-qbo-sync-notice-dismiss mw-qbo-sync-welcome-remove" href="<?php echo admin_url( 'admin.php?page=myworks-wc-qbo-sync&mw_wc_qbo_sync_qbo_is_init=true') ?>"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">Dismiss this notice</span></a>
		<?php } ?>
	</div>
	<div class="mw-qbo-sync-settings-section">
		<div class="mw-qbo-sync-columns counter">
			<div class="col-1-3 <?php if((int) $MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_connected')){ ?>over-layer<?php } ?>">
				<img width="48" height="48" alt="" src="<?php echo plugins_url( 'myworks-woo-sync-for-quickbooks-online/admin/image/connecticon.png' ) ?>">
				<div class="mw-qbo-sync-col-content">
					<p class="mw-qbo-sync-col-title">Connect to QuickBooks Online</p>
					<p class="mw-qbo-sync-col-desc">Don't have an QuickBooks account yet? Register for one at quickbooks.intuit.com!</p>
					<p>
						<input type="hidden" value="f453ebb0a5" name="mw-qbo-syncsignupnonce" id="mw-qbo-syncsignupnonce">							<a class="button button-primary" href="<?php echo admin_url( 'admin.php?page=myworks-wc-qbo-sync-connection') ?>" id="mw-qbo-sync-signup">Connect to QuickBooks Online!</a></p>
				</div>
			</div>
            <div class="col-1-3 <?php if($MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_refreshed')){ ?>over-layer<?php } ?>">
				<img width="48" height="48" alt="" src="<?php echo plugins_url( 'myworks-woo-sync-for-quickbooks-online/admin/image/refreshicon.png' ) ?>">
				<div class="mw-qbo-sync-col-content">
					<p class="mw-qbo-sync-col-title">Refresh Data</p>
					<p class="mw-qbo-sync-col-desc">Now that you've connected to QuickBooks Online - click the button below to recognize the latest customers and products from QuickBooks Online.</p>
					<p><a class="button button-primary" href="<?php echo home_url( 'index.php?mw_qbo_sync_public_quick_refresh=1') ?>">Refresh Data</a></p>
				</div>
			</div>
			<div class="col-1-3 <?php if($MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_default_settings')){ ?>over-layer<?php } ?>">
				<img width="48" height="48" alt="" src="<?php echo plugins_url( 'myworks-woo-sync-for-quickbooks-online/admin/image/configureicon.png' ) ?>">
				<div class="mw-qbo-sync-col-content">
					<p class="mw-qbo-sync-col-title">Save Default Settings</p>
					<p class="mw-qbo-sync-col-desc">Visit MyWorks Sync > Settings to set and save your default settings. Not sure? <a href="https://docs.myworks.software/woocommerce-sync-for-quickbooks-online" target="_blank">Check out our documentation!</a>.</p>
					<p>
						<input type="hidden" value="a72829cbec" name="mw-qbo-synccheckapikeynonce" id="mw-qbo-synccheckapikeynonce">							<a class="button button-primary" href="<?php echo admin_url( 'admin.php?page=myworks-wc-qbo-sync-settings') ?>" id="mw-qbo-sync-save-api-key">Default Settings</a></p>
				</div>
			</div>
			<div class="col-1-3 <?php if($MSQS_QL->option_checked('mw_wc_qbo_sync_qbo_is_data_mapped')){ ?>over-layer<?php } ?>">
				<img width="48" height="48" alt="" src="<?php echo plugins_url( 'myworks-woo-sync-for-quickbooks-online/admin/image/saveicon.png' ) ?>">
				<div class="mw-qbo-sync-col-content">
					<p class="mw-qbo-sync-col-title">Map Existing Data</p>
					<p class="mw-qbo-sync-col-desc">You're almost done! You just need to map at least 1 current client, product, and payment gateway!<br/><br/>
				    <span class="<?php if($MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_customer')){ ?>tick-image<?php }else{ ?>close-image<?php } ?>">Map 1 Customer</span><br/>
				    <span class="<?php if($MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_product')){ ?>tick-image<?php }else{ ?>close-image<?php } ?>">Map 1 Product </span><br/>
				    <span class="<?php if($MSQS_QL->get_option('mw_wc_qbo_sync_qbo_is_data_mapped_payment')){ ?>tick-image<?php }else{ ?>close-image<?php } ?>">Map 1 Gateway </span>
				    </p>
					<p><a class="button button-primary" href="<?php echo admin_url( 'admin.php?page=myworks-wc-qbo-map') ?>">Go to Mapping</a></p>
				</div>
			</div>
		</div>
	</div>
</div>